import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowSorter;
import javax.swing.RowSorter.SortKey;
import javax.swing.SortOrder;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

public class Recherche extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 608801054694450408L;

	JTextField tRecherche = new JTextField();

	static String[] titreLivres = { "id", "Nom", "Auteur", "Dispo", "cote" };
	public static DefaultTableModel dtmLivres = new DefaultTableModel(null, titreLivres) {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@SuppressWarnings({ "unchecked", "rawtypes" })
		@Override
		public Class getColumnClass(int column) {
			switch (column) {
			case 0:
				return Integer.class;
			default:
				return Object.class;
			}
		}

		@Override
		public boolean isCellEditable(int row, int column) {
			return false;
		}
	};
	JTable tableauLivres = new JTable(dtmLivres);

	JButton boutonRecherche = new JButton("Recherche");

	JScrollPane jspTableaux = new JScrollPane(tableauLivres);

	TableRowSorter<DefaultTableModel> trieurLivres = new TableRowSorter<DefaultTableModel>(dtmLivres);
	List<RowSorter.SortKey> clesDeTri = new ArrayList<SortKey>(25);

	JPanel premiereLigne = new JPanel();
	JPanel pageTop = new JPanel();

	JPanel pLabelSelection = new JPanel();
	JPanel pLabelNbResults = new JPanel();

	JPanel pTexteIndic = new JPanel();

	JLabel lLivresSelectionnes = new JLabel("Sélection vide");
	JLabel lElevesSelectionnes = new JLabel("Sélection vide");

	JLabel lNbResults = new JLabel("Nombre de résultats");

	public Recherche() {

		// tableaux
		tableauLivres.getColumnModel().getColumn(0).setMaxWidth(60);
		tableauLivres.getColumnModel().getColumn(0).setPreferredWidth(45);

		tableauLivres.getColumnModel().getColumn(4).setMaxWidth(80);
		tableauLivres.getColumnModel().getColumn(4).setPreferredWidth(60);

		tableauLivres.getColumnModel().getColumn(3).setMaxWidth(100);
		tableauLivres.getColumnModel().getColumn(3).setPreferredWidth(80);

		tableauLivres.setRowSorter(trieurLivres);

		clesDeTri.add(new RowSorter.SortKey(2, SortOrder.ASCENDING));

		trieurLivres.setSortKeys(clesDeTri);

		// Bouton (et barre de recherche)
		boutonRecherche.addActionListener(alRecherche);
		tRecherche.addActionListener(alRecherche);

		/*
		 * Focus request
		 */

		this.addComponentListener(new ComponentListener() {
			public void componentShown(ComponentEvent e) {
				Recherche.this.requestFocusInWindow();
				tRecherche.requestFocus();
			}

			public void componentResized(ComponentEvent e) {
			}

			public void componentMoved(ComponentEvent e) {
			}

			public void componentHidden(ComponentEvent e) {
			}
		});

		/*
		 * Préparation du layout
		 */
		tRecherche.setPreferredSize(new Dimension(400, 30));

		premiereLigne.setLayout(new FlowLayout());
		premiereLigne.add(tRecherche);
		premiereLigne.add(boutonRecherche);

		pageTop.setLayout(new BoxLayout(pageTop, BoxLayout.PAGE_AXIS));
		pageTop.add(premiereLigne);

		/*
		 * BorderLayout
		 */
		this.setLayout(new BorderLayout());

		this.add(pageTop, BorderLayout.NORTH);

		this.add(jspTableaux, BorderLayout.CENTER);

		pLabelNbResults.add(lNbResults);

		pTexteIndic.setLayout(new GridLayout(1, 2));
		pTexteIndic.add(pLabelSelection);
		pTexteIndic.add(pLabelNbResults);

		this.add(pTexteIndic, BorderLayout.SOUTH);

		/*
		 * Gestion des clics sur les livres
		 */
		tableauLivres.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent souris) {
				JTable jt = (JTable) souris.getSource();

				Point p = souris.getPoint();
				int row = jt.rowAtPoint(p);

				if ((souris.getClickCount() == 2) && (SwingUtilities.isLeftMouseButton(souris))) {
					String resultats = utils.connexionServeur(ALaFraise.paraserv.get("ip"), Integer.parseInt(ALaFraise.paraserv.get("port")),
							"S=" + utils.sixChiffrise(jt.getValueAt(row, 0).toString()));

					new DetailLivre(Integer.parseInt(jt.getValueAt(row, 0).toString()), resultats, Recherche.this);
				}
			}
		});
	}

	ActionListener alRecherche = new ActionListener() {

		public void actionPerformed(ActionEvent e) {
			// TODO Changer le port et l'ip en fonction des paramètres

			String resultats = utils.connexionServeur(ALaFraise.paraserv.get("ip"), Integer.parseInt(ALaFraise.paraserv.get("port")),"R=" + tRecherche.getText());
			
			if (!resultats.equals("<-- RIEN -->")) { 
				dtmLivres.setRowCount(0);
				int nbRes = Integer.parseInt(resultats.split(";;;")[1]);
				lNbResults.setText("Nombre de résultats : " + nbRes);

				if (nbRes>0) {
					String tempCh = resultats.split(";;;")[0];

					String[] listRes = tempCh.substring(1, tempCh.length() - 1).split(";;");
					for (String unRes : listRes) {
						/*
						 * Petite manip pour que l'id soit considéré comme un entier (pour le tri, c'est mieux)
						 */
						Object[] listeOnlyStrings = unRes.substring(1, unRes.length() - 1).split(",,,");
						Object[] aoLaListe = new Object[listeOnlyStrings.length];
						for (int i=0;i<listeOnlyStrings.length;i++) {
							if (i==0) {
								aoLaListe[i] = Integer.valueOf(listeOnlyStrings[0].toString());
							} else {
								aoLaListe[i] = listeOnlyStrings[i];
							}
						}
						dtmLivres.addRow(aoLaListe);
					}
				}
			}
			else {
				System.out.println("On dirait qu'il n'y a pas de résultats.");
			}
		}
	};	

	/*public void colorize() {
		if (tStatut.getText().equals("Disponible")) {
			tStatut.setForeground(new Color(40, 155, 70));
			bEmprunter.setEnabled(true);
			bRendre.setEnabled(false);
		} 
		else if (tStatut.getText().equals("Emprunté")) {
			tStatut.setForeground(Color.RED);
			bEmprunter.setEnabled(false);
			bRendre.setEnabled(true);
		}
		else {
			tStatut.setForeground(Color.RED);
			bEmprunter.setEnabled(false);
			bRendre.setEnabled(false);
		}
	}*/
}
