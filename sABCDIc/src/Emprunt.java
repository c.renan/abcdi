import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.BoxLayout;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class Emprunt extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6538311028061474335L;

	JTextField tInfo = new JTextField();

	JButton bConfirmer = new JButton("Confirmer l'emprunt");

	JButton bCancel = new JButton("Annuler");
	
	JLabel lEleve = new JLabel();
	JLabel lIdEleve = new JLabel();

	static int idLivre;
		
	public Emprunt(int idLivre, Component parent) {
		Emprunt.idLivre = idLivre;
		InputMap im = getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
		ActionMap am = getRootPane().getActionMap();

		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "cancel");
		am.put("cancel", new AbstractAction() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});

		// layout
		this.getContentPane().setLayout(new BoxLayout(this.getContentPane(), BoxLayout.PAGE_AXIS));

		JPanel pPremLi = new JPanel();
		pPremLi.add(new JLabel("Scanner la carte ou taper le nom : "));

		this.getContentPane().add(pPremLi);

		JPanel pInfo = new JPanel();
		pInfo.setLayout(new FlowLayout());
		tInfo.setPreferredSize(new Dimension(200, 30));
		tInfo.requestFocus();
		tInfo.getDocument().addDocumentListener(dlInfo);

		pInfo.add(tInfo);

		this.getContentPane().add(pInfo);

		JPanel pDeduc = new JPanel();
		pDeduc.setLayout(new FlowLayout());

		pDeduc.add(new JLabel("Élève : "));
		pDeduc.add(lEleve);
		
		JPanel pCarte = new JPanel();
		pCarte.setLayout(new FlowLayout());
		pCarte.add(new JLabel("Carte n°"));
		pCarte.add(lIdEleve);
		
		this.getContentPane().add(pDeduc);
		this.getContentPane().add(pCarte);
		
		JPanel pConfirm = new JPanel();
		pConfirm.setLayout(new FlowLayout());
		bConfirmer.setEnabled(false);
		pConfirm.add(bConfirmer);
		
		bCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Emprunt.this.dispose();
			}
		});
		pConfirm.add(bCancel);

		bConfirmer.addActionListener(alEmprunte);
		
		this.getContentPane().add(pConfirm);
		
		try {
			Image imageIcone = ImageIO.read(new File("resources/emprunt.png"));
			this.setIconImage(imageIcone);
		} catch (Exception e) {
			e.printStackTrace();
		}

		this.setTitle("Emprunter le livre " + idLivre);
		
		this.pack();
		
		this.setLocationRelativeTo(parent);
		this.setVisible(true);
	}

	DocumentListener dlInfo = new DocumentListener() {
		@Override
		public void removeUpdate(DocumentEvent arg0) {
			selectionEleves(tInfo.getText());
		}

		@Override
		public void insertUpdate(DocumentEvent arg0) {
			selectionEleves(tInfo.getText());
		}

		@Override
		public void changedUpdate(DocumentEvent arg0) {
			selectionEleves(tInfo.getText());
		}

		public void selectionEleves(String filtre) {
			// Première question : est-ce un id ?
			if (tInfo.getText().matches("9\\d{5}")) {
				lEleve.setText(ALaFraise.listEleves.get(tInfo.getText()));
				bConfirmer.setEnabled(true);
				lIdEleve.setText(tInfo.getText());
			}
			else {
				// si ce n'en est pas un, alors on déchiffre
				ArrayList<String> lesPossibles= new ArrayList<String>();
				
				for (String i : ALaFraise.listEleves.keySet()) {
					boolean contient = true;
					for (String mot : filtre.split(" ")) {
						String unEleve=i+" "+ALaFraise.listEleves.get(i);
						if (!utils.vireAccents(unEleve).contains(utils.vireAccents(mot))) {
							contient= false;
						}
					}
					if (contient) {
						lesPossibles.add(i);
					}
				}
				
				if (lesPossibles.size()<=1) {
					if (lesPossibles.size()==0) {
						lEleve.setText("Aucun élève ne correspond.");
						lIdEleve.setText("");
						bConfirmer.setEnabled(false);
					} else {
						lEleve.setText(ALaFraise.listEleves.get(lesPossibles.get(0)));
						bConfirmer.setEnabled(true);
						lIdEleve.setText(lesPossibles.get(0));
					}
				}
				else {
					lEleve.setText(lesPossibles.size()+" possibilités");
					bConfirmer.setEnabled(false);
					lIdEleve.setText("");
				}
				
			}
			
		}

	};

	
	ActionListener alEmprunte = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			String reussiteOperation = utils.connexionServeur(ALaFraise.paraserv.get("ip"), Integer.parseInt(ALaFraise.paraserv.get("port")), "E="+lIdEleve.getText()+",L="+utils.sixChiffrise(String.valueOf(idLivre)));
			
			
			if (reussiteOperation.equals("1")) {
				JOptionPane.showMessageDialog(Emprunt.this, "C'est bon, l'emprunt est réalisé.", "Emprunt effectué", JOptionPane.INFORMATION_MESSAGE);
				Emprunt.this.dispose();
			} else {
				JOptionPane.showMessageDialog(Emprunt.this, "Erreur : "+reussiteOperation);
			}
			
		}
	};
}
