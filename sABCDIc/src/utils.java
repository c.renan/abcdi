import java.awt.Color;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.Normalizer;
import java.util.Map;

import javax.swing.JOptionPane;

public class utils {

	static public String connexionServeur(String ip, int port, String sRecherche) {
		Socket chaussette;
		PrintWriter chassedeau = null;
//		BufferedInputStream lecteur = null;
		BufferedReader lecteur;

		String reponse = null;
		try {
			chaussette = new Socket(ip, port);

			chassedeau = new PrintWriter(chaussette.getOutputStream(), true);
//			lecteur = new BufferedInputStream(chaussette.getInputStream());
			lecteur = new BufferedReader(new InputStreamReader(chaussette.getInputStream()));

			chassedeau.write(sRecherche);
			/*
			 * ne pas oublier de flusher sinon la commande ne part pas dans le tuyau (tadire
			 * que là c'est effectivement une commande de merde)
			 * 
			 */
			chassedeau.flush(); // mettre un nom de variable pourri exprès pour faire une blague de merde, on
								// reste dans le thème.

			reponse = lire(lecteur);

			chassedeau.close();
			chaussette.close();
		} catch (UnknownHostException uhe) {
			JOptionPane.showMessageDialog(null, "Les coordonnées fournies ne dirigent pas vers un serveur.", "Erreur",
					JOptionPane.ERROR_MESSAGE);
			uhe.printStackTrace();
		} catch (IOException ioe) {
			JOptionPane.showMessageDialog(null, "La connexion au serveur a échoué.", "Erreur",
					JOptionPane.ERROR_MESSAGE);
			ioe.printStackTrace();
		}

		return reponse;

	}

	public static String vireAccents(String src) {
		if (src != null) {
			String retour = Normalizer.normalize(src, Normalizer.Form.NFD);
			retour = retour.replaceAll("[^\\p{ASCII}]", "").toLowerCase();

			return retour;
		}
		return "";
	}

	public static String sixChiffrise(String src) {
		String retour = src;
		while (retour.length() < 6) {
			retour = "0" + retour;
		}
		return retour;
	}

	public static String sixChiffrise(String src, boolean eleve) {
		String retour = src;
		while (retour.length() < 5) {
			retour = "0" + retour;
		}
		if (retour.length() == 5) {
			if (eleve) {
				retour = "9" + retour;
			} else {
				retour = "0" + retour;
			}
		}
		return retour;
	}

	/*
	 * private static String lire(BufferedInputStream reader) throws IOException{
	 * String response = ""; int stream; byte[] b = new byte[4096]; stream =
	 * reader.read(b); response = new String(b, 0, stream); return response; }
	 */

	private static String lire(BufferedReader reader) throws IOException {
		String response = "";
		response = reader.readLine();
		return response;
	}

	static public Map<String, Object> selection(String requete) {
		// Map<String,Object> resultat = new HashMap<String,Object>();
		return null;
	}

	public static Color colorDewey(String codeDewey) {
		Color c = null;
		int cd = -1;
		if (codeDewey.matches("D[ ]*[\\d]+.*")) {
			cd = Integer.valueOf(codeDewey.replaceAll(" ", "").substring(1, 2));
		} else if (codeDewey.matches("[\\d]+.*")) {
			cd = Integer.valueOf(codeDewey.replaceAll(" ", "").substring(0, 1));
		}

		/*
		 * Pourquoi ce if ? Parce que si la personne oublie de mettre le D ou même j'ai
		 * besoin d'appeler la fonction sans le D quand je fais l'affichage de la
		 * marguerite.
		 */

		switch (cd) {
		case -1:
			break;
		case 0:
			c = Color.BLACK;
			break;
		case 1:
			c = new Color(153, 52, 0); // marron
			break;
		case 2:
			c = new Color(254, 0, 0); // rouge
			break;
		case 3:
			c = new Color(255, 102, 0); // orange
			break;
		case 4:
			c = new Color(255, 255, 0); // jaune
			break;
		case 5:
			c = new Color(0, 128, 1); // vert
			break;
		case 6:
			c = new Color(0, 3, 251); // bleu
			break;
		case 7:
			c = new Color(129, 0, 127); // violet
			break;
		case 8:
			c = new Color(131, 131, 131); // gris
			break;
		case 9:
			c = Color.WHITE;
			break;
		}
		return c;
	}

	public static Color couleurQuiSeVoit(Color base) {
		/*
		 * Les constantes 0.299, 0.587 et 0.114 utilisées dans cette fonction sont
		 * tirées de http://alienryderflex.com/hsp.html Merci mec.
		 */
		double lumiere = Math.sqrt(0.299 * Math.pow(base.getRed(), 2) + 0.587 * Math.pow(base.getGreen(), 2)
				+ 0.114 * Math.pow(base.getBlue(), 2));
		if (lumiere > 150) {
			return Color.black;
		} else {
			return Color.white;
		}
	}

}
