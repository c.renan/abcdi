import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.BoxLayout;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

public class Parametres extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6538311028061474335L;

	JTextField tIP = new JTextField();
	JTextField tPort = new JTextField();
	
	JTextField tSite = new JTextField();
	JTextField tProxy = new JTextField();
	JTextField tProxyPort = new JTextField();
	
	JButton bConfirmer = new JButton("Valider");
	JButton bCancel = new JButton("Annuler");
	
	public Parametres() {
		InputMap im = getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
		ActionMap am = getRootPane().getActionMap();

		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "cancel");
		am.put("cancel", new AbstractAction() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e) {
				quitter();
			}
		});
		
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "validate");
		am.put("validate", new AbstractAction() {
			
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				valider();
			}
		});

		// layout
		this.getContentPane().setLayout(new BoxLayout(this.getContentPane(), BoxLayout.PAGE_AXIS));

		JPanel pPremLi = new JPanel();
		pPremLi.add(new JLabel("Adresse IP du serveur :"));
		
		tIP.setPreferredSize(new Dimension(200, 30));
		tIP.setText(ALaFraise.paraserv.get("ip"));
		pPremLi.add(tIP);
		
		this.getContentPane().add(pPremLi);
		

		JPanel pDeuxLi = new JPanel();
		pDeuxLi.setLayout(new FlowLayout());

		pDeuxLi.add(new JLabel("Port utilisé :"));
		tPort.setPreferredSize(new Dimension(50,30));
		tPort.setText(ALaFraise.paraserv.get("port"));
		pDeuxLi.add(tPort);
		
		this.getContentPane().add(pDeuxLi);
		
		JPanel pTroisLi = new JPanel();
		pTroisLi.setLayout(new FlowLayout());
		
		pTroisLi.add(new JLabel("URL du serveur de mise à jour :"));
		tSite.setPreferredSize(new Dimension(200,30));
		tSite.setText(ALaFraise.paraserv.get("site"));
		pTroisLi.add(tSite);
		
		this.getContentPane().add(pTroisLi);
		
		JPanel pQuatLi = new JPanel();
		pQuatLi.setLayout(new FlowLayout());
		
		pQuatLi.add(new JLabel("Proxy :"));
		tProxy.setPreferredSize(new Dimension(200,30));
		tProxy.setText(ALaFraise.paraserv.get("proxy"));
		pQuatLi.add(tProxy);
		pQuatLi.add(new JLabel(":"));
		tProxyPort.setPreferredSize(new Dimension(50,30));
		tProxyPort.setText(ALaFraise.paraserv.get("proxyport"));
		pQuatLi.add(tProxyPort);
		
		this.getContentPane().add(pQuatLi);
		
		JPanel pConfirm = new JPanel();
		pConfirm.setLayout(new FlowLayout());
		pConfirm.add(bConfirmer);
		
		bCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Parametres.this.dispose();
			}
		});
		pConfirm.add(bCancel);

		bConfirmer.addActionListener(alChangeParam);
		
		this.getContentPane().add(pConfirm);
		
		try {
			Image imageIcone = ImageIO.read(new File("resources/emprunt.png"));
			this.setIconImage(imageIcone);
		} catch (Exception e) {
			e.printStackTrace();
		}

		this.setTitle("Paramètres serveur");
		
		this.pack();
		
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	public void quitter() {
		Parametres.this.dispose();
	}
	
	public void valider() {
		if (tIP.getText().matches("\\d+\\.\\d+\\.\\d+\\.\\d+") && tPort.getText().matches("(0|[1-9][0-9]{0,3}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])")) {
			ALaFraise.paraserv.put("ip", tIP.getText());
			ALaFraise.paraserv.put("port", tPort.getText());
			ALaFraise.paraserv.put("site", tSite.getText());
			ALaFraise.paraserv.put("proxy", tProxy.getText());
			ALaFraise.paraserv.put("proxyport", tProxyPort.getText());			

			try {
				File parFile = new File("paraserv.ini");
				parFile.setWritable(true);

				BufferedWriter bw = new BufferedWriter(new FileWriter("paraserv.ini"));

				for (String categorie : ALaFraise.paraserv.keySet()) {
					bw.write(categorie+"="+ALaFraise.paraserv.get(categorie)+"\n");
					bw.write("\n");	
				}
				bw.close();
			}
			catch (FileNotFoundException fnf) {
				JOptionPane.showMessageDialog(null, "Le fichier de paramètres n'a pas été trouvé.", "Erreur", JOptionPane.ERROR_MESSAGE);
				fnf.printStackTrace();
			}
			catch (IOException io) {
				JOptionPane.showMessageDialog(null, "Le fichier de paramètres est inaccessible en écriture.", "Erreur", JOptionPane.ERROR_MESSAGE);
				io.printStackTrace();
			}
			quitter();
		}
		else {
			JOptionPane.showMessageDialog(null, "Informations incorrectes.", "Erreur", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	
	ActionListener alChangeParam = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			valider();
		}
	};
}
