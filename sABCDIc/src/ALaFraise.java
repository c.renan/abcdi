import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import javax.swing.JOptionPane;

public class ALaFraise {

	/**
	 * @param args
	 */
	// static public ArrayList<String[]> listEleves = new ArrayList<String[]>();
	/* Au début je voulais faire une map, puis j'me suis dit « Oarf, un arraylist, mais en fait je pense que map c'est mieux. */
	static public HashMap<String,String> listEleves = new HashMap<String,String>();
	
	public static HashMap<String,String> paraserv = new HashMap<String,String>();
	
	public static void main(String[] args) {
		try {
			File paraFile = new File("paraserv.ini");
			paraFile.setReadable(true); // ah.
			
			BufferedReader brFlux = new BufferedReader(new FileReader("paraserv.ini"));
			String ligne;
			
			while ((ligne=brFlux.readLine())!=null) {
				if (ligne.matches(".+=.+")) {
					paraserv.put(ligne.split("=")[0], ligne.split("=")[1]);
				}
				else if (ligne.matches(".+=")) {
					paraserv.put(ligne.split("=")[0],"");
				}
			}
			brFlux.close();
		}
		catch (FileNotFoundException fnf) {
			JOptionPane.showMessageDialog(null, "Le fichier de paramètres n'a pas été trouvé.", "Erreur", JOptionPane.ERROR_MESSAGE);
			fnf.printStackTrace();
		}
		catch (IOException io) {
			JOptionPane.showMessageDialog(null, "Le fichier de paramètres est inaccessible en lecture.", "Erreur", JOptionPane.ERROR_MESSAGE);
			io.printStackTrace();
		}
		
		
		String eleves = utils.connexionServeur(paraserv.get("ip"), Integer.parseInt(paraserv.get("port")), "TE"); // TE pour « Tous élèves »

		String tempCh = eleves.split(";;;")[0];
		String[] listEl = tempCh.substring(1, tempCh.length() - 1).split(";;");
		for (String unEl : listEl) {
			String[] aoLaListe = unEl.substring(1, unEl.length() - 1).split(",,,");
			aoLaListe[0] = utils.sixChiffrise(aoLaListe[0], true);
			
			listEleves.put(aoLaListe[0], aoLaListe[1]+" "+aoLaListe[2]);

		}
				
		
		verifierMAJ vm = new verifierMAJ();
		vm.run(false);
		
		new FenetreClient();
	}

}
