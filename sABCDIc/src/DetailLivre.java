import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.BoxLayout;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;


public class DetailLivre extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;	
	JLabel lNom = new JLabel("Nom");
	JLabel lAuteur = new JLabel("Auteur");
	JLabel lDateP = new JLabel("Date de parution");
	JLabel lPresentation = new JLabel("Présentation");
	JLabel lDimensions = new JLabel("Dimensions");
	JLabel lEditeur = new JLabel("Éditeur");
	JLabel lNbPages = new JLabel("Nombre de pages");
	JLabel lCollection = new JLabel("Collection");
	JLabel lFormat = new JLabel("Format");
	JLabel lResume = new JLabel("Résumé");
	JLabel lStatut = new JLabel("Statut");
	//JLabel lEtat = new JLabel("État");
	JLabel lISBN = new JLabel("ISBN");
	JLabel lDewey = new JLabel("Cote");
	
	JTextField tNom = new JTextField();
	JTextField tAuteur = new JTextField();
	JTextField tDateP = new JTextField();
	JTextField tPresentation = new JTextField();
	JTextField tDimensions = new JTextField();
	JTextField tEditeur = new JTextField();
	JTextField tNbPages = new JTextField();
	JTextField tCollection = new JTextField();
	JTextField tFormat = new JTextField();
	JTextField tISBN = new JTextField();
	JTextField tDewey = new JTextField();
	JTextField tStatut = new JTextField();
	
	JTextArea tResume = new JTextArea();

	// Un JScrollPane pour la textarea resume
	JScrollPane spResume = new JScrollPane(tResume, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
			
	Map<String, JTextField> lesJTF = new LinkedHashMap<String, JTextField>();
	Map<String, JLabel> lesLabels = new LinkedHashMap<String, JLabel>();
	
	JButton bFermer = new JButton("Fermer");

	public DetailLivre(int idLivre,String details, Component parent) {
		InputMap im=getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
		ActionMap am = getRootPane().getActionMap();
		
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "cancel");
		am.put("cancel", new AbstractAction() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
		/*
		 * Avant je faisais des listes de labels et de tf. Maintenant je fais des maps.
		 */
		lesLabels.put("nom",lNom);
		lesLabels.put("auteur",lAuteur);
		lesLabels.put("dateParution",lDateP);
		lesLabels.put("presentation",lPresentation);
		lesLabels.put("dimensions",lDimensions);
		lesLabels.put("editeur",lEditeur);
		lesLabels.put("nbPages",lNbPages);
		lesLabels.put("collection",lCollection);
		lesLabels.put("format",lFormat);
		lesLabels.put("isbn",lISBN);
		lesLabels.put("dewey",lDewey);
		lesLabels.put("statut",lStatut);
		// je ne mets ni résumé ni état parce qu'ils ne sont pas associés à un jtextfield
		// je ne mets pas non plus lStatut parce qu'il n'est... oui pour la même raison en fait.
		
		lResume.setHorizontalAlignment(JLabel.RIGHT);
		
		lesJTF.put("nom",tNom);
		lesJTF.put("auteur",tAuteur);
		lesJTF.put("dateParution",tDateP);
		lesJTF.put("presentation",tPresentation);
		lesJTF.put("dimensions",tDimensions);
		lesJTF.put("editeur",tEditeur);
		lesJTF.put("nbPages",tNbPages);
		lesJTF.put("collection",tCollection);
		lesJTF.put("format",tFormat);
		lesJTF.put("isbn", tISBN);
		lesJTF.put("dewey",tDewey);
		lesJTF.put("statut",tStatut);
		//lesJTF.put("ecole",tEcole);
		
		for (String champ : lesLabels.keySet()) {
			lesJTF.get(champ).setPreferredSize(new Dimension(200,30));
			lesJTF.get(champ).setEditable(false);
			lesLabels.get(champ).setHorizontalAlignment(JLabel.RIGHT);
		}
		
		// mettre la couleur du Dewey dans le JTF
		if (tDewey.getText().length()>=3) {
			if (utils.colorDewey(tDewey.getText())!=null) {
				tDewey.setBackground(utils.colorDewey(tDewey.getText()));
				tDewey.setForeground(utils.couleurQuiSeVoit(tDewey.getBackground()));	
			}
		}
		// un mouseListener pour une nouvelle fenêtre qui indique les couleurs dewey
		tDewey.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent souris ) {
				if (souris.getClickCount()==2) {
					new deweyDetails();
				}
			}
		});
		tDewey.getDocument().addDocumentListener(new DocumentListener() {
			public void removeUpdate(DocumentEvent e) {
				colorize();
			}
			public void insertUpdate(DocumentEvent e) {
				colorize();
			}
			
			public void changedUpdate(DocumentEvent e) {
				colorize();
			}
			
			public void colorize() {
				if ((tDewey.getText().length()>=3)&&(tDewey.getText().matches(".*\\d\\d\\d.*"))) {
					tDewey.setBackground(utils.colorDewey(tDewey.getText().replaceAll("[^0-9]","").substring(0, 3))); // substring n'est probablement pas nécessaire mais bon.
					// hein
					// on ne sait jamais que voulez-vous
					tDewey.setForeground(utils.couleurQuiSeVoit(tDewey.getBackground()));
				}
				else {
					tDewey.setBackground(tNom.getBackground());
					tDewey.setForeground(tNom.getForeground());
				}
			}
		});
		tStatut.getDocument().addDocumentListener(new DocumentListener() {
			public void removeUpdate(DocumentEvent e) {
				colorize();
			}
			public void insertUpdate(DocumentEvent e) {
				colorize();
			}
			public void changedUpdate(DocumentEvent e) {
				colorize();
			}
			public void colorize() {
				if (tStatut.getText().equals("Disponible")) {
					tStatut.setForeground(new Color(40,155,70));
				}
				else {
					tStatut.setForeground(Color.RED);
				}
			}
		});
		tResume.setEditable(false);
    	tResume.setLineWrap(true);
    	tResume.setBackground(tNom.getBackground());tResume.setBackground(tNom.getBackground());
    	tResume.setWrapStyleWord(true);
    	
    	spResume.setBorder(new JTextField().getBorder());
		spResume.setPreferredSize(new Dimension(300,150)); // à l'époque où le setPreferredSize était sur le tResume, on ne pouvait pas scroller.
		
		tResume.setCaretPosition(0);
		
		/*
		 * Remplissage
		 */
		if (details.equals("<--RIEN-->")) {
			JOptionPane.showMessageDialog(null, "Le serveur n'a pas reconnu ce code-barre.", "Erreur de code-barre", JOptionPane.INFORMATION_MESSAGE);
			for (String champ : lesJTF.keySet()) {
				lesJTF.get(champ).setText("");
			}
			tResume.setText("");
		} else {
			for (String chaine : details.split(";;")) {
				if (chaine.split("::")[0].equals("resume")) {
					tResume.setText(chaine.split("::")[1]);
				} else {
					if (chaine.split("::").length>1) {
						lesJTF.get(chaine.split("::")[0]).setText(chaine.split("::")[1]);
					} else {
						lesJTF.get(chaine.split("::")[0]).setText("");
					}
				}
			}
			
		}
		
		
		/*
		 * GridBagLayout
		 */
		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		//Je me prépare des insets
		Insets iGauche = new Insets(0,10,0,3);
		Insets iDroite = new Insets(0,3,0,10);

		c.fill=GridBagConstraints.HORIZONTAL;
		// initialisation avant la boucle
		c.gridx = 0;
		c.gridy = 0;
		// Boucle pour créer le layout en deux deux
		for (String champ : lesLabels.keySet() ) {
			c.insets=iGauche;
			this.add(lesLabels.get(champ),c);
			c.gridx+=1;
			c.insets=iDroite;
			this.add(lesJTF.get(champ),c);
			c.gridx=(c.gridx+1)%4;
			if (c.gridx==0) {
				c.gridy+=1;
			}
		}
				
		c.gridwidth=1;

		c.gridx=0;
		c.gridy+=1;
		c.insets=iGauche;
		this.add(lResume,c);
		c.insets=iDroite;
		c.gridx+=1;
		c.gridwidth=3;
		c.gridheight=2;
		this.add(spResume,c);
		
		// pour les boutons je fais un nouveau panel que je mettrai sur toute la longueur
		
		JPanel panneauBas = new JPanel();
		bFermer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DetailLivre.this.setVisible(false);
			}
		});
		panneauBas.add(bFermer);
		
		c.gridx=0;
		c.gridy+=2;
		c.gridheight=1;
		c.gridwidth=4;
		this.add(panneauBas,c);
		
		this.pack();
		try{
			Image imageIcone = ImageIO.read(new File("resources/detailLivre.png"));
			this.setIconImage(imageIcone);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		this.setResizable(false);
		
		this.setTitle(idLivre+" -- "+tNom.getText());
		this.setLocationRelativeTo(parent);
		
		this.setVisible(true);
	}
	
	class deweyDetails extends JDialog {

		/**
		 * 
		 */
		private static final long serialVersionUID = -8079227387426132548L;
				
		JPanel pTexte = new JPanel();
		JPanel pTout = new JPanel();
		JPanel pBouton = new JPanel();
		
		JButton bFermer = new JButton("Fermer");
		
		JLabel l0 = new JLabel("000 - Informatique, informations, ouvrages généraux");
		JLabel l1 = new JLabel("100 - Philosophie, parapsychologie, psychologie");
		JLabel l2 = new JLabel("200 - Religion");
		JLabel l3 = new JLabel("300 - Sciences sociales");
		JLabel l4 = new JLabel("400 - Langage");
		JLabel l5 = new JLabel("500 - Sciences pures");
		JLabel l6 = new JLabel("600 - Technologie");
		JLabel l7 = new JLabel("700 - Beaux-arts, arts décoratifs, sports");
		JLabel l8 = new JLabel("800 - Littérature");
		JLabel l9 = new JLabel("900 - Géographie, histoire");
		
		Map<String,JLabel> lesJL = new LinkedHashMap<String,JLabel>();
		
		deweyDetails() {
			lesJL.put("000",l0);
			lesJL.put("100",l1);
			lesJL.put("200",l2);
			lesJL.put("300",l3);
			lesJL.put("400",l4);
			lesJL.put("500",l5);
			lesJL.put("600",l6);
			lesJL.put("700",l7);
			lesJL.put("800",l8);
			lesJL.put("900",l9);
			
			try {
				Image imageIcone = ImageIO.read(new File("resources/detailLivre.png"));
				this.setIconImage(imageIcone);
			} catch (Exception e) {
				e.printStackTrace();
			}
		
			bFermer.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					deweyDetails.this.dispose();
				}
			});
			
			pTout.setLayout(new BoxLayout(pTout,BoxLayout.PAGE_AXIS));
			pTexte.setLayout(new GridBagLayout());
			pBouton.setLayout(new FlowLayout());
			
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.fill=GridBagConstraints.BOTH;
			gbc.gridy=0;
			
			for (String d : lesJL.keySet()) {
				lesJL.get(d).setOpaque(true);
				lesJL.get(d).setBackground(utils.colorDewey(d));
				lesJL.get(d).setForeground(utils.couleurQuiSeVoit(utils.colorDewey(d)));
				lesJL.get(d).setBorder(new EmptyBorder(5,5,5,5));
				
				pTexte.add(lesJL.get(d),gbc);
				gbc.gridy+=1;
			}
			
			pBouton.add(bFermer);
			
			pTout.add(pTexte);
			pTout.add(pBouton);
			
			this.add(pTout);
		
			this.setTitle("Dewey simplifiée");
			
			this.pack();
			
			this.setLocationRelativeTo(DetailLivre.this.tDewey);
			this.setVisible(true);
		}
	
		
	}
	
}
