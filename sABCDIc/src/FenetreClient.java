import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.KeyStroke;

public class FenetreClient extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2673281661387551488L;

	CardLayout cl = new CardLayout();
	JPanel pContenu = new JPanel();
	JPanel pBoutons = new JPanel();

	String[] listeActivites = { "carteR", "carteS" };
	int indice = 0;

	public static JToggleButton bRecherche = new JToggleButton();
	public static JToggleButton bScan = new JToggleButton();
	
	public FenetreClient() {

		try {
			Image imageIcone = ImageIO.read(new File("resources/icone.png"));
			this.setIconImage(imageIcone);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// raccourci pour param
		InputMap im = getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
		ActionMap am = getRootPane().getActionMap();

		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_P, KeyEvent.CTRL_DOWN_MASK), "param");
		am.put("param", new AbstractAction() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e) {
				new Parametres();
			}
		});

		
		Recherche carteR = new Recherche();
		Scan carteS = new Scan();
		
		try {
			Image img = ImageIO.read(new File("resources/recherche.png"));
			Image resizedImage = img.getScaledInstance(100, 100, Image.SCALE_SMOOTH);
			bRecherche.setIcon(new ImageIcon(resizedImage));
		} catch (Exception e) {
			e.printStackTrace();
		}
		bRecherche.setToolTipText("Accéder à la fonction de recherche");
		bRecherche.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				cl.show(pContenu, listeActivites[0]);
				if (Scan.ident != null) {
					Scan.ident.dispose();
				}
			}
		});

		try {
			Image img = ImageIO.read(new File("resources/scanner.png"));
			Image resizedImage = img.getScaledInstance(100, 100, Image.SCALE_SMOOTH);
			bScan.setIcon(new ImageIcon(resizedImage));
		} catch (Exception e) {
			e.printStackTrace();
		}
		bScan.setToolTipText("Accéder à la fonction de scan");
		bScan.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				cl.show(pContenu, listeActivites[1]);
			}
		});

		ButtonGroup bgGrosMenu = new ButtonGroup();
		bgGrosMenu.add(bRecherche);
		bgGrosMenu.add(bScan);

		pBoutons.setLayout(new GridLayout(2, 1));

		pBoutons.add(bRecherche);
		pBoutons.add(bScan);

		pContenu.setLayout(cl);
		pContenu.add(carteR, listeActivites[0]);
		pContenu.add(carteS, listeActivites[1]);

		this.getContentPane().add(pBoutons, BorderLayout.WEST);
		this.getContentPane().add(pContenu, BorderLayout.CENTER);

		this.setTitle("sABCDI Client");
		this.setSize(800, 600);
		this.setResizable(true);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setVisible(true);
	}
}
