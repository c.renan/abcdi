import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import javax.swing.JOptionPane;
import javax.swing.ProgressMonitor;
import javax.swing.ProgressMonitorInputStream;
import javax.swing.SwingWorker;

import org.apache.commons.lang.SystemUtils;

public class verifierMAJ extends Thread {
	
	String versionInstallee= null;
	String versionLaPlusRecente = null;
	
	public void run(boolean retour) {
		// System.out.println("C'est parti !");
		
		try {				
			BufferedReader brVer = new BufferedReader(new FileReader("verc"));
			String ligne;
			while ((ligne=brVer.readLine())!=null) {
				versionInstallee = ligne;
			}
			brVer.close();

		} catch (FileNotFoundException fnf) {
			fnf.printStackTrace();
		} catch (IOException io) {
			io.printStackTrace();
		}

		if (versionInstallee!=null) {
			try {
				if (!ALaFraise.paraserv.get("proxy").isEmpty()) {
					System.setProperty("https.proxyHost", ALaFraise.paraserv.get("proxy"));
					System.setProperty("https.proxyPort", ALaFraise.paraserv.get("proxyport"));
				}

				URL myURL = new URL(ALaFraise.paraserv.get("site")+"verc");
				// System.out.println(myURL.toString());
				BufferedReader in = new BufferedReader( new InputStreamReader(myURL.openStream()));
				String inputLigne;
				while ((inputLigne = in.readLine())!=null) {						
					versionLaPlusRecente = inputLigne;
				}
				in.close();

				// ici on teste
				if (!versionInstallee.equals(versionLaPlusRecente)) {
					int choix=JOptionPane.showConfirmDialog(null, "<html>La version "+versionLaPlusRecente+" est disponible.<br>Voulez-vous la télécharger ?</html>", "Nouvelle version disponible", JOptionPane.YES_NO_OPTION);
					if (choix==JOptionPane.YES_OPTION) {
						 SwingWorker<Void, Void> worker = new SwingWorker<Void,Void>() {
							 @Override
							 protected Void doInBackground() throws Exception {
								 doTheJob();
								 return null;
							 }

						 };

						 worker.execute();
					}
				} else {
					if (retour) {
						JOptionPane.showMessageDialog(null, "La version actuelle est la plus à jour !", "À jour", JOptionPane.INFORMATION_MESSAGE);
					}
				}

				if (!ALaFraise.paraserv.get("proxy").isEmpty()) {
					System.clearProperty("https.proxyHost");
				}

			} catch (MalformedURLException murl) {
				JOptionPane.showMessageDialog(null, "<html>Attention, le site comportant les mises à jour ne répond pas.<br>Si cette erreur persiste, contacter le développeur.</html>", "Erreur site", JOptionPane.ERROR_MESSAGE);
			} catch (IOException io) {
				JOptionPane.showMessageDialog(null, "Erreur de lecture/écriture dans les fichiers.", "Erreur", JOptionPane.ERROR_MESSAGE);
				io.printStackTrace();
			}

		 }

	}
	
	public static void main(String args[]) {
	//	(new Thread(new verifierMAJ())).start();
	}
	
	 void doTheJob() {
		 byte[] tampon = new byte[16384]; // 16k
		 int octetsLus;
		 URL myURL;

		 if (!ALaFraise.paraserv.get("proxy").isEmpty()) {
			 System.setProperty("https.proxyHost", ALaFraise.paraserv.get("proxy"));
			 System.setProperty("https.proxyPort", ALaFraise.paraserv.get("proxyport"));
		 }

		 try {
			 myURL = new URL(ALaFraise.paraserv.get("site")+"winInstaller/Installer_sABCDIc.exe");

			 FileOutputStream fos = new FileOutputStream(new File("installeurs/sABCDIc-"+versionLaPlusRecente+".exe"));
			 HttpURLConnection connexion =(HttpURLConnection) myURL.openConnection();
			 connexion.connect();
			 int taille = connexion.getContentLength();

			 InputStream stream = connexion.getInputStream();
			 ProgressMonitorInputStream pmis = new ProgressMonitorInputStream(null, "Téléchargement de la version "+versionLaPlusRecente, stream);
			 ProgressMonitor pm = pmis.getProgressMonitor();
			 pm.setMinimum(0);
			 pm.setMaximum(taille);
			 //System.out.println(taille);
			 int progres = 0;
			 while ((octetsLus=pmis.read(tampon))!=-1) {
				 fos.write(tampon);
				 //System.out.println(octetsLus);
				 progres+=octetsLus;
				 pm.setProgress(progres);
			 }
			 fos.close();
			 pmis.close();
		 } catch (MalformedURLException e1) {
			 JOptionPane.showMessageDialog(null, "<html>Attention, le site à atteindre ne répond pas.<br>Fermer, puis relancer le progrjavaamme.<br> Si cette erreur persiste, contacter le développeur.</html>", "Erreur site", JOptionPane.ERROR_MESSAGE);
			 e1.printStackTrace();
		 } catch (FileNotFoundException e2) {
			 JOptionPane.showMessageDialog(null, "Erreur : fichier non trouvé.", "Erreur", JOptionPane.ERROR_MESSAGE);
			 e2.printStackTrace();
		 } catch (IOException e3) {
			 JOptionPane.showMessageDialog(null, "Erreur de lecture/écriture dans les fichiers.", "Erreur", JOptionPane.ERROR_MESSAGE);
			 e3.printStackTrace();
		 }

		 if (ALaFraise.paraserv.get("proxy").isEmpty()) {
			 System.clearProperty("https.proxyHost");
		 }


		 try {
			 if (SystemUtils.IS_OS_WINDOWS) {
				 //Runtime.getRuntime().exec("explorer.exe /select, \""+System.getProperty("user.dir")+"/installeurs/ABCDI-"+versionLaPlusRecente+".exe\"");
				 Desktop.getDesktop().open(new File(System.getProperty("user.dir")+"/installeurs/sABCDIc-"+versionLaPlusRecente+".exe"));
				 int choix = JOptionPane.showConfirmDialog(null, "<html>Le logiciel doit être redémarré pour que la mise à jour soit effective.<br>Quitter maintenant ?</html>","Redémarrage nécessaire.",JOptionPane.YES_NO_OPTION);
				 if (choix==JOptionPane.YES_OPTION) {
					 System.exit(0);
				 }
			 } else {
				 // complètement débile de faire ça vu que je télécharge un .exe.
				 // mais bon.
				 URI gagarine = new URI("file://"+System.getProperty("user.dir")+"/installeurs/");
				 Desktop bureau = Desktop.getDesktop();
				 bureau.browse(gagarine);
				 int choix = JOptionPane.showConfirmDialog(null, "<html>Le logiciel doit être redémarré pour que la mise à jour soit effective.<br>Quitter maintenant ?</html>","Redémarrage nécessaire.",JOptionPane.YES_NO_OPTION);
				 if (choix==JOptionPane.YES_OPTION) {
					 System.exit(0);
				 }
			 }

		 } catch (IOException io) {
			 io.printStackTrace();
		 } catch (URISyntaxException youri) {
			 youri.printStackTrace();
		 }


	 }

}
