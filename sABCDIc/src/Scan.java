import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class Scan extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	static int idLivre;
	
	JPanel pTip = new JPanel();

	JPanel pTop = new JPanel();
	JPanel pSession = new JPanel();
	
	JPanel pContainCentre = new JPanel();
	JPanel pCentre = new JPanel();
	
	JPanel pBottom = new JPanel();

	static JTextField tScan = new JTextField();

	JButton bScan = new JButton("Recherche");
	JButton bClear = new JButton("Effacer");

	JButton bEmprunter = new JButton("Emprunter");
	JButton bRendre = new JButton("Rendre");
	static JButton bDeco = new JButton("⌫ Déconnexion");

	static JLabel lSession = new JLabel("Connecté en tant que ...");
	
	public static Identification ident;
	
	RPlaceHolderTextField nom = new RPlaceHolderTextField("Nom");
	RPlaceHolderTextField auteur = new RPlaceHolderTextField("Auteur");
	RPlaceHolderTextField dateParution = new RPlaceHolderTextField("Date de parution");
	RPlaceHolderTextField presentation = new RPlaceHolderTextField("Présentation");
	RPlaceHolderTextField dimensions = new RPlaceHolderTextField("Dimensions");
	RPlaceHolderTextField editeur = new RPlaceHolderTextField("Éditeur");
	RPlaceHolderTextField nbPages = new RPlaceHolderTextField("Nombre de pages");
	RPlaceHolderTextField collection = new RPlaceHolderTextField("Collection");
	RPlaceHolderTextField format = new RPlaceHolderTextField("Format");
	RPlaceHolderTextField dewey = new RPlaceHolderTextField("Dewey ou référence");
	RPlaceHolderTextField isbn = new RPlaceHolderTextField("ISBN");

	JTextField tStatut = new JTextField();

	static JTextArea tResume = new JTextArea();

	// Un JScrollPane pour la textarea resume
	JScrollPane spResume = new JScrollPane(tResume, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
			JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

	static Map<String, RPlaceHolderTextField> lesRPHTF = new LinkedHashMap<String, RPlaceHolderTextField>();

	static String idEleve = "";
	
	public Scan() {
		lesRPHTF.put("nom",nom);
		lesRPHTF.put("auteur",auteur);
		lesRPHTF.put("dateParution",dateParution);
		lesRPHTF.put("presentation",presentation);
		lesRPHTF.put("dimensions",dimensions);
		lesRPHTF.put("editeur",editeur);
		lesRPHTF.put("nbPages",nbPages);
		lesRPHTF.put("collection",collection);
		lesRPHTF.put("format",format);
		lesRPHTF.put("isbn",isbn);
		lesRPHTF.put("dewey",dewey);

		for (String champ : lesRPHTF.keySet()) {
			lesRPHTF.get(champ).setPreferredSize(new Dimension(200, 30));
			lesRPHTF.get(champ).setEditable(false);
		}

		// mettre la couleur du Dewey dans le JTF
		if (dewey.texte().length() >= 3) {
			if (utils.colorDewey(dewey.texte()) != null) {
				dewey.setBackground(utils.colorDewey(dewey.texte()));
				dewey.setForeground(utils.couleurQuiSeVoit(dewey.getBackground()));
			}
		}
		// un mouseListener pour une nouvelle fenêtre qui indique les couleurs dewey
		dewey.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent souris) {
				if (souris.getClickCount() == 2) {
					new deweyDetails();
				}
			}
		});
		dewey.getDocument().addDocumentListener(new DocumentListener() {
			public void removeUpdate(DocumentEvent e) {
				colorize();
			}

			public void insertUpdate(DocumentEvent e) {
				colorize();
			}

			public void changedUpdate(DocumentEvent e) {
				colorize();
			}

			public void colorize() {
				if ((dewey.texte().length() >= 3) && (dewey.texte().matches(".*\\d\\d\\d.*"))) {
					dewey.setBackground(utils.colorDewey(dewey.getText().replaceAll("[^0-9]", "").substring(0, 3))); // substring
																														// n'est
																														// probablement
																														// pas
																														// nécessaire
																														// mais
																														// bon.
					// hein
					// on ne sait jamais que voulez-vous
					dewey.setForeground(utils.couleurQuiSeVoit(dewey.getBackground()));
				} else {
					dewey.setBackground(nom.getBackground());
					dewey.setForeground(nom.getForeground());
				}
			}
		});

		tStatut.getDocument().addDocumentListener(new DocumentListener() {
			public void removeUpdate(DocumentEvent e) {
				colorize();
			}

			public void insertUpdate(DocumentEvent e) {
				colorize();
			}

			public void changedUpdate(DocumentEvent e) {
				colorize();
			}

			public void colorize() {
				if (tStatut.getText().equals("Disponible")) {
					tStatut.setForeground(new Color(40, 155, 70));
					bEmprunter.setEnabled(true);
					bRendre.setEnabled(false);
				} 
				else {
					tStatut.setForeground(Color.RED);
					bEmprunter.setEnabled(false);
					bRendre.setEnabled(verifId(idLivre, idEleve));
				}
			}
		});

		tResume.setEditable(false);
		tResume.setBackground(nom.getBackground());
		tResume.setLineWrap(true);
		tResume.setWrapStyleWord(true);

		tResume.setMargin(new Insets(2,2,2,2));
		
		spResume.setBorder(nom.getBorder());
//		spResume.setPreferredSize(new Dimension(300, 150)); // à l'époque où le setPreferredSize était sur le tResume,
															// on ne pouvait pas scroller.

		tResume.setCaretPosition(0);

		tScan.addActionListener(alRecherche);
		bScan.addActionListener(alRecherche);
		
		bClear.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				effaceChamps();
			}
		});

		bEmprunter.addActionListener(alEmprunter);
		bEmprunter.setEnabled(false);
		
		bRendre.addActionListener(alRendre);
		
		bRendre.setEnabled(false);

		/*
		 * Focus request
		 */

		this.addComponentListener(new ComponentListener() {
			public void componentShown(ComponentEvent e) {
				ident = new Identification(Scan.this);
				/*
				 * Scan.this.requestFocusInWindow();
				 * tScan.requestFocus();
				*/
			}

			public void componentResized(ComponentEvent e) {
			}

			public void componentMoved(ComponentEvent e) {
			}

			public void componentHidden(ComponentEvent e) {
			}
		});

		/* layout */

		/*
		 * Le panel Scan contient deux panels : une ligne north et un center avec les
		 * détails.
		 */
		pTop.setLayout(new FlowLayout());
		tScan.setPreferredSize(new Dimension(400, 30));
		pTop.add(tScan);
		pTop.add(bScan);
		pTop.add(bClear);

		pSession.setLayout(new FlowLayout());
		pSession.add(lSession);
		bDeco.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				deconnecte();
			}
		});
		bDeco.setEnabled(false);
		pSession.add(bDeco);
		
		pTip.setLayout(new BoxLayout(pTip, BoxLayout.PAGE_AXIS));
		pTip.add(pTop);
		pTip.add(pSession);
		
		/*
		 * GridLayout
		 */

		pContainCentre.setLayout(new BoxLayout(pContainCentre,BoxLayout.PAGE_AXIS));
		
		pCentre.setLayout(new GridLayout(4,3));
		//pCentre.setBorder(BorderFactory.createLineBorder(Color.BLACK));

		for (String champ:lesRPHTF.keySet()) {
			pCentre.add(lesRPHTF.get(champ));
		}
		pCentre.add(tStatut);
		
		pContainCentre.add(pCentre);
		pContainCentre.add(tResume);
		
		pBottom.setLayout(new FlowLayout());
		pBottom.add(bEmprunter);
		pBottom.add(bRendre);

		this.setLayout(new BorderLayout());

		this.add(pTip,BorderLayout.NORTH);
		this.add(pContainCentre,BorderLayout.CENTER);
		this.add(pBottom,BorderLayout.SOUTH);

	}

	class deweyDetails extends JDialog {

		/**
		 * 
		 */
		private static final long serialVersionUID = -8079227387426132548L;

		JPanel pTexte = new JPanel();
		JPanel pTout = new JPanel();
		JPanel pBouton = new JPanel();

		JButton bFermer = new JButton("Fermer");

		JLabel l0 = new JLabel("000 - Informatique, informations, ouvrages généraux");
		JLabel l1 = new JLabel("100 - Philosophie, parapsychologie, psychologie");
		JLabel l2 = new JLabel("200 - Religion");
		JLabel l3 = new JLabel("300 - Sciences sociales");
		JLabel l4 = new JLabel("400 - Langage");
		JLabel l5 = new JLabel("500 - Sciences pures");
		JLabel l6 = new JLabel("600 - Technologie");
		JLabel l7 = new JLabel("700 - Beaux-arts, arts décoratifs, sports");
		JLabel l8 = new JLabel("800 - Littérature");
		JLabel l9 = new JLabel("900 - Géographie, histoire");

		Map<String, JLabel> lesJL = new LinkedHashMap<String, JLabel>();

		deweyDetails() {
			lesJL.put("000", l0);
			lesJL.put("100", l1);
			lesJL.put("200", l2);
			lesJL.put("300", l3);
			lesJL.put("400", l4);
			lesJL.put("500", l5);
			lesJL.put("600", l6);
			lesJL.put("700", l7);
			lesJL.put("800", l8);
			lesJL.put("900", l9);

			try {
				Image imageIcone = ImageIO.read(new File("resources/detailLivre.png"));
				this.setIconImage(imageIcone);
			} catch (Exception e) {
				e.printStackTrace();
			}

			bFermer.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					deweyDetails.this.dispose();
				}
			});

			pTout.setLayout(new BoxLayout(pTout, BoxLayout.PAGE_AXIS));
			pTexte.setLayout(new GridBagLayout());
			pBouton.setLayout(new FlowLayout());

			GridBagConstraints gbc = new GridBagConstraints();
			gbc.fill = GridBagConstraints.BOTH;
			gbc.gridy = 0;

			for (String d : lesJL.keySet()) {
				lesJL.get(d).setOpaque(true);
				lesJL.get(d).setBackground(utils.colorDewey(d));
				lesJL.get(d).setForeground(utils.couleurQuiSeVoit(utils.colorDewey(d)));
				lesJL.get(d).setBorder(new EmptyBorder(5, 5, 5, 5));

				pTexte.add(lesJL.get(d), gbc);
				gbc.gridy += 1;
			}

			pBouton.add(bFermer);

			pTout.add(pTexte);
			pTout.add(pBouton);

			this.add(pTout);

			this.setTitle("Dewey simplifiée");

			this.pack();

			this.setLocationRelativeTo(Scan.this.dewey);
			this.setVisible(true);
		}

	}

	ActionListener alRecherche = new ActionListener() {

		public void actionPerformed(ActionEvent e) {
			String resultats = utils.connexionServeur(ALaFraise.paraserv.get("ip"), Integer.parseInt(ALaFraise.paraserv.get("port")), "S=" + utils.sixChiffrise(tScan.getText()));

			if (resultats.equals("<--RIEN-->")) {
				JOptionPane.showMessageDialog(null, "Le serveur n'a pas reconnu ce code-barre.", "Erreur de code-barre",
						JOptionPane.INFORMATION_MESSAGE);
				effaceChamps();
			} else {
				idLivre = Integer.parseInt(tScan.getText());
				for (String chaine : resultats.split(";;")) {
					if (chaine.split("::")[0].equals("resume")) {
						tResume.setText(chaine.split("::")[1]);
					} else if (chaine.split("::")[0].equals("statut")) {
						tStatut.setText(chaine.split("::")[1]);
					}
					else {
						if (chaine.split("::").length > 1) {
							lesRPHTF.get(chaine.split("::")[0]).changeTexte(chaine.split("::")[1]);
						} else {
							lesRPHTF.get(chaine.split("::")[0]).efface();
						}
					}
				}

			}

		}
	};

	ActionListener alRendre = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			String reussiteOperation = utils.connexionServeur(ALaFraise.paraserv.get("ip"), Integer.parseInt(ALaFraise.paraserv.get("port")), "Z="+utils.sixChiffrise(String.valueOf(idLivre)));
						
			if (reussiteOperation.equals("1")) {
				JOptionPane.showMessageDialog(Scan.this, "Le livre a été rendu, il faut maintenant le ranger");
				effaceChamps();
			} else {
				JOptionPane.showMessageDialog(Scan.this, "Erreur : "+reussiteOperation);
			}
			
		}
	};
	
	ActionListener alEmprunter = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			String reussiteOperation = utils.connexionServeur(ALaFraise.paraserv.get("ip"), Integer.parseInt(ALaFraise.paraserv.get("port")), "E="+idEleve+",L="+utils.sixChiffrise(String.valueOf(idLivre)));
						
			if (reussiteOperation.equals("1")) {
				JOptionPane.showMessageDialog(Scan.this, "C'est bon, l'emprunt est réalisé.", "Emprunt effectué", JOptionPane.INFORMATION_MESSAGE);
			} else {
				JOptionPane.showMessageDialog(Scan.this, "Erreur : "+reussiteOperation);
			}
			
			effaceChamps();
			
			bRendre.setEnabled(false);
			
		}
	};

	static public void effaceChamps() {
		for (String champ : lesRPHTF.keySet()) {
			lesRPHTF.get(champ).efface();
		}
		tResume.setText("");
		tScan.setText("");
		tScan.requestFocus();
		idLivre=0;
	};
	
	public static void connecte(String id) {
		idEleve= id;
		lSession.setText("Connecté en tant que "+ALaFraise.listEleves.get(id));
		bDeco.setEnabled(true);
		tScan.requestFocus();
	}
	
	public void deconnecte() {
		idEleve = "";
		lSession.setText("Connecté en tant que ...");
		bDeco.setEnabled(false);
		effaceChamps();
		new Identification(Scan.this);
	}
	
	boolean verifId(int idLivre, String idEleve) {
		String reussiteOperation = utils.connexionServeur(ALaFraise.paraserv.get("ip"), Integer.parseInt(ALaFraise.paraserv.get("port")), "V="+idEleve+",W="+utils.sixChiffrise(String.valueOf(idLivre)));		
		
		if (reussiteOperation.equals("vrai")) {
			return true;
		} else {
			return false;
		}

	}
}
