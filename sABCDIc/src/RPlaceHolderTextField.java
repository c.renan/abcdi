import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JTextField;


public class RPlaceHolderTextField extends JTextField implements FocusListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	boolean boule = false;
	String parametre = "";
	
	public RPlaceHolderTextField(String text) {
		super(text); // super en fait c'est le constructeur de JTextField ici.
		parametre = text;
		this.setForeground(Color.gray);
		this.addFocusListener(this);
	}
		
	public void focusGained(FocusEvent e) {
		if (!boule) {
			boule = true;
			this.setForeground(Color.black);
			this.setText("");
		}	
	}
	
	public void focusLost(FocusEvent e) {
		if (this.getText().isEmpty()) {
			this.setText(parametre);
			this.setForeground(Color.gray);
			boule = false;
		}
	}
	
	public void changeTexte(String nouveauTexte) {
		this.setText(nouveauTexte);
		this.setForeground(Color.black);
		boule = true;
	}
	
	public boolean isVide() {
		return !boule;
	}
	
	public String texte() {
		if (boule) {
			return this.getText();
		}
		else
		{
			return "";
		}
	}
	
	public void efface() {
		if (boule) {
			this.setText(parametre);
			this.setForeground(Color.gray);
			boule = false;
		}			
	}
}
