#!/bin/bash

cd /home/crd/
if [ "$#" -eq 0 ]
then
    ancver=`cat ./output/sABCDIc/verc`
    echo "La version actuelle est $ancver."
    echo "Quelle va être la nouvelle version ?"
    read nouvver
    echo "La nouvelle version sera $nouvver, vous êtes sûr ? [y/N]"
    read yn
    if [ "$yn" = "y" ]||[ "$yn" = "Y" ]||[ "$yn" = "yes" ]||[ "$yn" = "oui" ]
    then
	sed "s/VERSION/$nouvver/" config_sABCDIc_pourVERSION.xml > config_sABCDIc.xml
	echo $nouvver > ./output/sABCDIc/verc

	echo "Il faut lancer ./sABCDIc.sh maintenant."
    else
	echo "La version n'a pas été changée."
    fi
fi

