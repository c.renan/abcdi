\babel@toc {french}{}
\contentsline {section}{\numberline {1}Installation, lancement et désinstallation}{5}{section.1}%
\contentsline {section}{\numberline {2}Compatibilité}{6}{section.2}%
\contentsline {section}{\numberline {3}Mise à jour}{6}{section.3}%
\contentsline {section}{\numberline {4}Principe d'utilisation}{7}{section.4}%
\contentsline {subsection}{\numberline {4.1}Idée principale}{7}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Mise en place}{7}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}Restriction d'utilisation}{7}{subsection.4.3}%
\contentsline {section}{\numberline {5}Les cinq parties du logiciel}{8}{section.5}%
\contentsline {subsection}{\numberline {5.1}Recherche}{8}{subsection.5.1}%
\contentsline {subsubsection}{\numberline {5.1.1}Cliquer sur les résultats de la recherche}{8}{subsubsection.5.1.1}%
\contentsline {subsubsection}{\numberline {5.1.2}Utilisation du champ «~âge~» dans la recherche}{9}{subsubsection.5.1.2}%
\contentsline {subsection}{\numberline {5.2}Prêt}{10}{subsection.5.2}%
\contentsline {subsection}{\numberline {5.3}Retour}{11}{subsection.5.3}%
\contentsline {subsection}{\numberline {5.4}Ajout de livre}{11}{subsection.5.4}%
\contentsline {subsubsection}{\numberline {5.4.1}Remplissage automatique}{11}{subsubsection.5.4.1}%
\contentsline {subsubsection}{\numberline {5.4.2}Le résumé}{12}{subsubsection.5.4.2}%
\contentsline {subsubsection}{\numberline {5.4.3}Les champs à entrer manuellement}{12}{subsubsection.5.4.3}%
\contentsline {subsection}{\numberline {5.5}Édition de codes-barres}{13}{subsection.5.5}%
\contentsline {subsection}{\numberline {5.6}Le journal client}{13}{subsection.5.6}%
\contentsline {section}{\numberline {6}Les menus}{15}{section.6}%
\contentsline {subsection}{\numberline {6.1}Base de données}{15}{subsection.6.1}%
\contentsline {subsection}{\numberline {6.2}Élèves}{15}{subsection.6.2}%
\contentsline {subsection}{\numberline {6.3}Paramètres}{16}{subsection.6.3}%
\contentsline {subsection}{\numberline {6.4}Aide}{16}{subsection.6.4}%
\contentsline {section}{\numberline {7}La fenêtre de paramètres}{17}{section.7}%
\contentsline {subsection}{\numberline {7.1}Écoles}{17}{subsection.7.1}%
\contentsline {subsection}{\numberline {7.2}Recherche}{17}{subsection.7.2}%
\contentsline {subsection}{\numberline {7.3}Livres}{17}{subsection.7.3}%
\contentsline {subsection}{\numberline {7.4}Élèves}{18}{subsection.7.4}%
\contentsline {subsection}{\numberline {7.5}Impression}{18}{subsection.7.5}%
\contentsline {subsection}{\numberline {7.6}Paramètres avancés}{19}{subsection.7.6}%
\contentsline {subsection}{\numberline {7.7}Mise à jour}{19}{subsection.7.7}%
\contentsline {section}{\numberline {8}Les fenêtres supplémentaires}{20}{section.8}%
\contentsline {subsection}{\numberline {8.1}Détails d'un livre}{20}{subsection.8.1}%
\contentsline {subsection}{\numberline {8.2}Détails d'un élève}{20}{subsection.8.2}%
\contentsline {section}{\numberline {9}Client sABCDI}{22}{section.9}%
\contentsline {subsection}{\numberline {9.1}Recherche}{22}{subsection.9.1}%
\contentsline {subsection}{\numberline {9.2}Scan}{22}{subsection.9.2}%
\contentsline {subsection}{\numberline {9.3}Paramètres réseaux}{23}{subsection.9.3}%
\contentsline {section}{\numberline {10}FAQ}{24}{section.10}%
\contentsline {section}{Index}{25}{section*.3}%
