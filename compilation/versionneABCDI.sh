#!/bin/bash

cd /home/crd/
if [ "$#" -eq 0 ]
then
    ancver=`cat ./output/ABCDI/ver`
    echo "La version actuelle est $ancver."
    echo "Quelle va être la nouvelle version ?"
    read nouvver
    echo "La nouvelle version sera $nouvver, vous êtes sûr ? [y/N]"
    read yn
    if [ "$yn" = "y" ]||[ "$yn" = "Y" ]||[ "$yn" = "yes" ]||[ "$yn" = "oui" ]
    then
	sed "s/VERSION/$nouvver/" config_ABCDI_pourVERSION.xml > config_ABCDI.xml
	echo $nouvver > ./output/ABCDI/ver

	cp ./output/ABCDI/ver ./output/patchABCDI/

	cd ./LaTeX/
	pdflatex ABCDI_aide.tex

	cp ./LaTeX/ABCDI_aide.pdf ./Bureau/ABCDI/help
	cp ./LaTeX/ABCDI_aide.pdf ./Bureau/patchABCDI/help
	./launch4j/launch4j ./config_ABCDI.xml

	cd ../output/
	makensis SCRIPT_PATCH_ABCDI.nsi
    else
	echo "La version n'a pas été changée."
    fi
fi

