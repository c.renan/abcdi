#include MUI
!include MUI.nsh

!define MUI_ICON "ABCDI-icone.ico"
!define MUI_UNICON "ABCDI-icone-uninstall.ico"
!define MUI_HEADERIMAGE
!define MUI_HEADERIMAGE_BITMAP "ABCDI_header_200x100.bmp"
!define MUI_HEADERIMAGE_RIGHT

# le nom
Name "ABCDI"

# define the name of the installer
Outfile "Installer ABCDI.exe"

# installdir par defaut
InstallDir "$LOCALAPPDATA\ABCDI"
  
;Get installation folder from registry if available
InstallDirRegKey HKCU "Software\ABCDI" "Grosse patate pourrie"

!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

!insertmacro MUI_LANGUAGE "French"

# je cree une variable
Var /GLOBAL variable

# default section
Section

  # define the output path for this file
  SetOutPath "$INSTDIR"

  # je copie le dossier
  File /r "ABCDI"

  # je fais un désinstalleur
  WriteUninstaller "$INSTDIR\ABCDI\Desinstaller ABCDI.exe"

  # et un raccourci sur le bureau
  CreateShortCut "$DESKTOP\ABCDI.lnk" "$INSTDIR\ABCDI\ABCDI.exe"

  # et une entree dans la base de registres
  WriteRegStr HKCU "Software\ABCDI" "Grosse patate pourrie" $INSTDIR

SectionEnd

Section "Uninstall"
  ReadRegStr $variable HKCU "Software\ABCDI" "Grosse patate pourrie"
  
  Delete "$variable\ABCDI\Desinstaller ABCDI.exe"

  RMDir /r "$variable\ABCDI"

  # ici je supprime le dossier. Ca ne supprime rien s'il n'est pas vide.
  # puisque j'ai enlevé /r
  RMDir "$variable"

  # c'est IfFileExists <nomFichier> <jump_if_present> [<jump_otherwise>]
  IfFileExists "$DESKTOP\ABCDI.lnk" 0 +2
  Delete "$DESKTOP\ABCDI.lnk"

  DeleteRegKey /ifempty HKCU "Software\ABCDI"

SectionEnd
