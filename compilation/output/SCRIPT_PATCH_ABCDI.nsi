#include MUI
!include MUI.nsh

!define MUI_ICON "ABCDI-icone.ico"
!define MUI_HEADERIMAGE
!define MUI_HEADERIMAGE_BITMAP "ABCDI_header_200x100.bmp"
!define MUI_HEADERIMAGE_RIGHT

# le nom
Name "Patch ABCDI"

Var /GLOBAL variable

# define the name of the installer
Outfile "patchABCDI.exe"

# installdir par defaut

InstallDir "$variable"
  
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_LANGUAGE "French"

# default section
Section
  ReadRegStr $variable HKCU "Software\ABCDI" "Grosse patate pourrie"	
  
  # define the output path for this file
  SetOutPath "$variable\ABCDI"

  # je copie le fichier et la version
  File "ABCDI\ABCDI.exe"

  # Si je dois rajouter un fichier il faut aller les chercher dans le dossier patch
  File /r patch\*.*

SectionEnd

