import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EcrireParametres {
	
	public static Map<String,List<String>> Lire() {
		Map<String,List<String>> sortie = new HashMap<String,List<String>>();
		
		try {
			File parFile = new File("parametres.ini");
			parFile.setReadable(true); // on ne sait jamais...
			
			BufferedReader brFlux = new BufferedReader(new FileReader("parametres.ini"));
					
			String nomCat="";
			List<String> cetteCategorie = new ArrayList<String>();
			
			String ligne;
			while ((ligne=brFlux.readLine())!=null) {
				if (ligne.matches("\\[([a-z])+\\]")) {
					if (!nomCat.isEmpty()) {
						List<String> localCat = new ArrayList<String>();
						localCat.addAll(cetteCategorie);
						sortie.put(nomCat, localCat);
					}
					nomCat = ligne.substring(1, ligne.length()-1);
					cetteCategorie.clear();
				}
				else {
					if (!ligne.isEmpty()) {
						cetteCategorie.add(ligne);
					}
				}
			}
			sortie.put(nomCat, cetteCategorie);
			
			brFlux.close();
			
			return sortie;
			
		}
		catch (FileNotFoundException fnf) {
			fnf.printStackTrace();
		}
		catch (IOException io) {
			io.printStackTrace();
		}
		
		return null;
	}
	
	public static void Ajouter(String categorie, String valeur) {
		try {
			File parFile = new File("parametres.ini");
			parFile.setWritable(true);
			
			BufferedWriter bw = new BufferedWriter(new FileWriter("parametres.ini"));
			
			for (String curCat : AuChocolat.parametres.keySet()) {
				bw.write("["+curCat+"]\n");
				for (String curField : AuChocolat.parametres.get(curCat)) {
					bw.write(curField+"\n");
				}
				if (curCat.equals(categorie)) {
					bw.write(valeur+"\n");
				}
				bw.write("\n");
			}
			
			bw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException io) {
			io.printStackTrace();
		}
		
		AuChocolat.parametres = Lire();
	}
	
	public static void Changer(String categorie, String champ, String valeur) {
		try {
			File parFile = new File("parametres.ini");
			parFile.setWritable(true);
			
			BufferedWriter bw = new BufferedWriter(new FileWriter("parametres.ini"));
			
			for (String curCat : AuChocolat.parametres.keySet()) {
				bw.write("["+curCat+"]\n");
				for (String curField : AuChocolat.parametres.get(curCat)) {
					if (curCat.equals(categorie) && curField.matches(champ+"=(([,\\{\\}A-Za-z0-9 -/:])+)?")) {
						bw.write(champ+"="+valeur+"\n");
					}
					else {
						bw.write(curField+"\n");
					}
					
				}
				bw.write("\n");
			}
			bw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException io) {
			io.printStackTrace();
		}
		
		AuChocolat.parametres = Lire();
	}
	
	public static void Supprimer(String categorie, String valeur) {
		try {
			File parFile = new File("parametres.ini");
			parFile.setWritable(true);
			
			BufferedWriter bw = new BufferedWriter(new FileWriter("parametres.ini"));
			
			for (String curCat : AuChocolat.parametres.keySet()) {
				bw.write("["+curCat+"]\n");
				for (String curField : AuChocolat.parametres.get(curCat)) {
					if (!(curCat.equals(categorie) && curField.equals(valeur))) {
						bw.write(curField+"\n");
					}
				}
				bw.write("\n");
			}
			bw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException io) {
			io.printStackTrace();
		}
		
		AuChocolat.parametres = Lire();
	}
	
}
