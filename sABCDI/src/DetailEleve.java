import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.RowSorter;
import javax.swing.RowSorter.SortKey;
import javax.swing.SortOrder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;


public class DetailEleve extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	JLabel lNom = new JLabel("Nom");
	JLabel lPrenom = new JLabel("Prénom");
	JLabel lDateNaiss = new JLabel("Date de naissance");
	JLabel lEcole = new JLabel("École");
	JLabel lClasse = new JLabel("Classe");
	JLabel lStatut = new JLabel("Statut");
	JLabel lId = new JLabel("Id");
	
	JTextField tNom = new JTextField();
	JTextField tPrenom = new JTextField();
	JTextField tDateNaiss = new JTextField();
	JTextField tClasse = new JTextField();
	JTextField tId = new JTextField();
	
	JComboBox<String> cbEcole = new JComboBox<String>();
	
	JTextField tStatut = new JTextField();
	
	// cb pour le statut
	String[] comboString = {"", "Parti"};
	JComboBox<String> cbStatut = new JComboBox<String>(comboString);
		
	JLabel lEmprunts = new JLabel("Livres empruntés");
	String[] titre = {"Id", "Id Livre", "Livre", "Date d'emprunt", "Revenu ?"};
	DefaultTableModel dtmEmprunts = new DefaultTableModel(null,titre){
		private static final long serialVersionUID = 35383773L;
		@SuppressWarnings({ "unchecked", "rawtypes" }) // ça n'a pas l'air très bien de faire ça.
		// Pas très class. C'est une blague parce que le problème vient de class justement.
		@Override
        public Class getColumnClass(int column) {
            switch (column) {
                case 0:
                    return Integer.class;
                default:
                    return Object.class;
            }
        }
		@Override
		public boolean isCellEditable(int row, int column) {
			return false; // C'est quand même compliqué pour pas grand chose...
		}
	};
	// je vais essayer de Sort
	TableRowSorter<DefaultTableModel> trieur = new TableRowSorter<DefaultTableModel>(dtmEmprunts);
	List<RowSorter.SortKey> clesDeTri = new ArrayList<SortKey>(25); // pourquoi 25 ? C'est le nombre de clés de tri possible. Donc avec trois colonnes, je pense qu'on est larges...
	
	JTable tableEmprunts = new JTable(dtmEmprunts);
	JScrollPane jspEmprunts = new JScrollPane(tableEmprunts);
	
	Map<String,JTextField> lesJTF = new LinkedHashMap<String,JTextField>();
	Map<String,JLabel> lesLabels = new LinkedHashMap<String,JLabel>();
	
	JButton bFermer = new JButton("Fermer");
	JButton bEnregistrer = new JButton("Enregistrer les modifications");
	
	public DetailEleve(final int idEleve) {
		// je me fais un petit keylistener pour fermer avec escape
		/*
		 * Donc il paraît qu'il ne faut pas utiliser de Keylistener en général, mais des keybindings.
		 */
		InputMap im=getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
		ActionMap am = getRootPane().getActionMap();
		
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "cancel");
		am.put("cancel", new AbstractAction() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
		// je sort je sort
		tableEmprunts.setRowSorter(trieur);
		clesDeTri.add(new RowSorter.SortKey(0, SortOrder.DESCENDING));
			
		trieur.setSortKeys(clesDeTri);
		
		Map<String,Object> infoEleve = ConnectionSQL.selectionEleveParId(idEleve);
		
		// je fais mes petites collections j'aime bien.
		// Je pense qu'il y a un moyen plus simple de les faire aussi...
		// Mais bon.
		lesLabels.put("nom",lNom);
		lesLabels.put("prenom",lPrenom);
		lesLabels.put("dateNaissance",lDateNaiss);
		lesLabels.put("classe",lClasse);
		lesLabels.put("id", lId);
						
		lesJTF.put("nom",tNom);
		lesJTF.put("prenom",tPrenom);
		lesJTF.put("dateNaissance",tDateNaiss);
		lesJTF.put("classe",tClasse);
		lesJTF.put("id", tId);
		
		for (String champ : lesLabels.keySet() ) {
			lesLabels.get(champ).setHorizontalAlignment(JLabel.RIGHT);
			if (champ.equals("id")) {
				lesJTF.get(champ).setText(String.valueOf(idEleve));
				lesJTF.get(champ).setEnabled(false);
				lesJTF.get(champ).setPreferredSize(new Dimension(200,30));
			} else {
				lesJTF.get(champ).setText(infoEleve.get(champ).toString());
				lesJTF.get(champ).setPreferredSize(new Dimension(200,30));
			}
		}
		
		lEcole.setHorizontalAlignment(JLabel.RIGHT);
		lStatut.setHorizontalAlignment(JLabel.RIGHT);
		lEmprunts.setHorizontalAlignment(JLabel.RIGHT);
		
		cbEcole.setPreferredSize(new Dimension(200,30));
		for (String uneEcole : FonctionsUtiles.listeDesEcoles()) {
			cbEcole.addItem(uneEcole);
		}
		cbEcole.setSelectedItem(infoEleve.get("ecole").toString());
			
		// je mets un mouselistener sur le tableEmprunts pour accéder au dit livre
		tableEmprunts.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent souris ) {
				JTable jt = (JTable) souris.getSource();
				Point p = souris.getPoint();
				int row= jt.rowAtPoint(p);
				if (souris.getClickCount()==2) {
					new DetailLivre(Integer.parseInt(jt.getValueAt(row, 1).toString()));
				}
			}
		});

		// statut
		cbStatut.setPreferredSize(new Dimension(200,30));
		cbStatut.setSelectedItem(infoEleve.get("statut").toString());

		
		// ici il faut initialiser le Table
		tableEmprunts.getColumnModel().getColumn(0).setMaxWidth(30);
		tableEmprunts.getColumnModel().getColumn(1).setMaxWidth(60);
		tableEmprunts.getColumnModel().getColumn(3).setMaxWidth(120);
		tableEmprunts.getColumnModel().getColumn(4).setMaxWidth(60);
		List<Map<String,Object>> loEmprunts = ConnectionSQL.selectionEmprunts(0, idEleve);
		// le 0 de selectionEmprunts indique que je fais ma recherche sur les élèves et non pas sur les livres.
		dtmEmprunts.setRowCount(0);
	
		for (int j=0;j<loEmprunts.size();j++) {
			String leLivre = ConnectionSQL.selectionLivreParId(Integer.parseInt(loEmprunts.get(j).get("idlivre").toString())).get("nom").toString();
			dtmEmprunts.addRow(new Object[]{Integer.parseInt(loEmprunts.get(j).get("id").toString()),loEmprunts.get(j).get("idlivre"),leLivre,loEmprunts.get(j).get("dateemprunt"),loEmprunts.get(j).get("revenu")});
			//dtmEmprunts.addRow(new Object[]{Integer.parseInt(loEmprunts.get(j).get("id").toString()),loEmprunts.get(j).get("idlivre"),leLivre,loEmprunts.get(j).get("dateemprunt"),loEmprunts.get(j).get("revenu")});
		}
		// j'ai l'impression que j'aurais pu faire beaucoup mieux avec une jointure interne mais bon.
		// un truc du genre "SELECT e.rowid,l.nom, e.date FROM emprunts AS e INNER JOIN livres AS l WHERE e.ideleve=##IDELEVE AND e.idlivre=l.rowid;"
		// C'est con parce que je sais le faire quoi.
		
		jspEmprunts.setBorder(new JTextField().getBorder());
		jspEmprunts.setPreferredSize(new Dimension(300,150));
		
	
		/*
		 * GridBagLayout
		 */
		this.setLayout(new GridBagLayout());
		
		GridBagConstraints c = new GridBagConstraints();
		Insets iGauche = new Insets(0, 10, 0, 3);
		Insets iDroite = new Insets(0, 3, 0, 10);
		
		c.fill=GridBagConstraints.HORIZONTAL;
		
		c.gridx = 0;
		c.gridy=0;
		for (String champ : lesLabels.keySet()) {
			c.insets=iGauche;
			this.add(lesLabels.get(champ),c);
			c.gridx+=1;
			c.insets=iDroite;
			this.add(lesJTF.get(champ),c);
			c.gridx=(c.gridx+1)%4;
			if (c.gridx==0) {
				c.gridy +=1;
			}
		}
		
		c.gridx=0;
		c.gridy+=1;
		c.insets=iGauche;
		this.add(lEcole,c);
		c.gridx+=1;
		c.insets=iDroite;
		this.add(cbEcole,c);
		
		c.gridx+=1;
		c.insets=iGauche;
		this.add(lStatut,c);
		c.gridx+=1;
		c.insets=iDroite;
		this.add(cbStatut,c);
		
		c.gridx=0;
		c.gridy+=1;
		c.insets=iGauche;
		this.add(lEmprunts,c);
		c.insets=iDroite;
		c.gridx+=1;
		c.gridwidth=3;
		c.gridheight=2;
		this.add(jspEmprunts,c);
		
		JPanel panneauBas = new JPanel();
		
		bFermer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DetailEleve.this.setVisible(false);
				
			}
		});
		panneauBas.add(bFermer);
		
		bEnregistrer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (tNom.getText().isEmpty() || tPrenom.getText().isEmpty()) {
					JOptionPane.showMessageDialog(DetailEleve.this, "L'élève doit avoir au moins un nom et un prénom.", "Erreur", JOptionPane.ERROR_MESSAGE);
					if (tNom.getText().isEmpty()) tNom.requestFocus();
					else tPrenom.requestFocus();
				}
				else {
					if (tDateNaiss.getText().isEmpty() || FonctionsUtiles.estUneDateCorrecte(tDateNaiss.getText())) {
						Map<String, Object> informations = new HashMap<String, Object>();

						informations.put("nom",tNom.getText());
						informations.put("prenom", tPrenom.getText());
						informations.put("dateNaissance", tDateNaiss.getText());
						informations.put("classe", tClasse.getText());
						informations.put("ecole",cbEcole.getSelectedItem());
						informations.put("statut",cbStatut.getSelectedItem().toString());

						ConnectionSQL.updatationEleve(idEleve, informations);

						JOptionPane.showMessageDialog(DetailEleve.this, "La fiche "+tNom.getText()+" "+tPrenom.getText()+" a bien été enregistrée.", "Fiche enregistrée", JOptionPane.INFORMATION_MESSAGE);
						DetailEleve.this.setVisible(false);
					}
					else
						JOptionPane.showMessageDialog(DetailEleve.this, "La fiche n'a pas été enregistrée : "+tDateNaiss.getText()+" n'est pas une date correcte.", "Problème de date", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		panneauBas.add(bEnregistrer);
		
		c.gridx=0;
		c.gridy+=2;
		c.gridheight=1;
		c.gridwidth=4;
		this.add(panneauBas,c);

		try{
			Image imageIcone = ImageIO.read(new File("resources/detailEleve.png"));
			this.setIconImage(imageIcone);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		this.setTitle(infoEleve.get("nom").toString()+" "+infoEleve.get("prenom").toString()+" (id : "+String.valueOf(idEleve)+")");
		this.pack();
		this.setLocationRelativeTo(null);
				
		this.setVisible(true);
	}
}
