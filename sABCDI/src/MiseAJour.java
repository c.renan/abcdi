import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class MiseAJour extends JPanel {

	/**
	 * 
	 */
	 private static final long serialVersionUID = 1L;

	 JLabel lVersionActuelle = new JLabel();
	 
	 JLabel lStatutMiseAJour = new JLabel("Cliquer sur le bouton suivant pour vérifier les mises à jour.");

	 JPanel pVersion = new JPanel();
	 JPanel pStatut = new JPanel();

	 JButton bVerif = new JButton("Vérifier");

	 JPanel pBouton = new JPanel();

	 String versionInstallee = null;
	 String versionLaPlusRecente = null;

	 ActionListener alVerif = new ActionListener() {
		 public void actionPerformed(ActionEvent e) {
			 verifierMAJ vm = new verifierMAJ();
			 vm.run(true);
			 
/*			 if (bVerif.getText().equals("Vérifier")) {
				 bVerif.setText("Mettre à jour");
				 bVerif.setEnabled(false);

				 try {				
					 BufferedReader brVer = new BufferedReader(new FileReader("ver"));
					 String ligne;
					 while ((ligne=brVer.readLine())!=null) {
						 versionInstallee = ligne;
					 }
					 brVer.close();

				 } catch (FileNotFoundException fnf) {
					 fnf.printStackTrace();
				 } catch (IOException io) {
					 io.printStackTrace();
				 }

				 if (versionInstallee!=null) {
					 try {
						 if (!FonctionsUtiles.LireChamp("site", "proxy").isEmpty()) {
							 System.setProperty("https.proxyHost", FonctionsUtiles.quelProxy());
							 System.setProperty("https.proxyPort", FonctionsUtiles.quelPort());
						 }

						 URL myURL = new URL(FonctionsUtiles.LireChamp("site", "url")+"ver");
						 BufferedReader in = new BufferedReader( new InputStreamReader(myURL.openStream()));
						 String inputLigne;
						 while ((inputLigne = in.readLine())!=null) {						
							 versionLaPlusRecente = inputLigne;
						 }
						 in.close();

						 // ici on teste
						 if (versionInstallee.equals(versionLaPlusRecente)) {
							 lStatutMiseAJour.setText("La version installée est déjà la plus récente.");
						 }
						 else {
							 lStatutMiseAJour.setText("La version "+versionLaPlusRecente+" est disponible. Cliquer sur le bouton suivant pour l'installer.");
							 bVerif.setEnabled(true);
						 }

						 if (!FonctionsUtiles.LireChamp("site", "proxy").isEmpty()) {
							 System.clearProperty("https.proxyHost");
						 }

					 } catch (MalformedURLException murl) {
						 JOptionPane.showMessageDialog(MiseAJour.this, "<html>Attention, le site à atteindre ne répond pas.<br>Fermer, puis relancer le programme.<br> Si cette erreur persiste, contacter le développeur.</html>", "Erreur site", JOptionPane.ERROR_MESSAGE);
					 } catch (IOException io) {
						 JOptionPane.showMessageDialog(MiseAJour.this, "Erreur de lecture/écriture dans les fichiers.", "Erreur", JOptionPane.ERROR_MESSAGE);
					 }

				 }
			 }
			 else {
				 SwingWorker<Void, Void> worker = new SwingWorker<Void,Void>() {
					 @Override
					 protected Void doInBackground() throws Exception {
						 doTheJob();
						 return null;
					 }

				 };

				 worker.execute();
			 }
			 */
		 }
	 };

	 public MiseAJour() {
		 try {				
			 BufferedReader brVer = new BufferedReader(new FileReader("ver"));
			 String ligne;
			 while ((ligne=brVer.readLine())!=null) {
				 lVersionActuelle.setText("La version actuellement installée est "+ligne+".");
			 }
			 brVer.close();

		 } catch (FileNotFoundException fnf) {
			 fnf.printStackTrace();
		 } catch (IOException io) {
			 io.printStackTrace();
		 }

		 
		 pVersion.setLayout(new FlowLayout());
		 pVersion.add(lVersionActuelle);
		 
		 pStatut.setLayout(new FlowLayout());
		 pStatut.add(lStatutMiseAJour);

		 bVerif.addActionListener(alVerif);

		 pBouton.setLayout(new FlowLayout());
		 pBouton.add(bVerif);

		 this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		 this.add(pVersion);
		 this.add(pStatut);
		 this.add(pBouton);
	 }

/*	 void doTheJob() {
		 byte[] tampon = new byte[16384]; // 16k
		 int octetsLus;
		 URL myURL;

		 if (!FonctionsUtiles.LireChamp("site", "proxy").isEmpty()) {
			 System.setProperty("https.proxyHost", FonctionsUtiles.quelProxy());
			 System.setProperty("https.proxyPort", FonctionsUtiles.quelPort());
		 }

		 try {
			 myURL = new URL(FonctionsUtiles.LireChamp("site", "url")+"winInstaller/patchABCDI.exe");

			 FileOutputStream fos = new FileOutputStream(new File("installeurs/ABCDI-"+versionLaPlusRecente+".exe"));
			 HttpURLConnection connexion =(HttpURLConnection) myURL.openConnection();
			 connexion.connect();
			 int taille = connexion.getContentLength();

			 InputStream stream = connexion.getInputStream();
			 ProgressMonitorInputStream pmis = new ProgressMonitorInputStream(MiseAJour.this, "Téléchargement de la version "+versionLaPlusRecente, stream);
			 ProgressMonitor pm = pmis.getProgressMonitor();
			 pm.setMinimum(0);
			 pm.setMaximum(taille);
			 //System.out.println(taille);
			 int progres = 0;
			 while ((octetsLus=pmis.read(tampon))!=-1) {
				 fos.write(tampon);
				 //System.out.println(octetsLus);
				 progres+=octetsLus;
				 pm.setProgress(progres);
			 }
			 fos.close();
			 pmis.close();
		 } catch (MalformedURLException e1) {
			 JOptionPane.showMessageDialog(MiseAJour.this, "<html>Attention, le site à atteindre ne répond pas.<br>Fermer, puis relancer le programme.<br> Si cette erreur persiste, contacter le développeur.</html>", "Erreur site", JOptionPane.ERROR_MESSAGE);
			 e1.printStackTrace();
		 } catch (FileNotFoundException e2) {
			 JOptionPane.showMessageDialog(MiseAJour.this, "Erreur : fichier non trouvé.", "Erreur", JOptionPane.ERROR_MESSAGE);
			 e2.printStackTrace();
		 } catch (IOException e3) {
			 JOptionPane.showMessageDialog(MiseAJour.this, "Erreur de lecture/écriture dans les fichiers.", "Erreur", JOptionPane.ERROR_MESSAGE);
			 e3.printStackTrace();
		 }

		 if (!FonctionsUtiles.LireChamp("site", "proxy").isEmpty()) {
			 System.clearProperty("https.proxyHost");
		 }


		 try {
			 if (SystemUtils.IS_OS_WINDOWS) {
				 //Runtime.getRuntime().exec("explorer.exe /select, \""+System.getProperty("user.dir")+"/installeurs/ABCDI-"+versionLaPlusRecente+".exe\"");
				 Desktop.getDesktop().open(new File(System.getProperty("user.dir")+"/installeurs/ABCDI-"+versionLaPlusRecente+".exe"));
				 System.exit(0);
			 } else {
				 // complètement débile de faire ça vu que je télécharge un .exe.
				 // mais bon.
				 URI gagarine = new URI("file://"+System.getProperty("user.dir")+"/installeurs/");
				 Desktop bureau = Desktop.getDesktop();
				 bureau.browse(gagarine);
				 System.exit(0);
			 }

		 } catch (IOException io) {
			 io.printStackTrace();
		 } catch (URISyntaxException youri) {
			 youri.printStackTrace();
		 }


	 }
*/
}
