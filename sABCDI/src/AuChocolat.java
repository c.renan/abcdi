import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JWindow;

public class AuChocolat {

	/**
	 * @param args
	 */
	public static Map<String, List<String>> parametres = new HashMap<String, List<String>>();
	
	public static Map<String,String> correspondances = new HashMap<String,String>();
	
	public static void main(String[] args) {
		JWindow window = new JWindow();
		
		File splashFile = new File("splash");
		if (splashFile.exists()) {
			Icon logo = new ImageIcon("resources/anim.gif");
			JLabel labelLogo = new JLabel(logo);

			window.setSize(600, 480);
			window.setLocationRelativeTo(null);
			window.add(labelLogo);
			window.setBackground(Color.white);
			window.setVisible(true);
		}
		
		parametres=EcrireParametres.Lire();
		
		correspondances.put("nom", "Nom");
		correspondances.put("auteur","Auteur");
		correspondances.put("ecole", "École");
		correspondances.put("prenom","Prénom");
		correspondances.put("etat", "État");
		correspondances.put("dateNaissance", "Date de naissance");
		correspondances.put("classe", "Classe");
		correspondances.put("dateParution", "Date de parution");
		correspondances.put("presentation", "Présentation");
		correspondances.put("dimensions","Dimensions");
		correspondances.put("editeur","Éditeur");
		correspondances.put("nbPages","Nombre de pages");
		correspondances.put("collection", "Collection");
		correspondances.put("format","Format");
		correspondances.put("resume", "Résumé");
		correspondances.put("statut","Statut");
		correspondances.put("id","Id");
		correspondances.put("motsCles","Mots-Clés");
		correspondances.put("age","Âge");
		correspondances.put("isbn","ISBN");
		correspondances.put("prix","Prix");
		correspondances.put("dewey","Dewey");
		
		if (parametres.get("ecoles").size()<=1) {
			String premiereEcole=null;
			while ((premiereEcole==null)||(premiereEcole.equals("")))
			{
				premiereEcole = JOptionPane.showInputDialog(null, "<html>Il n'y a aucune école de paramétrée dans le logiciel.<br>Quel est le nom de l'école ?</html>");
			}
			EcrireParametres.Changer("ecoles", "defaut", premiereEcole);
			parametres=EcrireParametres.Lire();
			EcrireParametres.Ajouter("ecoles", premiereEcole);
			parametres=EcrireParametres.Lire();
		} 
		
		/*
		 * Le serveur
		 */
		ServerSocket chaussette;
		try {
			chaussette = new ServerSocket(Integer.parseInt(FonctionsUtiles.LireChamp("serveur", "port")));
			// TODO aller chercher le numero de port dans les paramètres
			Thread t = new Thread(new accepterConnexion(chaussette));
			t.start();
			//System.out.println("Le serveur est lancé");
		} catch (IOException ioe) {
			if (ioe.getMessage().contains("Bind failed")) {
				JOptionPane.showMessageDialog(null, "<html>Le port utilisé pour le serveur est déjà occupé.<br>ABCDI est-il déjà lancé ?</html>","Erreur",JOptionPane.ERROR_MESSAGE);
			}
			ioe.printStackTrace();
		}
		
		/* Je ne sais PAS si ce que je fais est bien...*/
		verifierMAJ vm = new verifierMAJ();
		vm.run(false);
		
		new Fenetre(window);
		
	}
}

class accepterConnexion implements Runnable {
	private ServerSocket serveurDeChaussettes;
	private Socket chaussette;
	public accepterConnexion(ServerSocket s) {
		serveurDeChaussettes = s;
	}
	public void run() {
		while (true) {
			try {

				chaussette = serveurDeChaussettes.accept();
				//System.out.println("Connexion reçue");
								
				Thread t = new Thread(new TraitementRequete(chaussette));
				t.start();

			} catch (IOException ioe){
				ioe.printStackTrace();
			}
		}
	}
}

//TODO je ne sais pas où mettre ça : on pourrait créer un numéro d'installation unique qui s'identifierait avec un échange de clés sur crd.ze.cx et avec ça on pourrait faire des sauvegardes en ligne.

