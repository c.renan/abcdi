import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;


public class TraitementRequete implements Runnable {

	private Socket chaussette; // les mecs ils mettent private, public tout ça. Je ne vois pas trop l'intérêt de mettre private en fait.
	// À moins que ce ne soit une règle que je ne connais pas.
	private PrintWriter ecrire = null;
	private BufferedInputStream lire = null;


	public TraitementRequete(Socket sSock) {
		chaussette = sSock;
	}

	public void run() {
		try {
			while (!chaussette.isClosed()) {
				/*
				 * Je fais comme dans le tuto SANS RÉFLÉCHIR.
				 */
				ecrire = new PrintWriter(chaussette.getOutputStream());
				lire = new BufferedInputStream(chaussette.getInputStream());

				
				String requeteClient = read();
				
				if (!requeteClient.equals("TE")) {
					DateFormat dfLog=new SimpleDateFormat("[dd/MM/yy HH:mm:ss]");
					Date maintenowLog=Calendar.getInstance().getTime();
					String laDateLog = dfLog.format(maintenowLog);


					try {
						FileWriter fwLog = new FileWriter("clients.log",true); // le true signifie qu'on append
						BufferedWriter bw = new BufferedWriter(fwLog);
						bw.write(laDateLog+" "+requeteClient);
						bw.newLine();
						bw.close();
						
						// avant je appendais maintenant je insert (c'est pour mettre le plus récent en haut)
						/**
						 *  je laisse quand même la fonction appendTPLog dans mon code au 
						 *  cas où...
						**/
						// Terminal.appendTPLog(laDateLog+" "+requeteClient+"\n"); // mais ça c'était avant
						Terminal.insertTPLog(0,laDateLog+" "+requeteClient+"\n");
					} 
					catch (IOException ioe) {
						ioe.printStackTrace();
					}
					
					/*
					try {
						BufferedReader br = new BufferedReader(new FileReader("clients.log"));
						String ligne;
						while ((ligne=br.readLine())!=null) {
							Terminal.tpLog.setText(Terminal.tpLog.getText()+ligne+"\n");
						}
						br.close();
					}
					catch (FileNotFoundException fnf) {
						fnf.printStackTrace();
					}
					catch (IOException ioe) {
						ioe.printStackTrace();
					}
				    */	
				}
				
				// TODO écrire dans le fichier clients.log la requête, l'ip du client peut-être, ou son nom, enfin un truc quoi. Ou rien.
				// TODO du coup la ligne doit prendre cette forme-là : [12/07/21 09:35:21] R=le loup puait des pieds
				// TODO et quand on clique sur une ligne avec des ID, on a des informations sur le livre correspondant.

				if (requeteClient.matches("R=.+")) {
					String[] leschamps = {"id", "nom", "auteur","statut","dewey"};
					String search = requeteClient.substring(2);

					String aRenvoyer = "[";

					List<Map<String,Object>> rs = ConnectionSQL.selectionLivres(search, "");
					int nbRes = 0;
					for (int i=0;i<rs.size();i++) {
						if (!aRenvoyer.equals("[")) {
							aRenvoyer += ";;";
						}
						String resultat = "{";

						for (String champ : leschamps ) {
							resultat += rs.get(i).get(champ).toString();
							if (!champ.equals("dewey")) {
								// ce champ = dewey est le dernier champ pour lequel on ne met pas de ,,,
								resultat+=",,,";
							}
						}
						resultat += "}";
						if (!rs.get(i).get("statut").equals("Disparu") || Boolean.valueOf(FonctionsUtiles.LireChamp("livres", "voirLesDisparus"))) {
							aRenvoyer += resultat;
							nbRes++;
						}

					}
					aRenvoyer += "]"+";;;"+nbRes;

					ecrire.write(aRenvoyer);
					ecrire.flush();

				}
				else if (requeteClient.matches("S=.+")) {
					if (requeteClient.matches("S=\\d{6}")) {
						Map<String,Object> resRechLivre = ConnectionSQL.selectionLivreParId(Integer.parseInt(requeteClient.substring(2)));
						if (resRechLivre!=null) {
							String aRenvoyer = "";

							String[] champs = {
									"nom",
									"auteur",
									"dateParution",
									"presentation",
									"dimensions",
									"editeur",
									"nbPages",
									"collection",
									"format",
									"isbn",
									"dewey",
							"statut"};

							for (String champ : champs) {
								if (!aRenvoyer.equals("")) {
									aRenvoyer+=";;";
								}
								aRenvoyer += champ+"::"+resRechLivre.get(champ);
							}
							aRenvoyer +=";;resume::"+resRechLivre.get("resume");
							ecrire.write(aRenvoyer);
							ecrire.flush();
						} else {
							ecrire.write("<--RIEN-->");
							ecrire.flush();
						}

					} else {
						ecrire.write("<--RIEN-->");
						ecrire.flush();
					}
				}
				else if (requeteClient.matches("TE")) {
					String[] leschamps = {"id", "nom", "prenom"};
					
					String aRenvoyer = "[";

					List<Map<String,Object>> rs = ConnectionSQL.selectionEleves("");
					int nbRes = 0;
					for (int i=0;i<rs.size();i++) {
						if (!aRenvoyer.equals("[")) {
							aRenvoyer += ";;";
						}
						String resultat = "{";

						for (String champ : leschamps ) {
							resultat += rs.get(i).get(champ).toString();
							if (!champ.equals("prenom")) {
								// ce champ = prenom est le dernier champ pour lequel on ne met pas de ,,,
								resultat+=",,,";
							}
						}
						resultat += "}";
						if (!rs.get(i).get("statut").equals("Disparu") || Boolean.valueOf(FonctionsUtiles.LireChamp("livres", "voirLesDisparus"))) {
							// TODO paramétrer ça pour le serveur	
							aRenvoyer += resultat;
							nbRes++;
						}

					}
					aRenvoyer += "]"+";;;"+nbRes;


					ecrire.write(aRenvoyer);
					ecrire.flush();

				}
				else if (requeteClient.matches("E=9\\d{5},L=\\d{6}")) {
					int idEleve=Integer.parseInt(requeteClient.split(",")[0].substring(3));
					int idLivre=Integer.parseInt(requeteClient.split(",")[1].substring(2));

					String message = "1";
					
					try {
						DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
						Date maintenow = Calendar.getInstance().getTime();
						String laDate = df.format(maintenow);
						ConnectionSQL.operation("INSERT INTO emprunts (idlivre, ideleve, dateemprunt, revenu) VALUES ("+idLivre+", "+idEleve+", '"+laDate+"', 'non');");
						// Oui maintenant que j'ai fait la commande operation, qui marche pour tout sauf pour une sélection, beh je m'emmerde moins.
						Map<String,Object> informations = new HashMap<String,Object>();
						informations.put("statut", "Emprunté");
						ConnectionSQL.updatationLivre(idLivre, informations);
					}
					catch (Exception e) {
						message=e.getLocalizedMessage();
						e.printStackTrace();
					}
					
					ecrire.write(message);
					ecrire.flush();
				}	
				else if (requeteClient.matches("Z=\\d{6}")) {
					int idLivre=Integer.parseInt(requeteClient.substring(2));

					String message = "1";
					
					try {
						Map<String,Object> informations = new HashMap<String,Object>();
						informations.put("statut", "Disponible");
						ConnectionSQL.updatationLivre(idLivre, informations);
						ConnectionSQL.operation("UPDATE emprunts SET revenu='oui' WHERE idlivre="+idLivre+";");
					}
					catch (Exception e) {
						message=e.getLocalizedMessage();
						e.printStackTrace();
					}
					
					ecrire.write(message);
					ecrire.flush();
				}
				else if (requeteClient.matches("V=9\\d{5},W=\\d{6}")) {
					int idEleve=Integer.parseInt(requeteClient.split(",")[0].substring(3));
					int idLivre=Integer.parseInt(requeteClient.split(",")[1].substring(2));

					String message = "faux";
					
					try {
						String[] lesChamps = {"id"};
						List<Map<String,Object>> cetEmprunt = ConnectionSQL.selectionParChamps(
								"SELECT rowid AS id FROM emprunts WHERE ideleve = "+idEleve+" AND idlivre="+idLivre +" AND revenu='non';", lesChamps);
						if (!cetEmprunt.isEmpty()) {
							message="vrai";
						}
					}
					catch (Exception e) {
						message=e.getLocalizedMessage();
						e.printStackTrace();
					}
					
					ecrire.write(message);
					ecrire.flush();
				}
				else if (requeteClient.matches("C=9\\d{5}")) {
					String message="1";
					ecrire.write(message);
					ecrire.flush();
				}
				else 
				{
					ecrire.write("<-- RIEN -->");
					ecrire.flush();
				}				

				ecrire.close();

				/*
				 *  ici le client va envoyer ... trucs.
				 * OK Soit R= et sa recherche
				 * OK Soit S= et le scan (livre)
				 * OK Soit E= et emprunter le livre dont l'id est ++ l'id élève est 
				 * OK Soit Z=idlivre pour le rendre
				 * OK Soit V=idEleve W=idLivre qui returne vrai ou faux selon qu'un emprunt est en cours
				 * Soit C= et l'id de l'élève qui se connecte, mais c'est juste pour le log
				 *  
				 */

			}
		} catch (IOException ioe) {
			JOptionPane.showMessageDialog(null, "Erreur d'entrée-sortie. La connexion semble dysfonctionner.", "Erreur", JOptionPane.ERROR_MESSAGE);
			ioe.printStackTrace();
		}
	}


	//La méthode que nous utilisons pour lire les réponses cf https://openclassrooms.com/fr/courses/2654601-java-et-la-programmation-reseau/2668874-les-sockets-cote-serveur
	private String read() throws IOException{      
		String response = "";
		int stream;
		byte[] b = new byte[4096];
		stream = lire.read(b);
		response = new String(b, 0, stream);
		return response;
	}


}
