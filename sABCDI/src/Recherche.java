import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.RowSorter;
import javax.swing.RowSorter.SortKey;
import javax.swing.SortOrder;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;


public class Recherche extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 608801054694450408L;
	
	RPlaceHolderTextField tRecherche = new RPlaceHolderTextField("Recherche...");
	RPlaceHolderTextField tAge = new RPlaceHolderTextField("Âge");
			
	JRadioButton rbLivres = new JRadioButton("Livres");
	JRadioButton rbEleves = new JRadioButton("Élèves");
	ButtonGroup bg = new ButtonGroup();
	
	static String[] titreLivres=FonctionsUtiles.faisCorrespondre(FonctionsUtiles.traiteStringListe(FonctionsUtiles.LireChamp("recherche", "livresColumns")));
	public static DefaultTableModel dtmLivres = new DefaultTableModel(null, titreLivres){
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		@SuppressWarnings({ "unchecked", "rawtypes" }) 
		@Override
        public Class getColumnClass(int column) {
            switch (column) {
                case 0:
                    return Integer.class;
                default:
                    return Object.class;
            }
        }
		@Override
		public boolean isCellEditable(int row, int column) {
			return false;
		}
	};
	JTable tableauLivres = new JTable(dtmLivres);
	
	
	static String[] titreEleves=FonctionsUtiles.faisCorrespondre(FonctionsUtiles.traiteStringListe(FonctionsUtiles.LireChamp("recherche", "elevesColumns")));
	public static DefaultTableModel dtmEleves = new DefaultTableModel(null, titreEleves){
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		@SuppressWarnings({ "unchecked", "rawtypes" }) 
		@Override
        public Class getColumnClass(int column) {
            switch (column) {
                case 0:
                    return Integer.class;
                default:
                    return Object.class;
            }
        }
		@Override
		public boolean isCellEditable(int row, int column) {
			return false;
		}
	};
	JTable tableauEleves = new JTable(dtmEleves);
	
	JButton boutonRecherche = new JButton("Recherche");
	
	JScrollPane jspTableaux = new JScrollPane();
	// ce jsp doit changer de tableaux, donc j'ai besoin de l'avoir défini.
	
	TableRowSorter<DefaultTableModel> trieurEleves = new TableRowSorter<DefaultTableModel>(dtmEleves);
	TableRowSorter<DefaultTableModel> trieurLivres = new TableRowSorter<DefaultTableModel>(dtmLivres);
	List<RowSorter.SortKey> clesDeTri = new ArrayList<SortKey>(25);
	
	JPopupMenu jpmLivres = new JPopupMenu();
	JPopupMenu jpmEleves = new JPopupMenu();
	JMenuItem miSupprimerEleves = new JMenuItem("Supprimer");
	JMenuItem miRestaurerEleves = new JMenuItem("Restaurer");
	JMenuItem miChangerDeClasse = new JMenuItem("Changer de classe");
	JMenuItem miRemoveEleves = new JMenuItem("Supprimer définitivement");
	JMenuItem miDetailEleves = new JMenuItem("Détails");
	JMenuItem miSupprimerLivres = new JMenuItem("Supprimer");
	JMenuItem miRestaurerLivres = new JMenuItem("Restaurer");
	JMenuItem miArchiverLivres = new JMenuItem("Archiver");
	JMenuItem miDupliquerLivre = new JMenuItem("Dupliquer");
	JMenuItem miRemoveLivres = new JMenuItem("Supprimer définitivement");
	JMenuItem miDetailLivres = new JMenuItem("Détails");
	
	JPanel premiereLigne = new JPanel();
	JPanel deuxiemeLigne = new JPanel();
	JPanel pageTop = new JPanel();
	
	JPanel pLabelSelection = new JPanel();
	JPanel pLabelNbResults = new JPanel();
	
	JPanel pTexteIndic = new JPanel();
	
	JLabel lLivresSelectionnes = new JLabel("Sélection vide");
	JLabel lElevesSelectionnes = new JLabel("Sélection vide");
	
	JLabel lNbResults = new JLabel("Nombre de résultats");
	
	public Recherche() {
		// popup menu
		miSupprimerLivres.addActionListener(alSuppressionLivres);
		miArchiverLivres.addActionListener(alArchivageLivres);
		miRestaurerLivres.addActionListener(alRestaurationLivres);
		miDupliquerLivre.addActionListener(alDuplicationLivre);
		miRemoveLivres.addActionListener(alRemoveLivres);
		miDetailLivres.addActionListener(alDetailLivres);
		
		miSupprimerEleves.addActionListener(alSuppressionEleves);
		miRestaurerEleves.addActionListener(alRestaurationEleves);
		miChangerDeClasse.addActionListener(alChangementClasse);
		miRemoveEleves.addActionListener(alRemoveEleves);
		miDetailEleves.addActionListener(alDetailEleves);
		
		// tableaux
		tableauLivres.getColumnModel().getColumn(0).setMaxWidth(100);
		tableauEleves.getColumnModel().getColumn(0).setMaxWidth(100);	
		tableauLivres.getColumnModel().getColumn(0).setPreferredWidth(45);
		tableauEleves.getColumnModel().getColumn(0).setPreferredWidth(45);
		
		
		tableauLivres.setRowSorter(trieurLivres);
		tableauEleves.setRowSorter(trieurEleves);
		
		clesDeTri.add(new RowSorter.SortKey(0, SortOrder.ASCENDING));
			
		trieurLivres.setSortKeys(clesDeTri);
		trieurEleves.setSortKeys(clesDeTri);

		/*
		 * Dans les tableaux, je mets un listselectionlistener
		 */
		ListSelectionModel lsmLivres = tableauLivres.getSelectionModel();
		lsmLivres.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				ListSelectionModel lsm = (ListSelectionModel)e.getSource();
				if (lsm.isSelectionEmpty()) {
					lLivresSelectionnes.setText("Sélection vide");
				} else if (tableauLivres.getSelectedRowCount()==1){
					lLivresSelectionnes.setText("Sélection : 1 livre");
				} else {
					lLivresSelectionnes.setText("Sélection : "+tableauLivres.getSelectedRowCount()+" livres");
				}
			}
		});
		
		ListSelectionModel lsmEleves = tableauEleves.getSelectionModel();
		lsmEleves.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				ListSelectionModel lsm = (ListSelectionModel)e.getSource();
				if (lsm.isSelectionEmpty()) {
					lElevesSelectionnes.setText("Sélection vide");
				} else if (tableauEleves.getSelectedRowCount()==1){
					lElevesSelectionnes.setText("Sélection : 1 élève");
				} else {
					lElevesSelectionnes.setText("Sélection : "+tableauEleves.getSelectedRowCount()+" élèves");
				}
			}
		});
		
		// radiobuttons pour le changement de tableau
		rbLivres.addActionListener(new rbListener(1));
		rbEleves.addActionListener(new rbListener(2));
		
		bg.add(rbLivres);
		bg.add(rbEleves);
		
		// Bouton (et barre de recherche)
		boutonRecherche.addActionListener(this);
		tRecherche.addActionListener(this);
		
		tAge.addActionListener(this);
		
		/*
		 * Préparation du layout
		 */
		tRecherche.setPreferredSize(new Dimension(400,30));
		tAge.setPreferredSize(new Dimension(50,30));
		
		premiereLigne.setLayout(new FlowLayout());
		premiereLigne.add(tRecherche);
		premiereLigne.add(tAge);
		if (rbEleves.isSelected()) {
			tAge.setEnabled(false);
		}
		premiereLigne.add(boutonRecherche);
		
		deuxiemeLigne.setLayout(new FlowLayout());
		deuxiemeLigne.add(new JLabel("parmi "));
		deuxiemeLigne.add(rbLivres);
		deuxiemeLigne.add(rbEleves);
		
		pageTop.setLayout(new BoxLayout(pageTop, BoxLayout.PAGE_AXIS));
		pageTop.add(premiereLigne);
		pageTop.add(deuxiemeLigne);
		
		/*
		 * Layout
		 *
		 */
		
		this.setLayout(new BorderLayout());
		
		this.add(pageTop, BorderLayout.NORTH);
		
		this.add(jspTableaux, BorderLayout.CENTER);
		
		pLabelNbResults.add(lNbResults);
		
		pTexteIndic.setLayout(new GridLayout(1,2));
		pTexteIndic.add(pLabelSelection);
		pTexteIndic.add(pLabelNbResults);
		
		this.add(pTexteIndic, BorderLayout.SOUTH);
		/*
		 * Gestion des clics sur les livres
		 */
		tableauLivres.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent souris ) {
				jpmLivres.removeAll();
				jpmLivres.add(miDetailLivres);
				
				JTable jt = (JTable) souris.getSource();
				
				Point p = souris.getPoint();
				int row= jt.rowAtPoint(p);
				
				if ((souris.getClickCount()==2)&&(SwingUtilities.isLeftMouseButton(souris))) {
					new DetailLivre(Integer.parseInt(jt.getValueAt(row, 0).toString()));
				}
				if (SwingUtilities.isRightMouseButton(souris)) {
					Map<String,Object> livre = ConnectionSQL.selectionLivreParId(Integer.valueOf(jt.getValueAt(row,0).toString()));
					if (jt.getSelectedRowCount()==1) {
						jpmLivres.add(miDupliquerLivre);
					}
					if (livre.get("statut").equals("Disparu")||livre.get("statut").equals("Archivé")) {
						jpmLivres.add(miRestaurerLivres);
					} else {
						jpmLivres.add(miSupprimerLivres);
						jpmLivres.add(miArchiverLivres);
					}
					jpmLivres.add(miRemoveLivres);
				
					if (jt.isRowSelected(jt.rowAtPoint(p))) {

						jpmLivres.show(jt, p.x,p.y);
					}

					else {
						if (souris.getClickCount()==2) {
							jt.setRowSelectionInterval(jt.rowAtPoint(p), jt.rowAtPoint(p));
							jpmLivres.show(jt,p.x,p.y);
						}
					}
				}
			}
			
		});
		/*
		 * Gestion des clics pour les élèves
		 */
		tableauEleves.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent souris ) {
				jpmEleves.removeAll();
				jpmEleves.add(miDetailEleves);
				jpmEleves.add(miChangerDeClasse);
				
				JTable jt = (JTable) souris.getSource();
				Point p = souris.getPoint();
				int row= jt.rowAtPoint(p);
				if ((souris.getClickCount()==2)&&(SwingUtilities.isLeftMouseButton(souris))) {
					new DetailEleve(Integer.parseInt(jt.getValueAt(row, 0).toString()));
				}
				if (SwingUtilities.isRightMouseButton(souris)) {
					Map<String,Object> eleve = ConnectionSQL.selectionEleveParId(Integer.valueOf(jt.getValueAt(row,0).toString()));
					if (eleve.get("statut").equals("Parti")) {
						jpmEleves.add(miRestaurerEleves);
					} else {
						jpmEleves.add(miSupprimerEleves);
					}
					jpmEleves.add(miRemoveEleves);
					if (jt.isRowSelected(jt.rowAtPoint(p))) {
						jpmEleves.show(jt, p.x,p.y);
					}
					else {
						if (souris.getClickCount()==2) {
							jt.setRowSelectionInterval(jt.rowAtPoint(p), jt.rowAtPoint(p));
							jpmEleves.show(jt,p.x,p.y);
						}
					}
				}
			}
		});
		
		/*
		 * La fonction qui suit est amusante non ?
		 * Non ?
		 * Non, c'est vrai. Mais je l'ai faite car je croyais pouvoir comme ça résoudre un bug que
		 * j'ai contourné depuis.
		 */
		if (FonctionsUtiles.LireChamp("recherche", "defautRecherche").equals("livres")) {
			rbLivres.doClick();
			//rbLivres.setSelected(true);
			//jspTableaux.setViewportView(tableauLivres);
		}
		else {
			rbEleves.doClick();
			//rbEleves.setSelected(true);
			//jspTableaux.setViewportView(tableauEleves);
		}

	}

	public void actionPerformed(ActionEvent e) {
		if (rbLivres.isSelected()) {
			// livres
			List<Map<String,Object>> rs = ConnectionSQL.selectionLivres(tRecherche.texte(), tAge.texte());
			DefaultTableModel modeleTable = (DefaultTableModel) Recherche.this.tableauLivres.getModel();
	
			modeleTable.setRowCount(0);
		
			for (int i=0;i<rs.size();i++) {
				Object[] aoLaListe = new Object[FonctionsUtiles.traiteStringListe(FonctionsUtiles.LireChamp("recherche", "livresColumns")).length];
				int j=0;
				for (String champ : FonctionsUtiles.traiteStringListe(FonctionsUtiles.LireChamp("recherche", "livresColumns"))) {
					aoLaListe[j] = rs.get(i).get(champ);
					j++;
				}
				if (!rs.get(i).get("statut").equals("Disparu") || Boolean.valueOf(FonctionsUtiles.LireChamp("livres", "voirLesDisparus"))) {
					modeleTable.addRow(aoLaListe);
				}
			}
			lNbResults.setText("Nombre de résultats : "+modeleTable.getRowCount());
		}
		else {
			// eleves
			List<Map<String,Object>> rs = ConnectionSQL.selectionEleves(tRecherche.texte());
			DefaultTableModel modeleTable = (DefaultTableModel) Recherche.this.tableauEleves.getModel();
			modeleTable.setRowCount(0);
			for (int i=0;i<rs.size();i++) {
				Object[] aoLaListe = new Object[FonctionsUtiles.traiteStringListe(FonctionsUtiles.LireChamp("recherche","elevesColumns")).length];
				int j=0;
				for (String champ : FonctionsUtiles.traiteStringListe(FonctionsUtiles.LireChamp("recherche","elevesColumns"))) {
					aoLaListe[j] = rs.get(i).get(champ);
					j++;
				}
				if (!rs.get(i).get("statut").equals("Parti") || Boolean.valueOf(FonctionsUtiles.LireChamp("eleves", "voirLesPartis"))) {
					modeleTable.addRow(aoLaListe);
				}
			}
			lNbResults.setText("Nombre de résultats : "+modeleTable.getRowCount());
			pLabelNbResults.removeAll();
			pLabelNbResults.add(lNbResults);
			pLabelNbResults.repaint();
		}
	}	

	class rbListener implements ActionListener{
		/**
		 * Alors ça c'est mon invention.
		 * À mon avis, ça va être bien de la dreum mais bon.
		 * (pour l'instant ça a l'air de marcher)
		 */
		int indice;
		
		rbListener(int index) {
			indice=index;
		}
		public void actionPerformed(ActionEvent e) {
			if (indice==1) {
				jspTableaux.setViewportView(tableauLivres);
				tAge.setEnabled(true);
				
				pLabelSelection.removeAll();
				pLabelSelection.add(lLivresSelectionnes);
				pLabelSelection.repaint();
				
				lNbResults.setText("Nombre de résultats : "+tableauLivres.getRowCount());
				pLabelNbResults.removeAll();
				pLabelNbResults.add(lNbResults);
				pLabelNbResults.repaint();
				
			}
			if (indice==2) {
				jspTableaux.setViewportView(tableauEleves);
				tAge.setEnabled(false);
				
				pLabelSelection.removeAll();
				pLabelSelection.add(lElevesSelectionnes);
				pLabelSelection.repaint();
				
				lNbResults.setText("Nombre de résultats : "+tableauEleves.getRowCount());
				pLabelNbResults.removeAll();
				pLabelNbResults.add(lNbResults);
				pLabelNbResults.repaint();
			}
		}
	}

	ActionListener alSuppressionLivres = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			String indicesLivres = "";
			int j=0;
			for (int i : tableauLivres.getSelectedRows()) {
				if (j>0) {
					indicesLivres += " OR ";
				}
				indicesLivres += "rowid="+tableauLivres.getValueAt(i, 0);
				j++;
			}
			ConnectionSQL.operation("UPDATE livres SET statut='Disparu' WHERE "+indicesLivres+";");
			boutonRecherche.doClick();
		}
	};

	ActionListener alArchivageLivres = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			String indicesLivres = "";
			int j=0;
			for (int i : tableauLivres.getSelectedRows()) {
				if (j>0) {
					indicesLivres += " OR ";
				}
				indicesLivres += "rowid="+tableauLivres.getValueAt(i, 0);
				j++;
			}
			ConnectionSQL.operation("UPDATE livres SET statut='Archivé' WHERE "+indicesLivres+";");
			boutonRecherche.doClick();
		}
	};
	
	ActionListener alRestaurationLivres = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			String indicesLivres = "";
			int j=0;
			for (int i : tableauLivres.getSelectedRows()) {
				if (j>0) {
					indicesLivres += " OR ";
				}
				indicesLivres += "rowid="+tableauLivres.getValueAt(i, 0);
				j++;
			}
			ConnectionSQL.operation("UPDATE livres SET statut='Disponible' WHERE "+indicesLivres+";");
			boutonRecherche.doClick();
		}
	};

	ActionListener alDuplicationLivre = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			int nombreDuplication = -1;
			while (nombreDuplication<0) {
				Object retour = JOptionPane.showInputDialog(Recherche.this, "Combien d'exemplaires de ce livre ajouter ?", "Duplication livre", JOptionPane.QUESTION_MESSAGE);
				if (retour!=null) {
					try {
						nombreDuplication = Integer.valueOf(retour.toString());
					}
					catch (NumberFormatException nf) {
						nf.printStackTrace();
						nombreDuplication = -1;
					}
					// Je crois que ce n'est pas très propre de faire comme ça. Il faut vraiment que je lise ce bouquin.
				}
				else {
					nombreDuplication = 0;
					// rien
				}
			}
			
			for (int i=0;i<nombreDuplication;i++) {
				ConnectionSQL.operation("INSERT INTO livres SELECT * FROM livres WHERE rowid="+tableauLivres.getValueAt(tableauLivres.getSelectedRow(),0)+";");
			}
			
			boutonRecherche.doClick();
		}
	};
	
	ActionListener alDetailEleves = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			int rep=JOptionPane.YES_OPTION;
			if (tableauEleves.getSelectedRowCount()>10) {
				rep = JOptionPane.showConfirmDialog(Recherche.this, tableauEleves.getSelectedRowCount()+" fenêtres vont être ouvertes. Continuer ?","Attention",JOptionPane.YES_NO_OPTION);
			}
			if (rep==JOptionPane.YES_OPTION) {
				for (int i : tableauEleves.getSelectedRows()) {
					new DetailEleve(Integer.parseInt(tableauEleves.getValueAt(i, 0).toString()));
				}
			}
		}
	};

	ActionListener alDetailLivres = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			int rep=JOptionPane.YES_OPTION;
			if (tableauLivres.getSelectedRowCount()>10) {
				rep = JOptionPane.showConfirmDialog(Recherche.this, tableauLivres.getSelectedRowCount()+" fenêtres vont être ouvertes. Continuer ?","Attention",JOptionPane.YES_NO_OPTION);
			}
			if (rep==JOptionPane.YES_OPTION) {
				for (int i : tableauLivres.getSelectedRows()) {
					new DetailLivre(Integer.parseInt(tableauLivres.getValueAt(i, 0).toString()));
				}
			}
		}
	};

	ActionListener alRemoveLivres = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			String indicesLivres = "";
			String indicesEmprunts = "";
			int j=0;
			for (int i : tableauLivres.getSelectedRows()) {
				if (j>0) {
					indicesLivres += " OR ";
					indicesEmprunts += " OR ";
				}
				indicesLivres += "rowid="+tableauLivres.getValueAt(i, 0);
				indicesEmprunts += "idlivre="+tableauLivres.getValueAt(i, 0);
				j++;
			}
			ConnectionSQL.operation("DELETE FROM livres WHERE "+indicesLivres+";");
			ConnectionSQL.operation("DELETE FROM emprunts WHERE "+indicesEmprunts+";");
			boutonRecherche.doClick();
		}
	};
	
	ActionListener alSuppressionEleves = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			String indicesEleves = "";
			int j=0;
			for (int i : tableauEleves.getSelectedRows()) {
				if (j>0) {
					indicesEleves += " OR ";
				}
				indicesEleves += "rowid="+tableauEleves.getValueAt(i, 0);
				j++;
			}
			ConnectionSQL.operation("UPDATE eleves SET statut='Parti' WHERE "+indicesEleves+";");
			boutonRecherche.doClick();
		}
	};
	
	ActionListener alRestaurationEleves = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			String indicesEleves = "";
			int j=0;
			for (int i : tableauEleves.getSelectedRows()) {
				if (j>0) {
					indicesEleves += " OR ";
				}
				indicesEleves += "rowid="+tableauEleves.getValueAt(i, 0);
				j++;
			}
			ConnectionSQL.operation("UPDATE eleves SET statut='' WHERE "+indicesEleves+";");
			boutonRecherche.doClick();
		}
	};
	
	ActionListener alChangementClasse = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			String nouvelleClasse = JOptionPane.showInputDialog(Recherche.this, "En quelle classe ces élèves vont-ils passer ?", "Changement de classe", JOptionPane.QUESTION_MESSAGE);
			if (nouvelleClasse!=null) {
				String indicesEleves ="";
				int j=0;
				for (int i:tableauEleves.getSelectedRows()) {
					if (j>0) {
						indicesEleves += " OR ";
					}
					indicesEleves += "rowid="+tableauEleves.getValueAt(i,0);
					j++;
				}
				ConnectionSQL.operation("UPDATE eleves SET classe='"+nouvelleClasse+"' WHERE "+indicesEleves+";");

				if (tableauEleves.getSelectedRowCount()>1) {
					JOptionPane.showMessageDialog(Recherche.this, "Les "+tableauEleves.getSelectedRowCount()+" élèves sont bien passés en "+nouvelleClasse+".", "Changement effectué.", JOptionPane.INFORMATION_MESSAGE);
				}else {
					JOptionPane.showMessageDialog(Recherche.this, "L'élève est bien passé en "+nouvelleClasse+".", "Changement effectué.", JOptionPane.INFORMATION_MESSAGE);
				}
				boutonRecherche.doClick();
			}
		} 
	};

	ActionListener alRemoveEleves = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			String indicesEleves = "";
			int j=0;
			for (int i : tableauEleves.getSelectedRows()) {
				if (j>0) {
					indicesEleves += " OR ";
				}
				indicesEleves += "rowid="+tableauEleves.getValueAt(i, 0);
				j++;
			}
			int reponse = JOptionPane.showConfirmDialog(Recherche.this, "Supprimer "+j+" élèves ?", "Confirmation", JOptionPane.YES_NO_OPTION);
			if (reponse==0) {
				ConnectionSQL.operation("DELETE FROM eleves WHERE "+indicesEleves+";");
			}
			boutonRecherche.doClick();
		}
	};
	
}
