import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class APropos extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1018250682173041206L;

	JPanel p = new JPanel();
	
	JPanel pImage = new JPanel();
	JPanel pTitre = new JPanel();
	
	JLabel lTitre = new JLabel(); 
	
	JPanel pDetails = new JPanel();
	
	JLabel lDetails = new JLabel("<html>ABCDI permet la gestion d'une bibliothèque dans une (ou plusieurs) école(s).</html>");
	
	JPanel pCopyright = new JPanel();
	JPanel pMail = new JPanel();
	
	JLabel lCopyright = new JLabel("© C Renan Développement 2017");
	JLabel lMail = new JLabel("<html><a href='mailto:c.renan.dev@gmail.com'>c.renan.dev@gmail.com</a></html>");
	
	JPanel pFermer = new JPanel();
	
	JButton bFermer = new JButton("Fermer");
	
	public APropos() {
		p.setLayout(new BoxLayout(p,BoxLayout.PAGE_AXIS));

		pImage.setLayout(new FlowLayout());
		try {
			Image img = ImageIO.read(new File("resources/icone_rect.png"));
			Image resizedImage = img.getScaledInstance(315,158, Image.SCALE_SMOOTH);
			pImage.add(new JLabel(new ImageIcon(resizedImage)));
		} catch (Exception e) {
			e.printStackTrace();
		}
		p.add(pImage);
		
		String versionInstallee = "";
		try {				
			BufferedReader brVer = new BufferedReader(new FileReader("ver"));
			String ligne;
			while ((ligne=brVer.readLine())!=null) {
				versionInstallee = ligne;
			}
			brVer.close();

		} catch (FileNotFoundException fnf) {
			fnf.printStackTrace();
		} catch (IOException io) {
			io.printStackTrace();
		}
		lTitre.setText("ABCDI v"+versionInstallee);
		
		pTitre.setLayout(new FlowLayout());
		lTitre.setFont(lTitre.getFont().deriveFont(30.0f));
		pTitre.add(lTitre);
		p.add(pTitre);
	
		pDetails.setLayout(new FlowLayout());
		pDetails.add(lDetails);
		p.add(pDetails);
		
		pCopyright.setLayout(new FlowLayout());
		pMail.setLayout(new FlowLayout());
		
		lCopyright.setFont(lCopyright.getFont().deriveFont(8.0f));
		pCopyright.add(lCopyright);
		pMail.add(lMail);
		
		p.add(pCopyright);
		p.add(pMail);
		
		pFermer.setLayout(new FlowLayout());
		bFermer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		pFermer.add(bFermer);
		
		p.add(pFermer);
		
		try{
			Image imageIcone = ImageIO.read(new File("resources/icone.png"));
			this.setIconImage(imageIcone);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		this.setTitle("À propos d'ABCDI");
		this.setContentPane(p);
		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	
}
