import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.Action;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.ProgressMonitor;
import javax.swing.SwingWorker;
import javax.swing.text.DefaultEditorKit;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


public class PanneauAjout extends JPanel implements ActionListener, PropertyChangeListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	ProgressMonitor progressMonitor;

	RPlaceHolderTextField tfISBN = new RPlaceHolderTextField("ISBN");

	JButton boutonGo = new JButton("Go !");
	JButton boutonEnregistrer = new JButton("Enregistrer");

	JButton boutonVider = new JButton("Vider les champs");

	JLabel lStatut = new JLabel("");

	RPlaceHolderTextField nom = new RPlaceHolderTextField("Nom");
	RPlaceHolderTextField auteur = new RPlaceHolderTextField("Auteur");
	RPlaceHolderTextField dateParution = new RPlaceHolderTextField("Date de parution");
	RPlaceHolderTextField presentation = new RPlaceHolderTextField("Présentation");
	RPlaceHolderTextField dimensions = new RPlaceHolderTextField("Dimensions");
	RPlaceHolderTextField editeur = new RPlaceHolderTextField("Éditeur");
	RPlaceHolderTextField nbPages = new RPlaceHolderTextField("Nombre de pages");
	RPlaceHolderTextField collection = new RPlaceHolderTextField("Collection");
	RPlaceHolderTextField format = new RPlaceHolderTextField("Format");
	RPlaceHolderTextField prix = new RPlaceHolderTextField("Prix neuf");
	RPlaceHolderTextField dewey = new RPlaceHolderTextField("Dewey ou référence");

	RPlaceHolderTextField isbn = new RPlaceHolderTextField("ISBN");

	RPlaceHolderTextField ageMin = new RPlaceHolderTextField("Âge Min");
	RPlaceHolderTextField ageMax = new RPlaceHolderTextField("Âge Max");

	RPlaceHolderTextField motsCles = new RPlaceHolderTextField("Mots-clés");

	List<RPlaceHolderTextField> lesRPHTF = new ArrayList<RPlaceHolderTextField>();

	JTextArea resume = new JTextArea();
	JScrollPane jspResume = new JScrollPane(resume);

	JLabel lEcole = new JLabel("École");
	JComboBox<String> cbEcole = new JComboBox<String>();

	JPopupMenu jpmResume = new JPopupMenu();
	JMenuItem miBabelio = new JMenuItem("Importer de Babelio");

	public PanneauAjout() {

		// le menu babelio + copier coller pour le resume
		miBabelio.setEnabled(false);
		jpmResume.add(miBabelio);

		miBabelio.addActionListener(alBabelio);

		Action couper = new DefaultEditorKit.CutAction();
		couper.putValue(Action.NAME, "Couper");
		couper.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_X, KeyEvent.CTRL_DOWN_MASK));
		jpmResume.add(couper);

		Action copier = new DefaultEditorKit.CopyAction();
		copier.putValue(Action.NAME, "Copier");
		copier.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.CTRL_DOWN_MASK));
		jpmResume.add(copier);

		Action coller = new DefaultEditorKit.PasteAction();
		coller.putValue(Action.NAME, "Coller");
		coller.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_V, KeyEvent.CTRL_DOWN_MASK));
		jpmResume.add(coller);

		resume.setComponentPopupMenu(jpmResume);

		/* je me fais une liste des RPlaceHolderTextFields pour faire une action groupée sur chacun
		 * on ne sait jamais.
		 * En fait on sait.
		 */
		lesRPHTF.add(nom);
		lesRPHTF.add(auteur);
		lesRPHTF.add(dateParution);
		lesRPHTF.add(presentation);
		lesRPHTF.add(dimensions);
		lesRPHTF.add(editeur);
		lesRPHTF.add(nbPages);
		lesRPHTF.add(collection);
		lesRPHTF.add(format);
		lesRPHTF.add(isbn);
		lesRPHTF.add(motsCles);
		lesRPHTF.add(prix);
		lesRPHTF.add(dewey);

		// j'ai mis une dimension commune pour toutes les zones de texte
		// Ça ne marche pas à cause de l'utilisation de GridBagLayout
		Dimension taillePref = new Dimension(200,30);

		tfISBN.setPreferredSize(taillePref);
		tfISBN.addActionListener(this);
		boutonGo.addActionListener(this);
		tfISBN.setActionCommand("1");
		boutonGo.setActionCommand("1");

		/* les ActionCommand permettent d'utiliser un même actionlistener pour
		 * plusieurs actions différentes.
		 * Ici, l'actioncommand 1 va chercher les informations du livre sur 
		 * internet (sur www.decitre.fr) et l'action 2 va enregistrer dans la bdd
		 */
		boutonEnregistrer.addActionListener(this);
		boutonEnregistrer.setActionCommand("2");

		boutonEnregistrer.addFocusListener(new FocusListener() {
			public void focusLost(FocusEvent e) {
				boutonEnregistrer.setText("Enregistrer");
			}

			public void focusGained(FocusEvent e) {
				boutonEnregistrer.setText("Pressez espace pour valider");

			}
		});

		boutonVider.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (RPlaceHolderTextField rphtf : lesRPHTF)
				{
					rphtf.efface();
				}
				resume.setText("");
				tfISBN.requestFocus();
				ageMax.efface();
				ageMin.efface();
				miBabelio.setEnabled(false);
			}
		});

		for (RPlaceHolderTextField rp : lesRPHTF) {
			rp.setPreferredSize(taillePref);
		}

		ageMin.setPreferredSize(new Dimension(98,30));
		ageMax.setPreferredSize(new Dimension(98,30));

		ageMin.changeTexte(FonctionsUtiles.LireChamp("livres", "ageDefaultMin"));
		ageMax.changeTexte(FonctionsUtiles.LireChamp("livres", "ageDefaultMax"));

		/*
		 * 	linewrap pour le textarea
		 */
		resume.setLineWrap(true);

		/*
		 * La combobox qui contient les écoles
		 */
		cbEcole.setPreferredSize(new Dimension(200,30));
		for (String uneEcole : FonctionsUtiles.listeDesEcoles()) {
			cbEcole.addItem(uneEcole);
		}
		cbEcole.setSelectedItem(FonctionsUtiles.LireChamp("ecoles", "defaut"));


		/* 
		 * LAYOUT
		 * Donc avant j'utilisais avec finesse et élégance un super
		 * GridBagLayout et des petits modulos et tout.
		 * Mais c'est moche.
		 * 
		 * Alors maintenant j'ai mis un gridlayout tout simple. C'est aussi moche, mais y a plus ce problème de champ
		 * rikiki.
		 */

		//0. ECOLE
		JPanel zeroiemeLigne = new JPanel();
		zeroiemeLigne.add(new JLabel("École : "));
		zeroiemeLigne.add(cbEcole);

		// 1. Layout du top panel
		JPanel premiereLigne = new JPanel();
		premiereLigne.add(new JLabel("Scanner un ISBN : "));
		premiereLigne.add(tfISBN);
		premiereLigne.add(boutonGo);
		premiereLigne.add(lStatut); // mais il ne sert à rien ce statut vide ! (tu vas voir)


		//2. Layout des informations des livres
		JPanel infos = new JPanel();
		//infos.setLayout(new GridBagLayout());
		infos.setLayout(new GridLayout(5,3)); // 5 lignes, 3 colonnes

		/*
		GridBagConstraints contraintes = new GridBagConstraints();

		contraintes.gridx = 0;
		contraintes.gridy = 0;
		contraintes.insets = new Insets(5,5,5,5);
		 */
		for (RPlaceHolderTextField rp : lesRPHTF) {
			infos.add(rp);

			/*
		  	infos.add(rp, contraintes);

			contraintes.gridx = (contraintes.gridx + 1)%3;
			if (contraintes.gridx==0) {
				contraintes.gridy+= 1;
			}
			 */

		}


		// 2.5 les deux âges dans la même cellule. 
		JPanel casounette = new JPanel();
		casounette.setLayout(new FlowLayout());
		casounette.add(ageMin);
		casounette.add(ageMax);
		/*
		infos.add(casounette,contraintes);
		 */
		infos.add(casounette);


		//3. Mise en place du résumé et des boutons

		this.setLayout(new GridBagLayout());

		GridBagConstraints autreCon = new GridBagConstraints();

		autreCon.fill= GridBagConstraints.BOTH;
		//autreCon.gridx = 0;
		autreCon.gridy = 0;
		autreCon.weighty=0;
		autreCon.weightx=1;
		//autreCon.anchor=GridBagConstraints.PAGE_START;

		this.add(zeroiemeLigne, autreCon);

		autreCon.gridy+=1;

		this.add(premiereLigne, autreCon);


		autreCon.fill = GridBagConstraints.HORIZONTAL;
		autreCon.gridy += 1;
		autreCon.weighty=0;
		this.add(infos, autreCon);

		autreCon.fill=GridBagConstraints.BOTH;
		autreCon.gridy+=1;
		autreCon.weighty=1;
		autreCon.insets = new Insets(5,20,5,20);
		//this.add(resume,autreCon);
		this.add(jspResume,autreCon);

		JPanel pBoutons = new JPanel();
		pBoutons.setLayout(new GridLayout(1,2));
		pBoutons.add(boutonVider);
		pBoutons.add(boutonEnregistrer);

		autreCon.fill=GridBagConstraints.NONE;
		autreCon.gridy+=1;
		autreCon.weighty=0.1;
		autreCon.insets = new Insets(0,0,0,0);
		this.add(pBoutons,autreCon);


		// textfield focus
		this.addComponentListener(new ComponentListener() {

			public void componentShown(ComponentEvent e) {
				PanneauAjout.this.requestFocusInWindow();
				tfISBN.requestFocus();
			}

			public void componentResized(ComponentEvent e) { }

			public void componentMoved(ComponentEvent e) { }

			public void componentHidden(ComponentEvent e) { }
		});
	}

	private void enregistreEtNettoie() {
		Map<String,Object> champs = new HashMap<String,Object>();
		champs.put("nom", nom.texte());
		champs.put("auteur", auteur.texte());
		champs.put("dateParution", dateParution.texte());
		champs.put("presentation", presentation.texte());
		champs.put("dimensions", dimensions.texte());
		champs.put("editeur", editeur.texte());
		champs.put("nbPages", nbPages.texte());
		champs.put("collection",collection.texte());
		champs.put("format",format.texte());
		champs.put("isbn", isbn.texte());
		if (!ageMin.texte().isEmpty()&&!ageMax.texte().isEmpty()) {
			champs.put("age", "["+ageMin.texte()+","+ageMax.texte()+"]");
		} else {
			champs.put("age", "");
		}
		champs.put("motsCles", motsCles.texte());
		champs.put("resume",resume.getText());
		champs.put("statut", "Disponible");
		champs.put("ecole", cbEcole.getSelectedItem().toString());
		champs.put("prix", prix.texte());
		champs.put("dewey", dewey.texte());

		ConnectionSQL.insertionLivre(champs);
		for (RPlaceHolderTextField rphtf : lesRPHTF)
		{
			rphtf.efface();
		}
		resume.setText("");
		tfISBN.requestFocus();
		ageMax.efface();
		ageMin.efface();
	}

	public void actionPerformed(ActionEvent e) {
		int action = Integer.parseInt(e.getActionCommand());
		switch(action) {
		case 1:

			String ISBN = tfISBN.getText();
			isbn.changeTexte(tfISBN.getText());
			tfISBN.setText("");

			if (!FonctionsUtiles.LireChamp("site", "proxy").isEmpty()) {
				System.setProperty("https.proxyHost", FonctionsUtiles.quelProxy());
				System.setProperty("https.proxyPort", FonctionsUtiles.quelPort());
			}


			/* 
			 * babelio fonctionne avec une méthode POST sur la page https://www.babelio.com/resrecherche.php
			 * avec comme paramètres item_recherche=isbn et Recherche=[isbn]
			 * 
			 * EN FAIT ça marche aussi en GET
			 * tant mieux
			 * https://www.babelio.com/resrecherche.php?Recherche=9782710300250&page=1&item_recherche=isbn&tri=users
			 * 
			 * Le résultat apparaît dans une page. Le lien vers le livre se trouve ensuite dans un href dont la classe est titre_v2 (...)
			 * 
			 * Et donc on trouve le résumé de l'œuvre sur la page dans un truc dont l'id est d_bio (et la class livre_resume)
			 *
			 * [0][0] <- c'est moi qui écarquille.
			 * 
			 * Il semble que babelio ne soit bon que pour les résumés.
			 * LAWL.
			 */

			/*
			 * Le message qui suit ne fonctionne que dans une version où il y aurait un site pref. Ce n'est pas le cas.
			 */
			/*	if (FonctionsUtiles.LireChamp("site", "pref").equals("babelio")) {
					JOptionPane.showMessageDialog(PanneauAjout.this, "Pour l'instant, seul le site decitre est supporté. Ça viendra...", "Pas de babelio pour l'instant..." , JOptionPane.INFORMATION_MESSAGE);
				}
			 */

			progressMonitor = new ProgressMonitor(PanneauAjout.this, "Connexion au site Decitre...","",0,100);
			progressMonitor.setProgress(0);
			progressMonitor.setMillisToPopup(0);
			progressMonitor.setMillisToDecideToPopup(0);

			TacheDecitre tache = new TacheDecitre(ISBN);
			tache.addPropertyChangeListener(PanneauAjout.this);
			tache.execute();

			break;
		case 2:
			// là on enregistre...
			if (nom.isVide() || auteur.isVide()) {
				JOptionPane.showMessageDialog(null, "Entrez au moins un nom d'ouvrage et un auteur.", "Erreur", JOptionPane.ERROR_MESSAGE);
			}
			else {
				String nomDuLivre = nom.texte();
				enregistreEtNettoie();
				JOptionPane.showMessageDialog(null, nomDuLivre+" a été ajouté à la bibliothèque.", "Ajout effectué", JOptionPane.PLAIN_MESSAGE);
			}
			boutonEnregistrer.setText("Enregistrer");
			break;
		}

	}

	class AmazonFields extends JDialog {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		JLabel lAmazon = new JLabel("<html>Malheureusement, ce livre n'était pas dans la base de données de Decitre." +
				"<br>Pour accéder aux informations, ABCDI a accédé à un autre site mais les informations sont variables. Les voici :</html>");
		JLabel lCopierColler = new JLabel("<html>Il est possible de copier-coller ces informations dans les champs voulus (avec CTRL-C CTRL-V)");

		JEditorPane jepAmal = new JEditorPane();

		JButton bFermer = new JButton("Fermer");
		JButton bOuvrir = new JButton("Accéder à la page web");

		JPanel pContaineur = new JPanel();

		JPanel l1 = new JPanel();
		JPanel l2 = new JPanel();
		JPanel l3 = new JPanel();
		JPanel l4 = new JPanel();

		AmazonFields(String chaine, String adresse) {
			try {
				Image imageIcone = ImageIO.read(new File("resources/ajouter.png"));
				this.setIconImage(imageIcone);
			} catch (Exception e) {
				e.printStackTrace();
			}

			pContaineur.setLayout(new BoxLayout(pContaineur, BoxLayout.PAGE_AXIS));

			l1.setLayout(new FlowLayout());
			l2.setLayout(new FlowLayout());
			l3.setLayout(new FlowLayout());
			l4.setLayout(new FlowLayout());

			l1.add(lAmazon);

			jepAmal.setContentType("text/html");
			jepAmal.setText(chaine);

			l2.add(jepAmal);

			l3.add(lCopierColler);

			bOuvrir.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					try {
						URI amazonURI = new URI(adresse);
						Desktop.getDesktop().browse(amazonURI);
					} catch (IOException ioe) {
						ioe.printStackTrace();
						JOptionPane.showMessageDialog(AmazonFields.this, "<html>Impossible d'accéder à la page demandée.<br>"+ioe.getLocalizedMessage()+"</html>","Erreur.",JOptionPane.ERROR_MESSAGE);
					} catch (URISyntaxException e1) {
						e1.printStackTrace();
						JOptionPane.showMessageDialog(AmazonFields.this, "<html>Impossible d'accéder à la page demandée.<br>"+e1.getLocalizedMessage()+"</html>","Erreur.",JOptionPane.ERROR_MESSAGE);
					}
					AmazonFields.this.dispose();
				}
			});			
			bFermer.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					AmazonFields.this.dispose();
				}
			});

			l4.add(bOuvrir);
			l4.add(bFermer);
			pContaineur.add(l1);
			pContaineur.add(l2);
			pContaineur.add(l3);
			pContaineur.add(l4);

			this.add(pContaineur);

			this.setTitle("Informations");
			this.pack();

			this.setLocationRelativeTo(PanneauAjout.this);
			this.setVisible(true);	
		}
	}

	class TacheDecitre extends SwingWorker<Void,Void> {
		private String ISBN;

		public TacheDecitre(String ISBN) {
			this.ISBN=ISBN;
		}

		@Override
		protected Void doInBackground() throws Exception {
			try {
				URL merciDecitre = new URL("https://www.decitre.fr/rechercher/result?q="+ISBN);

				HttpURLConnection connectionDecitre = (HttpURLConnection)merciDecitre.openConnection();
				connectionDecitre.setRequestProperty("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.79 Safari/537.36");
				connectionDecitre.connect();

				String line= null;
				StringBuffer tmp = new StringBuffer();

				BufferedReader in = new BufferedReader(new InputStreamReader(connectionDecitre.getInputStream()));
				// ça raise une invalid charset exception que je ne suis pas obligé de catch.

				System.out.println("Connexion Decitre OK.");
				setProgress(25);
				
				while ((line=in.readLine())!=null) {
					tmp.append(line);
				}
				Document doc = Jsoup.parse(String.valueOf(tmp));
				in.close();

				System.out.println("Page téléchargée.");
				setProgress(50);
				
				Elements livre = doc.getElementsByClass("product-title");
				if (livre.last() != null) {
					// Je reste dans cette condition dorénavant.
					nom.changeTexte(livre.last().text());
					Element author = doc.getElementsByClass("authors").first();
					auteur.changeTexte(author.text());

					Element elPrix = doc.getElementsByClass("col-main").first().getElementsByClass("final-price").first();
					prix.changeTexte(elPrix.text());

					Elements li = doc.select("li.information");
					for (Element lili : li) {
						if (lili.getElementsByClass("name").select("span").html().equals("Date de parution")) {
							dateParution.changeTexte(lili.getElementsByClass("value").first().html());
						}
						if (lili.getElementsByClass("name").select("span").html().equals("Editeur")) {
							if (lili.getElementsByClass("value").select("a").html().isEmpty()) {
								editeur.changeTexte(lili.getElementsByClass("value").first().html());
							} else {
								editeur.changeTexte(lili.getElementsByClass("value").select("a").html());
							}
						}
						if (lili.getElementsByClass("name").select("span").html().equals("Collection")) {
							if (lili.getElementsByClass("value").select("a").html().isEmpty()) {
								collection.changeTexte(lili.getElementsByClass("value").first().html());
							} else {
								collection.changeTexte(lili.getElementsByClass("value").select("a").html());
							}
						}
						if (lili.getElementsByClass("name").select("span").html().equals("Format")) {
							format.changeTexte(lili.getElementsByClass("value").first().html());
						}
						if (lili.getElementsByClass("name").select("span").html().equals("Présentation")) {
							presentation.changeTexte(lili.getElementsByClass("value").first().html());
						}
						if (lili.getElementsByClass("name").select("span").html().equals("Nb. de pages")) {
							nbPages.changeTexte(lili.getElementsByClass("value").first().html());
						}
						if (lili.getElementsByClass("name").select("span").html().equals("Dimensions")) {
							dimensions.changeTexte(lili.getElementsByClass("value").first().html());
						}

					}				
					System.out.println("Champs remplis");
					setProgress(75);

					// la condition suivante est un peu moche mais que voulez-vous ?
					/*
					 * fun fact : elle était fausse aussi. Ça buggait.
					 */
					if (!((doc.select("div[itemprop=description]").text().isEmpty()) || (Boolean.valueOf(FonctionsUtiles.LireChamp("livres", "ajoutSansResume"))))) {
						Element description = doc.select("div[itemprop=description]").last();
						resume.setText(description.text());
						if (Boolean.valueOf(FonctionsUtiles.LireChamp("livres", "ajoutSansValidation"))) {
							String nomDuLivre = "";
							if (nom.texte().length()>12) {
								nomDuLivre = nom.texte().substring(0,10)+"...";
							} else {
								nomDuLivre = nom.texte();
							}
							enregistreEtNettoie();
							lStatut.setText(nomDuLivre+" a été ajouté.");
						}
						else {
							boutonEnregistrer.requestFocus();
						}
					}
					else {
						resume.setText(doc.getElementById("resume").getElementsByClass("content").first().text());
						resume.requestFocus();
					}
					miBabelio.setEnabled(true);
					
					System.out.println("Résumé indiqué (ou pas).");
					setProgress(100);
				}
				else {
					progressMonitor.close();
					
					progressMonitor = new ProgressMonitor(PanneauAjout.this, "La connexion au site Decitre a échoué.\nRecherche des informations sur Amazon.\nCela prend beaucoup plus de temps...","",0,100);
					progressMonitor.setProgress(0);
					progressMonitor.setMillisToPopup(0);
					progressMonitor.setMillisToDecideToPopup(0);

					Tache tache = new Tache(ISBN);
					tache.addPropertyChangeListener(PanneauAjout.this);
					tache.execute();

				}

				if (!FonctionsUtiles.LireChamp("site", "proxy").isEmpty()) {
					System.clearProperty("https.proxyHost");
				}

			}
			catch(MalformedURLException mfurl) {
				JOptionPane.showMessageDialog(PanneauAjout.this, "<html>Impossible d'établir la connexion avec le site.<br>Réessayer ou contacter le développeur si le problème persiste.<br><small>Malformed URL Exception</small></html>","Erreur de connexion", JOptionPane.ERROR_MESSAGE);
				mfurl.printStackTrace();
			}
			catch(IOException io) {
				io.printStackTrace();
				if (io.getLocalizedMessage().contains("Server returned HTTP response code: 503")) {
					int reponse = JOptionPane.showConfirmDialog(PanneauAjout.this, "<html>Impossible d'établir la connexion avec le site.<br><small>" +
							"IO Exception - Erreur 503</small><br>Ouvrir la page du livre dans le navigateur ?</html>", "Erreur de connexion", JOptionPane.YES_NO_OPTION);
					//JOptionPane.showConfirmDialog(PanneauAjout.this, "<html>Impossible d'établir la connexion avec le site.<br>Réessayer ou contacter le développeur si le problème persiste.<br><small>IO Exception</small></html>","Erreur de connexion", JOptionPane.ERROR_MESSAGE);
					//io.printStackTrace();
					if (reponse==0) {
						try {
							URI amazonURI = new URI(io.getLocalizedMessage().substring(49));
							Desktop.getDesktop().browse(amazonURI);
						} catch (URISyntaxException uriSynt) {
							uriSynt.printStackTrace();
						} catch (IOException ioioio) {
							ioioio.printStackTrace();
						}

					} else {

					}
				} else {
					JOptionPane.showMessageDialog(PanneauAjout.this, "<html>Impossible d'établir la connexion avec le site.<br>Réessayer ou contacter le développeur si le problème persiste.<br><small>IO Exception</small></html>","Erreur de connexion", JOptionPane.ERROR_MESSAGE);
				}
			}
			return null;
		}

	}

	class Tache extends SwingWorker<Void,Void> {
		private String ISBN;

		public Tache(String ISBN) {
			this.ISBN=ISBN;
		}

		@Override
		public Void doInBackground() {
			try {
				URL merciAmazon = new URL("https://www.amazon.com/s?rh=p_66%3A"+ISBN);

				/*
				 * Je set le User Agent car Amazon refuse les connexions qui s'appellent java ou même wget.
				 * Du coup en mettant le user-agent à une version valable de mozilla, bim ça marche.
				 */

				HttpURLConnection connection = (HttpURLConnection)merciAmazon.openConnection();
				connection.setRequestProperty("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.79 Safari/537.36");
				connection.connect();

				String ligne= null;
				StringBuffer bufTemp = new StringBuffer();

				BufferedReader brIn = new BufferedReader(new InputStreamReader(connection.getInputStream()));

				setProgress(20);
				System.out.println("Connexion Amazon OK.");

				while((ligne=brIn.readLine())!=null) {
					bufTemp.append(ligne);
				}
				Document docTemp = Jsoup.parse(String.valueOf(bufTemp));
				brIn.close();

				setProgress(30);
				System.out.println("Fichier téléchargé. Téléchargement de la 2e page.");

				Elements adresses = docTemp.getElementsByClass("s-result-item");

				/*
				 * C'est au pluriel même s'il n'y au qu'une adresse a priori. En effet : avant j'avais une recherche par Id maintenant c'est par class.
				 */

				System.out.println(adresses.isEmpty());

				if (!adresses.isEmpty()) {
					System.out.println(adresses.get(0).text());
					if (!adresses.get(0).text().contains("Aucun résultat")) {
						String pageDuLivre= "https://www.amazon.fr"+adresses.get(0).getElementsByTag("a").get(0).attr("href");
						System.out.println(pageDuLivre);
						URL livreAmazon = new URL(pageDuLivre);

						HttpURLConnection connection2 = (HttpURLConnection)livreAmazon.openConnection();
						connection2.setRequestProperty("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.79 Safari/537.36");
						connection2.connect();

						System.out.println("Connexion deuxième page OK.");
						setProgress(40);

						ligne=null;
						StringBuffer encoreUnBuffer = new StringBuffer();

						BufferedReader encoreUnBR = new BufferedReader(new InputStreamReader(connection2.getInputStream()));
						while ((ligne=encoreUnBR.readLine())!=null) {
							encoreUnBuffer.append(ligne);
						}
						Document objectifAmazon = Jsoup.parse(String.valueOf(encoreUnBuffer));
						encoreUnBR.close();

						System.out.println("Page du livre téléchargée.");
						setProgress(50);

						Element nomLivre = objectifAmazon.getElementById("productTitle");
						nom.changeTexte(nomLivre.html());

						System.out.println("Nom du livre indiqué.");
						setProgress(60);

						Element elPrix = objectifAmazon.getElementById("price");
						if (elPrix==null) {
							elPrix = objectifAmazon.getElementById("newBuyBoxPrice");
						}
						prix.changeTexte(elPrix.text());

						System.out.println("Prix fixé.");
						setProgress(70);

						Element elEditorialReviews = objectifAmazon.getElementById("editorialReviews_feature_div");
						if (elEditorialReviews!=null) {
							Elements titresER = elEditorialReviews.getElementsByTag("h3");
							for (Element titreER : titresER) {
								if (titreER.text().contains("Quatrième de couverture")) {
									resume.setText(titreER.nextElementSibling().text());
								}
							}
						}
						System.out.println("Résumé déterminé (ou pas).");
						setProgress(80);

						Elements contributorNameID = objectifAmazon.getElementsByClass("contributorNameID");
						String texteAuteur = "";

						for (Element cnid : contributorNameID) {
							texteAuteur += cnid.getElementsByTag("a").get(0).html();
							if (!contributorNameID.get(contributorNameID.size()-1).equals(cnid)) {
								// si c'est pas le dernier
								texteAuteur+= ", ";
							}
						}
						auteur.changeTexte(texteAuteur);

						System.out.println("Auteur(s) déterminé(s).");
						setProgress(90);

						//Elements details = objectifAmazon.getElementById("detailBullets_feature_div").getElementsByTag("li");
						Elements details = objectifAmazon.getElementsByClass("rpi-attribute-content");
						if (!details.isEmpty()) {
							for (Element detail:details) {
								String label = detail.getElementsByClass("rpi-attribute-label").get(0).text();
								String value = detail.getElementsByClass("rpi-attribute-value").get(0).text();

								if (label.contains("Date de publication")) {
									dateParution.changeTexte(value);
								}
								if (label.contains("Éditeur")) {
									editeur.changeTexte(value);
								}
								if (label.contains("Dimensions")) {
									dimensions.changeTexte(value);
								}
								if (label.contains("Nombre de pages")) {
									nbPages.changeTexte(value);
								}
							}
						} else {
							Element divDetails = objectifAmazon.getElementById("prodDetails");
							Elements trDetails = divDetails.getElementsByTag("table").first().getElementsByTag("tr");
							String informations = "<html>";
							for (Element detail : trDetails) {
								informations += detail.getElementsByTag("th").first().text()+" : "+detail.getElementsByTag("td").first().text()+"<br>";
							}
							informations +="</html>";
							new AmazonFields(informations,pageDuLivre);
						}

						System.out.println("Détails intégrés (indibutablement).");
						setProgress(100);
					}
					else {

						setProgress(100);
						JOptionPane.showMessageDialog(null, "Désolé, ce livre n'est pas dans les bases de données des sites utilisés.\nIl va falloir l'entrer à la main...", "Erreur", JOptionPane.INFORMATION_MESSAGE);

					}
				}
				else {

					setProgress(100);
					JOptionPane.showMessageDialog(null, "Désolé, ce livre n'est pas dans les bases de données des sites utilisés.\nIl va falloir l'entrer à la main...", "Erreur", JOptionPane.INFORMATION_MESSAGE);

				}
			}
			catch (IOException ioe) {
				ioe.printStackTrace();
				setProgress(100);
				if (ioe.getLocalizedMessage().contains("Server returned HTTP response code: 503")) {
					int reponse = JOptionPane.showConfirmDialog(PanneauAjout.this, "<html>Impossible d'établir la connexion avec le site.<br><small>" +
							"IO Exception - Erreur 503</small><br>Ouvrir la page du livre dans le navigateur ?</html>", "Erreur de connexion", JOptionPane.YES_NO_OPTION);
					//JOptionPane.showConfirmDialog(this, "<html>Impossible d'établir la connexion avec le site.<br>Réessayer ou contacter le développeur si le problème persiste.<br><small>IO Exception</small></html>","Erreur de connexion", JOptionPane.ERROR_MESSAGE);
					//io.printStackTrace();
					if (reponse==0) {
						try {
							URI amazonURI = new URI(ioe.getLocalizedMessage().substring(49));
							Desktop.getDesktop().browse(amazonURI);
						} catch (URISyntaxException uriSynt) {
							uriSynt.printStackTrace();
						} catch (IOException ioioio) {
							ioioio.printStackTrace();
						}

					} else {

					}
				} else if (ioe.getLocalizedMessage().contains("Server returned HTTP response code: 400")) {
					JOptionPane.showMessageDialog(PanneauAjout.this, "<html>Impossible d'établir la connexion avec le site.<br><small>" +
							"Erreur 400 : Bad Request</small><br>Vérifier l'ISBN.</html>", "Erreur de connexion", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(PanneauAjout.this, "<html>Impossible d'établir la connexion avec le site.<br>Réessayer ou contacter le développeur si le problème persiste.<br><small>IO Exception</small></html>","Erreur de connexion", JOptionPane.ERROR_MESSAGE);
				}
			}
			return null;
		}

	}

	private ActionListener alBabelio = new ActionListener() {
		public void actionPerformed(ActionEvent e) {			
			if (isbn.texte().isEmpty()) {
				JOptionPane.showMessageDialog(PanneauAjout.this, "Cette action doit être effectuée lorsqu'un ISBN est inscrit dans la cellule réservée à l'ISBN", "Erreur d'ISBN", JOptionPane.WARNING_MESSAGE);
			}
			else {
				String cetISBN = isbn.texte();

				try {

					if (!FonctionsUtiles.LireChamp("site", "proxy").isEmpty()) {
						System.setProperty("https.proxyHost", FonctionsUtiles.quelProxy());
						System.setProperty("https.proxyPort", FonctionsUtiles.quelPort());
					}

					URL merciBabelio = new URL("https://www.babelio.com/resrecherche.php?Recherche="+cetISBN+"&page=1&item_recherche=isbn&tri=users");

					String line= null;
					StringBuffer tmp = new StringBuffer();

					BufferedReader in = new BufferedReader(new InputStreamReader(merciBabelio.openStream(), "UTF-8"));
					// ça raise une invalid charset exception que je ne suis pas obligé de catch.

					while ((line=in.readLine())!=null) {
						tmp.append(line);
					}
					Document doc = Jsoup.parse(String.valueOf(tmp));
					in.close();

					Elements classURL = doc.getElementsByClass("titre_v2");

					if (!classURL.isEmpty()) {
						URL livreBabelio = new URL("https://www.babelio.com"+classURL.get(0).attr("href"));

						line=null;
						StringBuffer encoreUnBuffer = new StringBuffer();

						BufferedReader encoreUnBR = new BufferedReader(new InputStreamReader(livreBabelio.openStream(),"iso-8859-1"));
						while ((line=encoreUnBR.readLine())!=null) {
							encoreUnBuffer.append(line);
						}
						Document objectifAmazon = Jsoup.parse(String.valueOf(encoreUnBuffer));
						encoreUnBR.close();

						resume.setText(objectifAmazon.getElementById("d_bio").text());					
					}
					else
					{
						JOptionPane.showMessageDialog(PanneauAjout.this, "<html>L'ISBN entrée n'est pas référencée sur Babelio.</html>", "Problème d'ISBN", JOptionPane.WARNING_MESSAGE);
					}

					if (!FonctionsUtiles.LireChamp("site", "proxy").isEmpty()) {
						System.clearProperty("https.proxyHost");
					}

				}
				catch (MalformedURLException mue) {
					JOptionPane.showMessageDialog(PanneauAjout.this, "<html>Impossible d'établir la connexion avec le site.<br>Réessayer ou contacter le développeur si le problème persiste.<br><small>Malformed URL Exception</small></html>","Erreur de connexion", JOptionPane.ERROR_MESSAGE);
					mue.printStackTrace();
				}
				catch (IOException ioe) {
					JOptionPane.showMessageDialog(PanneauAjout.this, "<html>Impossible d'établir la connexion avec le site.<br>Réessayer ou contacter le développeur si le problème persiste.<br><small>IO Exception</small></html>","Erreur de connexion", JOptionPane.ERROR_MESSAGE);
					ioe.printStackTrace();
				}
			}			
		}
	};

	@Override
	public void propertyChange(PropertyChangeEvent arg0) {
		if ("progress" == arg0.getPropertyName()) {
			int progress = (Integer)arg0.getNewValue();
			progressMonitor.setProgress(progress);
		}

	}

}
