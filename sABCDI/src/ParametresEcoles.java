import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


public class ParametresEcoles extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	JLabel lListeEcoles = new JLabel();
	
	String listeEcoles() {
		String stringEcoles;
		stringEcoles = "<html><center>Les écoles incluses dans le logiciel sont les suivantes : ";
		int i=0;
		for (String ecole : FonctionsUtiles.listeDesEcoles()) {
			if (i>0) stringEcoles += ", ";
			stringEcoles += ecole;
			i++;
		}
		stringEcoles+=".</center></html>";
		
		return stringEcoles;
	}
	
	JLabel lEcoleParDefaut = new JLabel("L'école par défaut est "+FonctionsUtiles.LireChamp("ecoles", "defaut")+".");
	
	RPlaceHolderTextField tAjoutEcole = new RPlaceHolderTextField("École");
	JButton bAjoutEcole = new JButton("Ajouter");
	
	JComboBox<String> cbEcoles = new JComboBox<String>();

	JButton bSetDefault = new JButton("Utiliser par défaut");
	JButton bRename = new JButton("Renommer");
	JButton bSupprimerEcole = new JButton("Supprimer");
	
	JDialog dQueFaireDesLivres = new JDialog();

	
	public ParametresEcoles () {
		// this.setLayout(new GridBagLayout());
		
		// ligne 1
		JPanel premiereLigne = new JPanel();
		premiereLigne.setLayout(new FlowLayout());
		/*
		GridBagConstraints cPE = new GridBagConstraints();
		cPE.fill = GridBagConstraints.BOTH;
		cPE.anchor=GridBagConstraints.PAGE_START;
		cPE.insets=new Insets(5, 0, 5, 0);
		cPE.gridx=0;
		cPE.gridy=0;
		cPE.gridwidth=3;
		cPE.weightx=1;
		*/
		
		lListeEcoles.setText(listeEcoles());
		lListeEcoles.setHorizontalAlignment(JLabel.CENTER);
		premiereLigne.add(lListeEcoles);
		
		// ligne 2
		/*
		cPE.weightx=0.5;
		cPE.gridwidth=1;
		cPE.gridy += 1;
		cPE.gridx = 0;
		*/
		JPanel deuxiemeLigne = new JPanel();
		deuxiemeLigne.setLayout(new FlowLayout());
		JLabel lNouvEcole = new JLabel("Ajouter une école : ");
		lNouvEcole.setHorizontalAlignment(JLabel.RIGHT);
		deuxiemeLigne.add(lNouvEcole);
		
		tAjoutEcole.addActionListener(alAjoutEcole);
		tAjoutEcole.setToolTipText("Le nom ne pourra contenir que des lettres (majuscules, minuscules) et des espaces.");
		/*
		 * cPE.gridx += 1;
		 */
		tAjoutEcole.setPreferredSize(new Dimension(200,30));
		deuxiemeLigne.add(tAjoutEcole);
		
		bAjoutEcole.addActionListener(alAjoutEcole);
		/*
		 * cPE.gridx+=1;
		 */
		deuxiemeLigne.add(bAjoutEcole);
		
		// ligne 2.5
		JPanel deuxVCinqLigne = new JPanel();
		deuxVCinqLigne.setLayout(new FlowLayout());
		deuxVCinqLigne.add(lEcoleParDefaut);
		
		
		// ligne 3
		JPanel troisiemeLigne = new JPanel();
		troisiemeLigne.setLayout(new FlowLayout());
		/*
		 * cPE.gridy+=1;
		 * cPE.gridx=0;
		 */
		JLabel lEcole = new JLabel("École : ");
		lEcole.setHorizontalAlignment(JLabel.RIGHT);
		troisiemeLigne.add(lEcole);
		
		/*
		 * cPE.gridx+= 1;
		 */
		for (String ecole : FonctionsUtiles.listeDesEcoles()) {
			cbEcoles.addItem(ecole);
		}
		troisiemeLigne.add(cbEcoles);
		
		/*
		 * cPE.gridx+=1;
		 */
		bSetDefault.addActionListener(alSetDefaultEcole);
		troisiemeLigne.add(bSetDefault);
		
		bRename.addActionListener(alRename);
		troisiemeLigne.add(bRename);
		
		/*
		 * cPE.gridy+=1;
		 */
		bSupprimerEcole.addActionListener(alSuppressionEcole);
		troisiemeLigne.add(bSupprimerEcole);
		
		// ligne 4
		/*
		 * cPE.gridy+=1;
		 * cPE.gridx = 0;
		 */
		
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		this.add(premiereLigne);
		this.add(deuxiemeLigne);
		this.add(deuxVCinqLigne);
		this.add(troisiemeLigne);
		
	}
	
	private ActionListener alAjoutEcole = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			if (tAjoutEcole.isVide()) {
				JOptionPane.showMessageDialog(ParametresEcoles.this, "Entrer un nom d'école.", "Nom d'école manquant", JOptionPane.INFORMATION_MESSAGE);
			}
			else {
				if (FonctionsUtiles.listeDesEcoles().contains(tAjoutEcole.texte())) {
					JOptionPane.showMessageDialog(ParametresEcoles.this, "Cette école est déjà dans la base.", "École déjà présente", JOptionPane.INFORMATION_MESSAGE);
				}
				else {
					String nomEcole = tAjoutEcole.texte().replaceAll("[^A-Za-z0-9 -]", "");
					EcrireParametres.Ajouter("ecoles", nomEcole);
					JOptionPane.showMessageDialog(ParametresEcoles.this, "L'école "+nomEcole+" a été ajoutée avec succès.", "École ajoutée", JOptionPane.INFORMATION_MESSAGE);
				
					cbEcoles.addItem(nomEcole);
					lListeEcoles.setText(listeEcoles());
					tAjoutEcole.efface();
					/*
					 * Je ne parviens pas à refresh la JFrame complète alors j'essaie de faire ce qu'il faut...
					 */
				}
				
			}
		}
	};
	
	private ActionListener alSuppressionEcole = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			String nomEcole = cbEcoles.getSelectedItem().toString();
			int reponse = JOptionPane.showConfirmDialog(ParametresEcoles.this, "Supprimer l'école "+nomEcole+" ?", "Confirmation de suppression", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			if (reponse==0) {
				// la réponse est oui.
				List<Map<String,Object>> elevesDeCetteEcole = ConnectionSQL.selection("SELECT rowid AS id,* FROM eleves WHERE ecole='"+nomEcole+"';", "eleves");
				if (!elevesDeCetteEcole.isEmpty()) {
					JOptionPane.showMessageDialog(ParametresEcoles.this,"Il y a des élèves dans cette école, il faut les supprimer avant de supprimer l'école.",
							"Erreur", JOptionPane.ERROR_MESSAGE);
				} else {
					List<Map<String,Object>> livresDeCetteEcole = ConnectionSQL.selection("SELECT rowid AS id,* FROM livres WHERE ecole='"+nomEcole+"';", "livres");
					if (livresDeCetteEcole.isEmpty()) {
						// ici on efface l'école
						int indice = cbEcoles.getSelectedIndex(); // je sauve son indice dans la liste pour pouvoir l'effacer après
						EcrireParametres.Supprimer("ecoles", nomEcole);
						cbEcoles.setSelectedIndex(0);
						cbEcoles.removeItemAt(indice);

						if (AuChocolat.parametres.get("ecoles").size()<=1) {
							String premiereEcole=null;
							while ((premiereEcole==null)||(premiereEcole.equals("")))
							{
								premiereEcole = JOptionPane.showInputDialog(null, "<html>Il n'y a aucune école de paramétrée dans le logiciel.<br>Quel est le nom de l'école ?</html>");
							}
							EcrireParametres.Changer("ecoles", "defaut", premiereEcole);
							EcrireParametres.Ajouter("ecoles", premiereEcole);
							cbEcoles.addItem(premiereEcole);
						}
						lListeEcoles.setText(listeEcoles());

						JOptionPane.showMessageDialog(ParametresEcoles.this, "L'école "+nomEcole+" a bien été supprimée.", "Suppression réussie", JOptionPane.INFORMATION_MESSAGE);
					} else {
						String leMessageDuNombreDeLivres ="<html>Cette école dispose de "+livresDeCetteEcole.size()+" livre";
						if (livresDeCetteEcole.size()>1) {
							leMessageDuNombreDeLivres += "s";
						}
						leMessageDuNombreDeLivres+=". Qu'en faire ?</html>";
						String[] choix = {"Basculer vers une autre école", "Supprimer", "Cas par cas", "Annuler"};
						int choixQuoiFaire = JOptionPane.showOptionDialog(ParametresEcoles.this, leMessageDuNombreDeLivres, "Que faire des livres ?", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE,
								null, choix, choix[0]);
						System.out.println(choixQuoiFaire);
						switch (choixQuoiFaire) {
						case 0:
							// basculer vers une autre école
							if (FonctionsUtiles.listeDesEcoles().size()>1) {
								String[] choixDEcoles= new String[FonctionsUtiles.listeDesEcoles().size()-1];
								int i =0;
								for (String uneEcole : FonctionsUtiles.listeDesEcoles()) {
									if (!uneEcole.equals(nomEcole)) {
										choixDEcoles[i] = uneEcole;
										i++;
									}
								}
								Object choixEcoleBascule = JOptionPane.showInputDialog(ParametresEcoles.this, "Vers quelle école basculer les livres ?", "Choix à bascule", JOptionPane.QUESTION_MESSAGE, null,
										choixDEcoles, choixDEcoles[0]);
								if (choixEcoleBascule!=null) {
									// Bon sang que c'est compliqué...
									ConnectionSQL.operation("UPDATE livres SET ecole='"+choixEcoleBascule+"' WHERE ecole='"+nomEcole+"';");
									int indice = cbEcoles.getSelectedIndex();
									EcrireParametres.Supprimer("ecoles", nomEcole);
									cbEcoles.setSelectedIndex(0);
									cbEcoles.removeItemAt(indice);
									if (AuChocolat.parametres.get("ecoles").size()<=1) {
										String premiereEcole=null;
										while ((premiereEcole==null)||(premiereEcole.equals("")))
										{
											premiereEcole = JOptionPane.showInputDialog(null, "<html>Il n'y a aucune école de paramétrée dans le logiciel.<br>Quel est le nom de l'école ?</html>");
										}
										EcrireParametres.Changer("ecoles", "defaut", premiereEcole);
										
										EcrireParametres.Ajouter("ecoles", premiereEcole);
										
										cbEcoles.addItem(premiereEcole);
									}
									lListeEcoles.setText(listeEcoles());
									JOptionPane.showMessageDialog(ParametresEcoles.this, "L'école "+nomEcole+" a bien été supprimée.", "Suppression réussie", JOptionPane.INFORMATION_MESSAGE);
								}
							}
							else {
								JOptionPane.showMessageDialog(ParametresEcoles.this, "<html>Bascule impossible : il n'y a pas d'autre école dans le logiciel.</html>", "Bascule impossible", JOptionPane.INFORMATION_MESSAGE);
							}
							break;
						case 1:
							// Supprimer
							ConnectionSQL.operation("UPDATE livres SET statut='Disparu', ecole='' WHERE ecole='"+nomEcole+"';");
							int indice = cbEcoles.getSelectedIndex();
							EcrireParametres.Supprimer("ecoles", nomEcole);
							cbEcoles.setSelectedIndex(0);
							cbEcoles.removeItemAt(indice);
							
							if (AuChocolat.parametres.get("ecoles").size()<=1) {
								String premiereEcole=null;
								while ((premiereEcole==null)||(premiereEcole.equals("")))
								{
									premiereEcole = JOptionPane.showInputDialog(null, "<html>Il n'y a aucune école de paramétrée dans le logiciel.<br>Quel est le nom de l'école ?</html>");
								}
								EcrireParametres.Changer("ecoles", "defaut", premiereEcole);
								
								EcrireParametres.Ajouter("ecoles", premiereEcole);
								
								cbEcoles.addItem(premiereEcole);
							}
							lListeEcoles.setText(listeEcoles());
							JOptionPane.showMessageDialog(ParametresEcoles.this, "L'école "+nomEcole+" a bien été supprimée.", "Suppression réussie", JOptionPane.INFORMATION_MESSAGE);
							break;
						case 2:
							// voir au cas par cas
							for (Map<String,Object> unLivre : livresDeCetteEcole) {	
								new DetailLivre(Integer.parseInt(unLivre.get("id").toString()));
							}
							JOptionPane.showMessageDialog(ParametresEcoles.this, "Après cette opération, cliquer à nouveau sur le bouton Supprimer pour définitivement supprimer l'école "+nomEcole, "Information", JOptionPane.INFORMATION_MESSAGE);
							break;
						}

					}
				}
				
			}
			else {
				// ici on ne fait rien. C'est si l'utilisateur a répondu non.
				// mais alors pourquoi un else ???
				// heu
				// ta gueule ?
			}
			
		}
	};
	
	private ActionListener alSetDefaultEcole = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			EcrireParametres.Changer("ecoles", "defaut", cbEcoles.getSelectedItem().toString());
			JOptionPane.showMessageDialog(ParametresEcoles.this, "L'école par défaut est maintenant "+cbEcoles.getSelectedItem().toString()+".", "Changement effectué", JOptionPane.INFORMATION_MESSAGE);
		}
	};
	
	private ActionListener alRename = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			boolean onChange = true;
			
			String ancienNom = cbEcoles.getSelectedItem().toString();
			String nouveauNom = JOptionPane.showInputDialog(ParametresEcoles.this, 
					"Quel sera le nouveau nom de l'école "+ancienNom+" ?", 
					"Changement de nom", 
					JOptionPane.QUESTION_MESSAGE);
			for (String ecole : FonctionsUtiles.listeDesEcoles()) {
				if (nouveauNom.equals(ecole)) {
					int reponse = JOptionPane.showConfirmDialog(ParametresEcoles.this, "Cette école existe déjà, fusionner ?", "École déjà existante", JOptionPane.YES_NO_OPTION);
					// 0 c'est oui, 1 c'est non
					if (reponse == 1) {
						onChange = false;
					}
						
				}
			}
			if (onChange) {
				// donc là on change
				ConnectionSQL.operation("UPDATE livres SET ecole='"+nouveauNom+"' WHERE ecole='"+ancienNom+"';");
				ConnectionSQL.operation("UPDATE eleves SET ecole='"+nouveauNom+"' WHERE ecole='"+ancienNom+"';");

				EcrireParametres.Supprimer("ecoles", cbEcoles.getSelectedItem().toString());
				EcrireParametres.Ajouter("ecoles", nouveauNom);
				
				// et si l'école était par défaut
				if (FonctionsUtiles.LireChamp("ecoles", "defaut").equals(ancienNom)) {
					EcrireParametres.Changer("ecoles", "defaut", nouveauNom);
				}
				
				// ensuite je remets la liste des écoles de la combobox à jour
				cbEcoles.removeAllItems();
				for (String ecole : FonctionsUtiles.listeDesEcoles()) {
					cbEcoles.addItem(ecole);
				}
			}
		}
	};
}
