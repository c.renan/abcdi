import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;


public class ParametresRecherche extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public JRadioButton rbLivres = new JRadioButton("Livres");
	public JRadioButton rbEleves = new JRadioButton("Élèves");
	ButtonGroup bg = new ButtonGroup();
	
	JButton bValider = new JButton("Valider les changements");
	
	JCheckBox xbNom = new JCheckBox("Nom");
	JCheckBox xbAuteur = new JCheckBox("Auteur");
	JCheckBox xbStatut = new JCheckBox("Statut");
	JCheckBox xbEcole = new JCheckBox("École");

	JCheckBox xbNomEleve = new JCheckBox("Nom");
	JCheckBox xbPrenom = new JCheckBox("Prénom");
	JCheckBox xbEcoleEleve = new JCheckBox("École");
	JCheckBox xbClasse = new JCheckBox("Classe");
	
	Map<String,JCheckBox> lesXB = new LinkedHashMap<String,JCheckBox>();
	Map<String,JCheckBox> lesXBEleves = new LinkedHashMap<String, JCheckBox>();
	
	JCheckBox xbSansAgeOk = new JCheckBox("Lorsque l'âge est précisé dans la recherche, inclure les résultats sans âge");
	
	JPanel premiereLigne = new JPanel();
	JPanel deuxiemeLigne = new JPanel();
	JPanel troisiemeLigne = new JPanel();
	JPanel quatriemeLigne = new JPanel();
	JPanel cinquiemeLigne = new JPanel();
	
	public ParametresRecherche() {
		
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		// ligne 1
		premiereLigne.setLayout(new FlowLayout());
		premiereLigne.add(new JLabel("Recherche par défaut : "));
		bg.add(rbEleves);
		bg.add(rbLivres);
		if (FonctionsUtiles.LireChamp("recherche", "defautRecherche").equals("livres")) {
			rbLivres.setSelected(true);
		} else {
			rbEleves.setSelected(true);
		}
		premiereLigne.add(rbLivres);
		premiereLigne.add(rbEleves);
		
		// ligne 2
		deuxiemeLigne.setLayout(new FlowLayout());
		deuxiemeLigne.add(new JLabel("Colonnes \"livres\" :"));
		
		lesXB.put("nom", xbNom);
		lesXB.put("auteur", xbAuteur);
		lesXB.put("statut",xbStatut);
		lesXB.put("ecole",xbEcole);
		
		for (String champ : lesXB.keySet()) {
			if (Arrays.asList(FonctionsUtiles.traiteStringListe(FonctionsUtiles.LireChamp("recherche", "livresColumns"))).contains(champ)) {
				lesXB.get(champ).setSelected(true);
			}
			deuxiemeLigne.add(lesXB.get(champ));
		}
		
		// ligne 3
		troisiemeLigne.setLayout(new FlowLayout());
		troisiemeLigne.add(new JLabel("Colonnes \"élèves\" :"));
		
		lesXBEleves.put("nom", xbNomEleve);
		lesXBEleves.put("prenom", xbPrenom);
		lesXBEleves.put("ecole",xbEcoleEleve);
		lesXBEleves.put("classe",xbClasse);
		
		for (String champ : lesXBEleves.keySet()) {
			if (Arrays.asList(FonctionsUtiles.traiteStringListe(FonctionsUtiles.LireChamp("recherche", "elevesColumns"))).contains(champ)) {
				lesXBEleves.get(champ).setSelected(true);
			}
			troisiemeLigne.add(lesXBEleves.get(champ));
		}
		
		// l4		
		quatriemeLigne.setLayout(new FlowLayout());
		if (Boolean.valueOf(FonctionsUtiles.LireChamp("recherche", "sansAgeOk"))) {
			xbSansAgeOk.setSelected(true);
		} else {
			xbSansAgeOk.setSelected(false);
		}
		quatriemeLigne.add(xbSansAgeOk);
		
		// l5
		bValider.addActionListener(alValider);
		
		cinquiemeLigne.setLayout(new FlowLayout());
		cinquiemeLigne.add(bValider);
		
		this.add(premiereLigne);
		this.add(deuxiemeLigne);
		this.add(troisiemeLigne);
		this.add(quatriemeLigne);
		this.add(cinquiemeLigne);
	}
	
	ActionListener alValider = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			// validation ligne 1
			if (rbLivres.isSelected() && !FonctionsUtiles.LireChamp("recherche", "defautRecherche").equals("livres")) {
				EcrireParametres.Changer("recherche", "defautRecherche", "livres");
			}
			if (rbEleves.isSelected() && !FonctionsUtiles.LireChamp("recherche", "defautRecherche").equals("eleves")) {
				EcrireParametres.Changer("recherche", "defautRecherche", "eleves");
			}
			
			// validation ligne 2
			String valeur ="{id";
			for (String champ: lesXB.keySet()) {
				if (lesXB.get(champ).isSelected()) {
					valeur +=","+champ;
				}
			}
			valeur+="}";
			
			EcrireParametres.Changer("recherche", "livresColumns", valeur);
		
			Recherche.dtmLivres.setColumnIdentifiers(FonctionsUtiles.faisCorrespondre(FonctionsUtiles.traiteStringListe(FonctionsUtiles.LireChamp("recherche", "livresColumns"))));
	
			// validation ligne 3
			valeur ="{id";
			for (String champ: lesXBEleves.keySet()) {
				if (lesXBEleves.get(champ).isSelected()) {
					valeur +=","+champ;
				}
			}
			valeur+="}";
			
			EcrireParametres.Changer("recherche", "elevesColumns", valeur);
			
			Recherche.dtmEleves.setColumnIdentifiers(FonctionsUtiles.faisCorrespondre(FonctionsUtiles.traiteStringListe(FonctionsUtiles.LireChamp("recherche", "elevesColumns"))));
			
			//validation ligne 4
			if (xbSansAgeOk.isSelected() ^ Boolean.valueOf(FonctionsUtiles.LireChamp("recherche", "sansAgeOk"))) {
				EcrireParametres.Changer("recherche", "sansAgeOk", String.valueOf(xbSansAgeOk.isSelected()));
			}
			
			JOptionPane.showMessageDialog(ParametresRecherche.this, "Les modifications, s'il y en a eu, ont été enregistrées.", "Modifications", JOptionPane.INFORMATION_MESSAGE);
		}
	};

}
