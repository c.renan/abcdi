import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


public class ParametresEleves extends JPanel {

	/**
	 * C'est marrant hein ?
	 */
	private static final long serialVersionUID = -5937881974707070494L;

	JPanel premiereLigne = new JPanel();
	JPanel deuxiemeLigne = new JPanel();
	JPanel troisiemeLigne = new JPanel();
	
	JCheckBox xbContinuerParDefaut = new JCheckBox("Lors de l'ajout d'un élève, valider et continuer par défaut");
	JCheckBox xbVoirLesElevesPartis = new JCheckBox("Voir les élèves partis");
	
	JButton bValider = new JButton("Valider");
	
	public ParametresEleves() {
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		
		xbContinuerParDefaut.setSelected(Boolean.valueOf(FonctionsUtiles.LireChamp("eleves", "continuerParDefaut")));
		xbVoirLesElevesPartis.setSelected(Boolean.valueOf(FonctionsUtiles.LireChamp("eleves", "voirLesPartis")));
		
		premiereLigne.setLayout(new FlowLayout());
		premiereLigne.add(xbContinuerParDefaut);
		
		deuxiemeLigne.setLayout(new FlowLayout());
		deuxiemeLigne.add(xbVoirLesElevesPartis);
		
		troisiemeLigne.setLayout(new FlowLayout());
		bValider.addActionListener(alValider);
		troisiemeLigne.add(bValider);
		
		this.add(premiereLigne);
		this.add(deuxiemeLigne);
		this.add(troisiemeLigne);
		
	}
	
	private ActionListener alValider = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			int nombreModif = 0;
			if (xbContinuerParDefaut.isSelected() ^ Boolean.valueOf(FonctionsUtiles.LireChamp("eleves", "continuerParDefaut"))) {
				EcrireParametres.Changer("eleves", "continuerParDefaut", String.valueOf(xbContinuerParDefaut.isSelected()));
				nombreModif+=1;
			}
			if (xbVoirLesElevesPartis.isSelected() ^ Boolean.valueOf(FonctionsUtiles.LireChamp("eleves", "voirLesPartis"))) {
				EcrireParametres.Changer("eleves", "voirLesPartis", String.valueOf(xbVoirLesElevesPartis.isSelected()));
				nombreModif+= 1;
			}
			if (nombreModif>0) {
				JOptionPane.showMessageDialog(ParametresEleves.this, "Les modifications ont été apportées.", "Modifications", JOptionPane.INFORMATION_MESSAGE);
			}
		}
	};
}
