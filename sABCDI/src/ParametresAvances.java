import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class ParametresAvances extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3845301160456060741L;

	JTextField tProxy = new JTextField();
	JTextField tPort = new JTextField();	
	JButton bSetProxy = new JButton("Activer le proxy");
	JButton bRemProxy = new JButton("Oublier le proxy");
	
	JTextField tSite = new JTextField();
	JButton bEnregistrer = new JButton("Enregistrer"); 
	
	JTextField tIPthis = new JTextField();
	JTextField tPortthis = new JTextField();
	JButton bChangePortthis = new JButton("Enregistrer");
	
	public ParametresAvances () {
		if (!FonctionsUtiles.LireChamp("site", "proxy").isEmpty()) {
			tProxy.setText(FonctionsUtiles.quelProxy());
			tPort.setText(FonctionsUtiles.quelPort());
		}
		
		JPanel pProxy = new JPanel();
		pProxy.setLayout(new FlowLayout());
		
		pProxy.add(new JLabel("Proxy : "));
		tProxy.setPreferredSize(new Dimension(200,30));
		pProxy.add(tProxy);
		
		pProxy.add(new JLabel("Port : "));
		tPort.setPreferredSize(new Dimension(50,30));
		pProxy.add(tPort);
		
		bSetProxy.addActionListener(alSetProxy);
		bRemProxy.addActionListener(alRemProxy);
		pProxy.add(bSetProxy);
		pProxy.add(bRemProxy);
		
		
		JPanel pSite = new JPanel();
		pSite.setLayout(new FlowLayout());
		
		pSite.add(new JLabel("URL du serveur ABCDI : "));
		tSite.setPreferredSize(new Dimension(200,30));
		tSite.setText(FonctionsUtiles.LireChamp("site", "url"));
		/*
		 * tSite.addFocusListener(new FocusListener() {
			public void focusLost(FocusEvent e) {
				if (tSite.getText().length()<7) {
					tSite.setText("http://"+tSite.getText());
				}
				else if (!tSite.getText().substring(0, 7).equals("http://")) {
					// ici, j'aurais pu faire une regex...
					tSite.setText("http://"+tSite.getText());
				}
				if (!tSite.getText().matches(".+[/]")) {
					tSite.setText(tSite.getText()+"/");
				}
				// Ça ne sert à rien sauf à me mettre des bâtons dans les roues...
			}
			public void focusGained(FocusEvent e) {
				
			}
		});
		*
		*/
		
		pSite.add(tSite);
		bEnregistrer.addActionListener(alChangerSite);
		
		pSite.add(bEnregistrer);
		
		JPanel pServerthis = new JPanel();
		pServerthis.setLayout(new FlowLayout());
		
		pServerthis.add(new JLabel("IP de ce poste : "));
		tIPthis.setPreferredSize(new Dimension(200,30));
		try {
			// c'est TRÈS compliqué ce qui suit, mais je voulais l'adresse locale moi.
			// et je trouvais pas comment la trouver
			// alors j'ai trouvé un mec qui proposait de toutes les lister.
			// oilà oilà.
			Enumeration<NetworkInterface> monEnum = NetworkInterface.getNetworkInterfaces();
			while (monEnum.hasMoreElements()) {
				NetworkInterface n = (NetworkInterface) monEnum.nextElement();
				Enumeration<InetAddress> deuxEnum = n.getInetAddresses();
				while (deuxEnum.hasMoreElements()) {
					InetAddress i = (InetAddress) deuxEnum.nextElement();
					//System.out.println(i.getHostAddress());
					if (i.getHostAddress().matches("192.+")) {
						tIPthis.setText(i.getHostAddress());
					}
				}
			}
		} catch (SocketException se) {
			se.printStackTrace();
		}

		tIPthis.setEnabled(false);
		pServerthis.add(tIPthis);
		
		pServerthis.add(new JLabel("Port pour les clients : "));
		tPortthis.setPreferredSize(new Dimension(50,30));
		tPortthis.setText(FonctionsUtiles.LireChamp("serveur", "port"));
		pServerthis.add(tPortthis);
		
		bChangePortthis.addActionListener(alChangerPortthis);
		pServerthis.add(bChangePortthis);
		
		/*****/
		
		this.setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
		
		this.add(pProxy);
		
		this.add(pSite);
		
		this.add(pServerthis);
	}
	
	ActionListener alChangerSite = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			EcrireParametres.Changer("site", "url", tSite.getText());
			
			JOptionPane.showMessageDialog(ParametresAvances.this, "Les modifications ont été prises en compte.", "Modifications enregistrées", JOptionPane.INFORMATION_MESSAGE);
		}
	};
	
	ActionListener alSetProxy = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			EcrireParametres.Changer("site", "proxy", tProxy.getText()+":"+tPort.getText());
			
			JOptionPane.showMessageDialog(ParametresAvances.this, "L'utilisation du proxy "+tProxy.getText()+":"+tPort.getText()+" est prise en compte.", "Modifications enregistrées", JOptionPane.INFORMATION_MESSAGE);
		}
	};
	
	ActionListener alRemProxy = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			EcrireParametres.Changer("site", "proxy", "");
			tProxy.setText("");
			tPort.setText("");
			
			JOptionPane.showMessageDialog(ParametresAvances.this, "L'utilisation du proxy a été supprimée.", "Modifications enregistrées", JOptionPane.INFORMATION_MESSAGE);
		}
	};
	
	ActionListener alChangerPortthis = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			EcrireParametres.Changer("serveur", "port", tPortthis.getText());
			
			JOptionPane.showMessageDialog(ParametresAvances.this, "<html>Les modifications ont été prises en compte.<br>Redémarrer le serveur pour que les modifications soient prises en compte.<br>Ne pas oublier d'effectuer les changements dans les paramètres clients.</html>", "Modifications enregistrées", JOptionPane.INFORMATION_MESSAGE);
		}
	};
	
}
