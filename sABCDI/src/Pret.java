import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowSorter;
import javax.swing.RowSorter.SortKey;
import javax.swing.SortOrder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;


public class Pret extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2914221787551765899L;

	RPlaceHolderTextField tEleve = new RPlaceHolderTextField("Élève");
	RPlaceHolderTextField tLivre = new RPlaceHolderTextField("Livre");
	
	JTextField tScan = new JTextField();
	JLabel lScan = new JLabel("Scanner un élève : ");
	
	JButton bOubli = new JButton("Carte oubliée ?");
	JButton bResetEleve = new JButton("⌫");
	
	JPanel premiereLigne = new JPanel();
	
	JPanel scanLigne = new JPanel();
	
	JPanel sessionLigne = new JPanel();
	
	JPanel pTop = new JPanel();
	
	JLabel lSession = new JLabel("Livres empruntés par...");
		
	String[] titreEmprunts = {"Id", "Livre", "Date d'emprunt", "Revenu ?"};
	DefaultTableModel dtmEmprunts = new DefaultTableModel(null, titreEmprunts){
		private static final long serialVersionUID = 1L;
		@SuppressWarnings({ "unchecked", "rawtypes" }) 
		@Override
        public Class getColumnClass(int column) {
            switch (column) {
                case 0:
                    return Integer.class;
                default:
                    return Object.class;
            }
        }
		@Override
		public boolean isCellEditable(int row, int column) {
			return false;
		}
	};
	JTable tableauEmprunts = new JTable(dtmEmprunts);
	
	JScrollPane jspTableau = new JScrollPane(tableauEmprunts);
	
	int idEleve = 0;
	int idLivre = 0;
	
	// pour le sort
	TableRowSorter<DefaultTableModel> trieur = new TableRowSorter<DefaultTableModel>(dtmEmprunts);
	List<RowSorter.SortKey> clesDeTri = new ArrayList<SortKey>(25); // pourquoi 25 ? C'est le nombre de clés de tri possible. Donc avec trois colonnes, je pense qu'on est larges...
	

	
	public Pret() {
		// le tri
		tableauEmprunts.setRowSorter(trieur);
		clesDeTri.add(new RowSorter.SortKey(0, SortOrder.DESCENDING));
			
		trieur.setSortKeys(clesDeTri);

		// le tableau
		tableauEmprunts.getColumnModel().getColumn(0).setMaxWidth(50);
		tableauEmprunts.getColumnModel().getColumn(0).setPreferredWidth(30);
		tableauEmprunts.getColumnModel().getColumn(2).setMaxWidth(120);
		tableauEmprunts.getColumnModel().getColumn(3).setMaxWidth(60);

		tableauEmprunts.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent souris ) {
				JTable jt = (JTable) souris.getSource();
				Point p = souris.getPoint();
				int row = jt.rowAtPoint(p);
				int col = jt.columnAtPoint(p);
				if (souris.getClickCount()==2) {
					Map<String,Object> r = ConnectionSQL.selectionEmpruntParId(Integer.parseInt(jt.getValueAt(row, 0).toString()));
					if (col==1) {
						new DetailLivre(Integer.parseInt(r.get("idlivre").toString()));
					}
				}
			}
		});
		

		
		/*
		List<Map<String,Object>> loEmprunts = ConnectionSQL.selectionEmprunts(0, 0);
		dtmEmprunts.setRowCount(0);
		
		for (int j=0;j<loEmprunts.size();j++) {
			//System.out.println(loEmprunts.get(j).get("idlivre").toString() + " : " + loEmprunts.get(j).get("id").toString());
			try {
				String leLivre = ConnectionSQL.selectionLivreParId(Integer.parseInt(loEmprunts.get(j).get("idlivre").toString())).get("nom").toString();
				Map<String,Object> lEleve = ConnectionSQL.selectionEleveParId(Integer.parseInt(loEmprunts.get(j).get("ideleve").toString()));
				String nomEleve = lEleve.get("nom").toString()+" "+lEleve.get("prenom").toString();
				dtmEmprunts.addRow(new Object[]{loEmprunts.get(j).get("id"),nomEleve,leLivre,loEmprunts.get(j).get("dateemprunt"),loEmprunts.get(j).get("revenu")});
			} catch (NullPointerException npe) {
				JOptionPane.showMessageDialog(Pret.this, "<html>Le livre numéro "+loEmprunts.get(j).get("idlivre".toString())+" n'a pas été trouvé dans la base...<br> Il est conseillé de revenir à une sauvegarde précédente (par exemple current.dbak).</html>", "Absence de livre", JOptionPane.WARNING_MESSAGE);
			}
		}
		*/
		
		// j'ai l'impression que j'aurais pu faire beaucoup mieux avec une jointure interne mais bon.
		// un truc du genre "SELECT e.rowid,l.nom, e.date FROM emprunts AS e INNER JOIN livres AS l WHERE e.ideleve=##IDELEVE AND e.idlivre=l.rowid;"
		// C'est con parce que je sais le faire quoi.
		/*
		 * cf Retour.java
		 * C'est vrai que c'est plus joli.
		 */
		
		jspTableau.setBorder(new JTextField().getBorder());
		jspTableau.setPreferredSize(new Dimension(640,480));
		
		
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		
		// ce componentlistener requests le focus.
		this.addComponentListener(new ComponentListener() {
			public void componentShown(ComponentEvent e) {
				Pret.this.requestFocusInWindow();
				tScan.requestFocus();
				/*
				 List<Map<String,Object>> loEmprunts = ConnectionSQL.selectionEmprunts(0, 0);
				 
				dtmEmprunts.setRowCount(0);
				
				for (int j=0;j<loEmprunts.size();j++) {
					try {
						String leLivre = ConnectionSQL.selectionLivreParId(Integer.parseInt(loEmprunts.get(j).get("idlivre").toString())).get("nom").toString();
						Map<String,Object> lEleve = ConnectionSQL.selectionEleveParId(Integer.parseInt(loEmprunts.get(j).get("ideleve").toString()));
						String nomEleve = lEleve.get("nom").toString()+" "+lEleve.get("prenom").toString();
						dtmEmprunts.addRow(new Object[]{loEmprunts.get(j).get("id"),nomEleve,leLivre,loEmprunts.get(j).get("dateemprunt"),loEmprunts.get(j).get("revenu")});
					}
					catch (NullPointerException npe) {
						JOptionPane.showMessageDialog(Pret.this, "<html>Le livre numéro "+loEmprunts.get(j).get("idlivre".toString())+" n'a pas été trouvé dans la base...<br> Il est conseillé de revenir à une sauvegarde précédente (par exemple current.dbak).</html>", "Absence de livre", JOptionPane.WARNING_MESSAGE);
					}
				}
				*/
			}
			public void componentResized(ComponentEvent e) { }
			public void componentMoved(ComponentEvent e) { }
			public void componentHidden(ComponentEvent e) {
				tLivre.efface();
				tEleve.efface();
				bResetEleve.doClick();
			}
		});
		
		tScan.setPreferredSize(new Dimension(200,30));
		tScan.requestFocusInWindow();
		tScan.addActionListener(alScan);
		
		scanLigne.setLayout(new FlowLayout());
		scanLigne.add(lScan);
		scanLigne.add(tScan);
		
		bOubli.addActionListener(alChoixEleve);
		
		scanLigne.add(bOubli);
		
		premiereLigne.setLayout(new FlowLayout());
	
		tEleve.setPreferredSize(new Dimension(200,30));
		tLivre.setPreferredSize(new Dimension(200,30));
		
		tEleve.setEditable(false);
		tLivre.setEditable(false);
		
		bResetEleve.setToolTipText("Efface l'élève en cours.");
		bResetEleve.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				tEleve.efface();
				lScan.setText("Scanner un élève : ");
				bResetEleve.setEnabled(false);
				dtmEmprunts.setRowCount(0);
				lSession.setText("Livres empruntés par...");
				tScan.requestFocusInWindow();
			}
		});
		bResetEleve.setEnabled(false);
		
		premiereLigne.add(tEleve);
		premiereLigne.add(bResetEleve);
		premiereLigne.add(tLivre);
	
		sessionLigne.setLayout(new FlowLayout());
		sessionLigne.add(lSession);
		
		pTop.setLayout(new BoxLayout(pTop, BoxLayout.PAGE_AXIS));
		
		pTop.add(scanLigne);
		pTop.add(premiereLigne);
		pTop.add(sessionLigne);
		
		this.setLayout(new BorderLayout());
		this.add(pTop,BorderLayout.NORTH);
		
		this.add(jspTableau,BorderLayout.CENTER);

	}
	
	private ActionListener alChoixEleve = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			String recherche=JOptionPane.showInputDialog(Pret.this, "Taper le début du nom ou du prénom de l'élève.");
			if (recherche!=null) {
				List<Map<String,Object>> rs = ConnectionSQL.selectionEleves(recherche);
				if (rs.size()>0) {
					List<String> poss = new ArrayList<String>();
					for (int i=0;i<rs.size();i++) {
						String temp = rs.get(i).get("nom").toString()+" "+rs.get(i).get("prenom").toString()+" ["+rs.get(i).get("id").toString()+"]";
						poss.add(temp);
					}
					Object eleveChoisi = JOptionPane.showInputDialog(Pret.this, "Choisir l'élève dans la liste ci-dessous.","Choix id élève",JOptionPane.PLAIN_MESSAGE,null,poss.toArray(),poss.get(0));
					if (eleveChoisi!=null) {
						String eC = eleveChoisi.toString();
						eC = eC.substring(eC.indexOf("[") + 1);
						eC = eC.substring(0, eC.indexOf("]"));
						
						Pret.this.tScan.setText(FonctionsUtiles.sixChiffrise(eC, true));
						
						/* Ce qui suit sert à simuler une pression sur ENTER */
						Pret.this.tScan.requestFocusInWindow();
						try {
							Robot robot = new Robot();
							robot.keyPress(KeyEvent.VK_ENTER);
							robot.keyRelease(KeyEvent.VK_ENTER);
						} catch (AWTException ae) {
							ae.printStackTrace();}
					}

				}
			}
		}
	};
	
	private ActionListener alScan = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			if (tScan.getText().matches("\\d{6}")) {
				if (tScan.getText().matches("9(\\d)+")) {
					// dans ce cas c'est le code-barres d'un élève
					idEleve = Integer.parseInt(tScan.getText().substring(1));
					Map<String,Object> cetEleve = ConnectionSQL.selectionEleveParId(idEleve);
					// on vérifie que l'élève existe
					if (!cetEleve.get("nom").equals("élève inexistant(e)")) {
						tEleve.changeTexte(cetEleve.get("nom").toString()+" "+cetEleve.get("prenom").toString());
						lScan.setText("Scanner un livre : ");
						lSession.setText("Livres empruntés par "+cetEleve.get("prenom")+" "+cetEleve.get("nom"));
						
						majEmprunts(dtmEmprunts, String.valueOf(idEleve));
					}
					else {
						JOptionPane.showMessageDialog(Pret.this, "Cet élève n'existe pas dans la base de données.", "Élève inexistant", JOptionPane.INFORMATION_MESSAGE);
					}
					bResetEleve.setEnabled(true);
				} else {
					// et là c'est un livre.
					idLivre = Integer.parseInt(tScan.getText());
					Map<String,Object> ceLivre = ConnectionSQL.selectionLivreParId(idLivre);
					if (ceLivre!=null) {
						tLivre.changeTexte(ceLivre.get("nom").toString().substring(0,Math.min(30,ceLivre.get("nom").toString().length())));
						// on vérifie que le livre n'est pas déjà emprunté...
						if (!ceLivre.get("statut").toString().equals("Disponible")) {
							String[] lesOptions = {"Oui","Afficher la fiche du livre","Annuler"};
							int choixLivreEmprunte = JOptionPane.showOptionDialog(Pret.this, "Ce livre n'est pas considéré comme \"Disponible\". Le changer de statut ?", "Livre indisponible ?",
									JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, lesOptions, "Annuler");
							switch (choixLivreEmprunte) {
							case 0:
								Map<String,Object> informations = new HashMap<String,Object>();
								informations.put("statut", "Disponible");
								ConnectionSQL.updatationLivre(idLivre, informations);
								ConnectionSQL.operation("UPDATE emprunts SET revenu='oui' WHERE idlivre="+idLivre+";");
								break;
							case 1:
								tLivre.efface();
								new DetailLivre(idLivre);
								break;
							case 2:
								tLivre.efface();
								break;
							}
						}
						
					}
					else {
						JOptionPane.showMessageDialog(Pret.this, "Pas de livre avec ce code-barre.","Erreur",JOptionPane.ERROR_MESSAGE);
					}
				}
			}
			else if (tScan.getText().matches("\\d{13}")) {
				// dans ce cas on scanne un ISBN
				// il faut utiliser une selection avec une vraie ligne SELECT id,* FROM livres WHERE isbn

				String filtreRequete = "";
				List<Map<String,Object>> tousLesISBN = ConnectionSQL.selection("SELECT rowid AS id, * FROM livres", "livres");
				int i=0;
				for (Map<String,Object> livre : tousLesISBN) {
					if (livre.get("isbn").toString().replaceAll("-", "").equals(tScan.getText())) {
						if (i>0) {
							filtreRequete += " OR ";
						}
						filtreRequete += "id="+livre.get("id");
						i++;
					}
				}
				if (i>1) {
					List<Map<String,Object>> cetISBN = ConnectionSQL.selection("SELECT rowid AS id,* FROM livres WHERE "+filtreRequete, "livres");
					
					Object[] choixLivre = new Object[cetISBN.size()];
					
					int j=0;
					for (Map<String,Object> livre : cetISBN) {
						choixLivre[j] = livre.get("id")+" - "+livre.get("nom")+" - "+livre.get("statut");
						j++;
					}
					
					String res = (String)JOptionPane.showInputDialog(Pret.this, "Choisir l'ouvrage correspondant.", "Plusieurs livres pour cet ISBN", JOptionPane.OK_CANCEL_OPTION, null, choixLivre, choixLivre[0]);
					if (res != null) {
						tLivre.changeTexte(res);
						idLivre = Integer.parseInt(res.split(" - ")[0]);
						if (!res.split(" - ")[2].equals("Disponible")) {
							String[] lesOptions = {"Oui","Afficher la fiche du livre","Annuler"};
							int choixLivreEmprunte = JOptionPane.showOptionDialog(Pret.this, "Ce livre n'est pas considéré comme \"Disponible\". Le changer de statut ?", "Livre indisponible ?",
									JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, lesOptions, "Annuler");
							System.out.println(choixLivreEmprunte);
							switch (choixLivreEmprunte) {
							case 0:
								Map<String,Object> informations = new HashMap<String,Object>();
								informations.put("statut", "Disponible");
								ConnectionSQL.updatationLivre(idLivre, informations);
								ConnectionSQL.operation("UPDATE emprunts SET revenu='oui' WHERE idlivre="+idLivre+";");
								tLivre.changeTexte(res.split(" - ")[1]);
								break;
							case 1:
								tLivre.efface();
								new DetailLivre(idLivre);
								break;
							case 2:
								tLivre.efface();
								break;
							}
						}
					}
				}
				else if (i==1) {
					List<Map<String,Object>> cetISBN = ConnectionSQL.selection("SELECT rowid AS id,* FROM livres WHERE "+filtreRequete, "livres");
					
					tLivre.changeTexte(cetISBN.get(0).get("nom").toString());
					idLivre = Integer.parseInt(cetISBN.get(0).get("id").toString());
				}
				
							
			}
			
			if (((!tEleve.isVide())&&(!tLivre.isVide()))&&(idLivre*idEleve>0)) {
				DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
				Date maintenow = Calendar.getInstance().getTime();
				String laDate = df.format(maintenow);
				ConnectionSQL.operation("INSERT INTO emprunts (idlivre, ideleve, dateemprunt, revenu) VALUES ("+idLivre+", "+idEleve+", '"+laDate+"', 'non');");
				// Oui maintenant que j'ai fait la commande operation, qui marche pour tout sauf pour une sélection, beh je m'emmerde moins.
				Map<String,Object> informations = new HashMap<String,Object>();
				informations.put("statut", "Emprunté");
				ConnectionSQL.updatationLivre(idLivre, informations);
				// mais j'utilise quand même mes vieilles fonctions inutiles et moisies.
				
				tLivre.efface();
				
				majEmprunts(dtmEmprunts, String.valueOf(idEleve));
			}

			tScan.setText("");
			tScan.requestFocus();
			
			//majEmprunts(dtmEmprunts, String.valueOf(idEleve));
			
			// remise à jour du tableau
			/*
			List<Map<String,Object>> loEmprunts = ConnectionSQL.selectionEmprunts(0, 0);
			dtmEmprunts.setRowCount(0);
			
			for (int j=0;j<loEmprunts.size();j++) {
				String leLivre = ConnectionSQL.selectionLivreParId(Integer.parseInt(loEmprunts.get(j).get("idlivre").toString())).get("nom").toString();
				Map<String,Object> lEleve = ConnectionSQL.selectionEleveParId(Integer.parseInt(loEmprunts.get(j).get("ideleve").toString()));
				String nomEleve = lEleve.get("nom").toString()+" "+lEleve.get("prenom").toString();
				dtmEmprunts.addRow(new Object[]{loEmprunts.get(j).get("id"),nomEleve,leLivre,loEmprunts.get(j).get("dateemprunt"),loEmprunts.get(j).get("revenu")});
			}
			*/
			// oui c'est une remise à jour un peu brutale...

		}
	};

	public static void majEmprunts(DefaultTableModel dtm,String idEleve) {
		String[] lesChamps = {"id", "nomLivre", "dateemprunt", "revenu"};

		List<Map<String,Object>> loEmprunts = ConnectionSQL.selectionParChamps(
				"SELECT em.rowid AS id, li.nom AS nomLivre, em.dateemprunt AS dateemprunt, em.revenu AS revenu "+
				"FROM emprunts AS em INNER JOIN eleves AS el INNER JOIN livres AS li "+
				"WHERE em.idEleve=el.rowid AND em.idLivre=li.rowid AND em.idEleve="+idEleve
				+";",lesChamps);
		dtm.setRowCount(0);

		for (int j=0;j<loEmprunts.size();j++) {
			dtm.addRow(new Object[]{loEmprunts.get(j).get("id"), loEmprunts.get(j).get("nomLivre"),loEmprunts.get(j).get("dateemprunt"),loEmprunts.get(j).get("revenu")});
		}
	};
	
	
}
