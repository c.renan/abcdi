import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class ParametresLivres extends JPanel {

	/**
	 * Allez pour rigoler je mets un serialVersionUID de ouf.
	 */
	private static final long serialVersionUID = -918291510638590402L;

	JCheckBox xbAjoutSansValidation = new JCheckBox("[Ajout] Scanner les livres sans valider à chaque fois");
	JCheckBox xbAjoutSansResume = new JCheckBox("[Ajout] Valider l'ajout d'un livre sans nécessiter un résumé");
	JCheckBox xbVoirLesDisparus = new JCheckBox("[Recherche] Voir les livres disparus");
	JCheckBox xbGrouperLivres = new JCheckBox("[Recherche] Grouper les duplicatas");
	JCheckBox xbAgeDefault = new JCheckBox("[Ajout] Utiliser une fourchette d'âge par défaut :");
	JTextField tfAgeMin = new JTextField();
	JTextField tfAgeMax = new JTextField();
	
	/**
	 * Je vire ce qui suit parce que babelio ne fournit pas du tout les mêmes 
	 * informations que decitre.
	 * Du coup je vais plutôt rajouter un bouton « Quel est le résumé de babelio » dans ajouter livre.
	 */
	
	/*
	 * String[] sites = new String[]{"decitre","babelio"};
	 * JComboBox<String> cbSites = new JComboBox<String>(sites);
	 */
	
	
	JPanel zeroiemeLigne = new JPanel();
	JPanel premiereLigne = new JPanel();
	JPanel deuxiemeLigne = new JPanel();
	JPanel troisiemeLigne = new JPanel();
	JPanel quatriemeLigne = new JPanel();
	JPanel cinquiemeLigne = new JPanel();
	JPanel validLigne = new JPanel();
	
	JButton bValider = new JButton("Valider");
	
	public ParametresLivres() {
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		
		// taille des JTextField
		tfAgeMin.setPreferredSize(new Dimension(50,30));
		tfAgeMax.setPreferredSize(new Dimension(50,30));
		
		tfAgeMin.setEnabled(false);
		tfAgeMax.setEnabled(false);
		
		// traitement des boutons
		xbAjoutSansValidation.setSelected(Boolean.valueOf(FonctionsUtiles.LireChamp("livres", "ajoutSansValidation")));
		xbAjoutSansResume.setSelected(Boolean.valueOf(FonctionsUtiles.LireChamp("livres", "ajoutSansResume")));
		xbVoirLesDisparus.setSelected(Boolean.valueOf(FonctionsUtiles.LireChamp("livres", "voirLesDisparus")));
		xbGrouperLivres.setSelected(Boolean.valueOf(FonctionsUtiles.LireChamp("livres", "grouper")));
		xbAgeDefault.setSelected(Boolean.valueOf(FonctionsUtiles.LireChamp("livres", "ageDefault")));
		
		tfAgeMin.setText(FonctionsUtiles.LireChamp("livres", "ageDefaultMin").toString());
		tfAgeMax.setText(FonctionsUtiles.LireChamp("livres", "ageDefaultMax").toString());

		if (xbAgeDefault.isSelected()) {			
			tfAgeMin.setEnabled(true);
			tfAgeMax.setEnabled(true);
		}
		
		xbAgeDefault.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (xbAgeDefault.isSelected()) {
					tfAgeMax.setEnabled(true);
					tfAgeMin.setEnabled(true);
				} else {
					tfAgeMin.setEnabled(false);
					tfAgeMax.setEnabled(false);
				}
			}
		});
		
		bValider.addActionListener(alValidationLivres);
		
		/*
		cbSites.setSelectedItem(FonctionsUtiles.LireChamp("site", "pref"));
		
		zeroiemeLigne.setLayout(new FlowLayout());
		zeroiemeLigne.add(new JLabel("Site préféré pour récupérer les informations des livres :"));
		zeroiemeLigne.add(cbSites);
		*/
		
		premiereLigne.setLayout(new FlowLayout());
		premiereLigne.add(xbAjoutSansValidation);
		
		deuxiemeLigne.setLayout(new FlowLayout());
		deuxiemeLigne.add(xbAjoutSansResume);
		
		troisiemeLigne.setLayout(new FlowLayout());
		troisiemeLigne.add(xbVoirLesDisparus);
		
		quatriemeLigne.setLayout(new FlowLayout());
		quatriemeLigne.add(xbGrouperLivres);
		
		cinquiemeLigne.setLayout(new FlowLayout());
		cinquiemeLigne.add(xbAgeDefault);
		cinquiemeLigne.add(tfAgeMin);
		cinquiemeLigne.add(new JLabel(" - "));
		cinquiemeLigne.add(tfAgeMax);
		
		
		validLigne.setLayout(new FlowLayout());
		validLigne.add(bValider);
		
		this.add(zeroiemeLigne);
		this.add(premiereLigne);
		this.add(cinquiemeLigne);
		this.add(deuxiemeLigne);
		this.add(troisiemeLigne);
		this.add(quatriemeLigne);
		
		
		this.add(validLigne);
	}
	
	private ActionListener alValidationLivres = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			int nombreModifs = 0;
			// je ne change que si c'est différent. Pour ça j'utilise l'opérateur logique ^ qui est en fait XOR.
			if (xbAjoutSansValidation.isSelected() ^ Boolean.valueOf(FonctionsUtiles.LireChamp("livres", "ajoutSansValidation"))) {
				nombreModifs+= 1;
				EcrireParametres.Changer("livres", "ajoutSansValidation", String.valueOf(xbAjoutSansValidation.isSelected()));
			}
			if (xbAjoutSansResume.isSelected() ^ Boolean.valueOf(FonctionsUtiles.LireChamp("livres", "ajoutSansResume"))) {
				nombreModifs+= 1;
				EcrireParametres.Changer("livres", "ajoutSansResume", String.valueOf(xbAjoutSansResume.isSelected()));
			}
			if (xbVoirLesDisparus.isSelected() ^ Boolean.valueOf(FonctionsUtiles.LireChamp("livres", "voirLesDisparus"))) {
				nombreModifs+= 1;
				EcrireParametres.Changer("livres", "voirLesDisparus", String.valueOf(xbVoirLesDisparus.isSelected()));
				
				Recherche.dtmLivres.setRowCount(0);
				// Ça efface comme ça je suis sûr.
			}
			if (xbGrouperLivres.isSelected() ^ Boolean.valueOf(FonctionsUtiles.LireChamp("livres", "grouper"))) {
				nombreModifs+=1;
				EcrireParametres.Changer("livres","grouper",String.valueOf(xbGrouperLivres.isSelected()));
			}
			if (!tfAgeMin.getText().equals(FonctionsUtiles.LireChamp("livres", "ageDefaultMin"))) {
				nombreModifs +=1;
				EcrireParametres.Changer("livres", "ageDefaultMin", tfAgeMin.getText());
			}
			if (!tfAgeMax.getText().equals(FonctionsUtiles.LireChamp("livres", "ageDefaultMax"))) {
				nombreModifs +=1;
				EcrireParametres.Changer("livres", "ageDefaultMax", tfAgeMax.getText());
			}
			if (xbAgeDefault.isSelected() ^ Boolean.valueOf(FonctionsUtiles.LireChamp("livres", "ageDefault"))) {
				nombreModifs += 1;
				EcrireParametres.Changer("livres","ageDefault",String.valueOf(xbAgeDefault.isSelected()));
			}
			/*if (!cbSites.getSelectedItem().toString().equals(FonctionsUtiles.LireChamp("site", "pref"))) {
				nombreModifs+=1;
				EcrireParametres.Changer("site","pref",cbSites.getSelectedItem().toString());
			}*/
			if (nombreModifs==1) {
				JOptionPane.showMessageDialog(ParametresLivres.this, "La modification a été apportée.", "Modification", JOptionPane.INFORMATION_MESSAGE);
			}
			if (nombreModifs>1) {
				JOptionPane.showMessageDialog(ParametresLivres.this, "Les "+nombreModifs+" modifications ont été apportées.", "Modifications", JOptionPane.INFORMATION_MESSAGE);
			}
		}
	};
}
