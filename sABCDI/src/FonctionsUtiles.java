import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class FonctionsUtiles {
	
	/**
	 * Enlève les accents et met tout en minuscules
	 * @param src (string)
	 * @return string
	 */
	public static String vireAccents(String src) {
		if (src!=null) {
			String retour = Normalizer.normalize(src, Normalizer.Form.NFD);
			retour = retour.replaceAll("[^\\p{ASCII}]","").toLowerCase();
			
			return retour;
		}
		return "";
	}
	
	public static String vireZeros(String src) {
		String retour = String.valueOf(Integer.parseInt(src));
		return retour;
	}
	
	public static String sixChiffrise(String src) {
		String retour = src;
		while (retour.length()<6) {
			retour = "0"+retour;
		}
		return retour;
	}
	
	public static String sixChiffrise(String src, boolean eleve) {
		String retour = src;
		while (retour.length()<5) {
			retour = "0"+retour;
		}
		if (retour.length()==5) {
			if (eleve) {
				retour = "9"+retour;
			}
			else {
				retour = "0"+retour;
			}
		}
		return retour;
	}
	
	public static String vireGules(String src) {
		if (src!=null) {
			String retour = Normalizer.normalize(src,Normalizer.Form.NFD);
			retour = retour.replaceAll("[^A-Za-z0-9 -]","").replaceAll(" ","%20");
			
			return retour;
		}
		return "";
	}
	
	public static int nombreDeDans(char carac, String chaine) {
		int i=0;
		
		for (int j=0; j<chaine.length(); j++) {
			if (chaine.charAt(j)==carac) {
				i++;				
			}
		}
		
		return i; 
	}
	
	public static boolean copieFichier(String src, String dst) {
		boolean resultat = false;
		FileInputStream fichierSource = null;
		FileOutputStream fichierDest = null;
		try {
			fichierSource = new FileInputStream(new File(src));
			fichierDest = new FileOutputStream(new File(dst));
			byte buffer[] = new byte[512*1024]; // ... Je pompe je pompe oui mais des panzanis !
			int nblecture; // ?
			while ((nblecture=fichierSource.read(buffer))!=-1) // -1 c'est l'EOF je crois 
			{ 
				fichierDest.write(buffer,0,nblecture);
			}
			resultat=true;
		}
		catch (FileNotFoundException nf) {
			nf.printStackTrace();
		}
		catch (IOException io) { //il a des bonnes idées de noms d'erreurs ce brave codeur de développez.net
			io.printStackTrace();
		}
		finally {
			try {
				fichierSource.close();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			try {
				fichierDest.close();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return resultat;
	}
	
	public static boolean estUneDateCorrecte(String uneDate) {
		if (uneDate.matches("^(0?[1-9]|[12][0-9]|3[01])[/](0?[1-9]|1[012])[/][12]?[0-9]?[0-9][0-9]$")) {
			// j'ai pompé cette regex sur internet et j'ai utilisé regexr.com pour la modifier.
			return true;
		}
		else return false;
	}
	
	/*
	 * En fait ce comparator ne sert à rien mais je le laisse quand même.
	 * Oui... Bon il pourrait servir, il doit bien y avoir un moyen...
	 */
	public static Comparator<Object[]> dateEmpruntComparator = new Comparator<Object[]>() {
		public int compare(Object[] uneDateEmprunt, Object[] uneAutreDateEmprunt) {
			String[] uneDate = uneDateEmprunt[uneDateEmprunt.length-1].toString().split("/");
			String[] autreDate = uneAutreDateEmprunt[uneAutreDateEmprunt.length-1].toString().split("/");
			/*
			 * Ici j'ai supposé que la date était le dernier champ de la liste.
			 * ATTENTION ! Ça ne fonctionne qu'avec les dates qui ont le même nombre de chiffres pour l'année...$
			 */
			if (uneDate[2].length() != autreDate[2].length()) {
				if (uneDate[2].length()==2) {
					uneDate[2] = "20"+uneDate[2]; // ah oui moi faut pas me faire chier, si je sais pas quelle année c'est c'est 20... Genre 2085. Hein.
				}
				else autreDate[2]="20"+autreDate[2];
			}
			/* //EN FAIT C'EST NAZEBROQUE.
			 * Je le laisse pour l'histoire de mon programme.
			if(Integer.parseInt(uneDate[2]) > Integer.parseInt(autreDate[2])) {
				return 1;
			}
			else if (Integer.parseInt(uneDate[2]) < Integer.parseInt(autreDate[2])) {
				return -1;
			}
			else { // ici les années sont égales.
				if (Integer.parseInt(uneDate[1]) > Integer.parseInt(autreDate[1])) {
					return 1;
				}
				else if (Integer.parseInt(uneDate[1]) < Integer.parseInt(autreDate[1])) {
					return -1;
				}
				else { // ici les mois sont égaux
					if (Integer.parseInt(uneDate[0]) > Integer.parseInt(autreDate[0])) {
						return 1;
					}
					else if (Integer.parseInt(uneDate[0]) < Integer.parseInt(autreDate[0])) {
						return -1;
					}
				}
			}
			*/
			// On va faire une stratégie bien meilleure qui est :
			int dateOrdonnee = Integer.parseInt(uneDate[2]+uneDate[1]+uneDate[0]);
			int autreDateOrdonnee = Integer.parseInt(autreDate[2]+autreDate[1]+autreDate[0]);
			
			if (dateOrdonnee>autreDateOrdonnee) {
				return 1;
			} else if (dateOrdonnee< autreDateOrdonnee) {
				return -1;
			}
			// c'est un peu plus rapide non ?
			return 0;
		}
	};
	
	public static List<String> listeDesEcoles() {
		List<String> lesEcoles = new ArrayList<String>();
		
		if (AuChocolat.parametres.get("ecoles").size()>1) {
			
			for (String uneEcole : AuChocolat.parametres.get("ecoles")) {
				if (!uneEcole.matches("defaut=([a-zA-Z0-9 -])+")) {
					lesEcoles.add(uneEcole);
				}
			}
			
			return lesEcoles;	
		}
		else return null;
	};
	
	public static String LireChamp(String categorie, String champ) {
		String sortie = "";
		
		for (String element : AuChocolat.parametres.get(categorie)) {
			if (element.matches(champ+"=.+")) {
				return element.substring(element.indexOf('=')+1);
			}
			
		}
		
		return sortie;
	}
	
	public static String[] traiteStringListe(String src) {
		return src.substring(1, src.length()-1).split(",");
	}
	
	public static String[] faisCorrespondre(String[] src) {
		String[] sortie = new String[src.length];
		for (int i=0;i<src.length;i++) {
			sortie[i] = AuChocolat.correspondances.get(src[i]);
		}
		return sortie;
	}
	
	public static String quelProxy() {
		String adresseComplete = FonctionsUtiles.LireChamp("site", "proxy"); 
		if (!adresseComplete.isEmpty()) {
			return adresseComplete.substring(0, adresseComplete.indexOf(':'));
		}
		return null;
	}
	
	public static String quelPort() {
		String adresseComplete = FonctionsUtiles.LireChamp("site", "proxy"); 
		if (!adresseComplete.isEmpty()) {
			return adresseComplete.substring(adresseComplete.indexOf(':')+1);
		}
		return null;
	}
	
	public static boolean isAgeDansIntervalle(String age, String intervalle) {
		// déjà il faut voir la forme de age.
		// on peut écrire 7
		// on peut écrire [7]
		// on peut écrire [7,8]
		// on peut même écrire [8,7] m'en fous
		int min, max;
		if (!intervalle.matches("\\[\\d+,\\d+\\]")) {
			return false;
		} else {
			min = Integer.valueOf(intervalle.substring(1, intervalle.indexOf(',')));
			max = Integer.valueOf(intervalle.substring(intervalle.indexOf(',')+1,intervalle.length()-1));
			
			if (min>max) {
				int temporarymin = min;
				min = max;
				max = temporarymin;
			}
		}
		if (age.matches("\\d+")) {
			int nombre = Integer.valueOf(age);
			if ((nombre>=min)&&(nombre<=max)) {
				return true;
			} else {
				return false;
			}
		}
		if (age.matches("\\[\\d+\\]")) {
			int nombre = Integer.valueOf(age.substring(1,age.length()-1));
			if ((nombre>=min)&&(nombre<=max)) {
				return true;
			} else {
				return false;
			}
		}
		if (age.matches("\\[\\d+,\\d+\\]")) {
			int minAge = Integer.valueOf(age.substring(1, age.indexOf(',')));
			int maxAge = Integer.valueOf(age.substring(age.indexOf(',')+1,age.length()-1));
			
			if (minAge>maxAge) {
				int temporarymin = minAge;
				minAge = maxAge;
				maxAge = temporarymin;
			}
			if (minAge<min) {
				if (maxAge>=min) {
					return true;
				} else return false;
			} else {
				if (minAge<=max) {
					return true;
				} else return false;
			}
			
		}
		return false;
	}
	
	public static Color colorDewey(String codeDewey) {
		Color c = null;
		int cd = -1;
		if (codeDewey.matches("D[ ]*[\\d]+.*")) {
			cd = Integer.valueOf(codeDewey.replaceAll(" ", "").substring(1, 2));
		} else if (codeDewey.matches("[\\d]+.*")) {
			cd = Integer.valueOf(codeDewey.replaceAll(" ","").substring(0,1));
		}
		
		/*
		 * Pourquoi ce if ?
		 * Parce que si la personne oublie de mettre le D ou même j'ai besoin d'appeler la fonction sans le D
		 * quand je fais l'affichage de la marguerite.
		 */
		
		switch (cd) {
		case -1:
			break;
		case 0:
			c = Color.BLACK;
			break;
		case 1:
			c = new Color(153,52,0); // marron
			break;
		case 2:
			c = new Color(254,0,0); // rouge
			break;
		case 3:
			c = new Color(255,102,0); // orange
			break;
		case 4:
			c = new Color(255,255,0); // jaune
			break;
		case 5:
			c = new Color(0,128,1); // vert
			break;
		case 6:
			c = new Color(0,3,251); // bleu
			break;
		case 7:
			c = new Color(129,0,127); // violet
			break;
		case 8:
			c = new Color(131,131,131); // gris
			break;
		case 9:
			c = Color.WHITE;
			break;
		}
		return c;
	}
	
	public static Color couleurQuiSeVoit(Color base) {
		/*
		 * Les constantes 0.299, 0.587 et 0.114 utilisées dans cette fonction sont tirées de
		 * http://alienryderflex.com/hsp.html
		 * Merci mec.
		 */
		double lumiere = Math.sqrt(0.299*Math.pow(base.getRed(),2) + 0.587*Math.pow(base.getGreen(),2)+ 0.114*Math.pow(base.getBlue(), 2));
		if (lumiere>150) {
			return Color.black;
		} else {
			return Color.white;
		}
	}
	
	public static String codeBarrise(String n,String type) {
		for (int i=n.length()+1;i<6;i++) {
			n = "0"+n;
		}
		if (type.equals("livre")) {
			return "0"+n;
		} else {
			return "9"+n;
		}
		
	}
}
