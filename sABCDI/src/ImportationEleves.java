import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;

public class ImportationEleves extends JFrame {
	/**
	 * 
	 */
	// En vrai je me demande à quoi ça sert ce truc de serialVersionUID. Il faudrait que je regarde.
	private static final long serialVersionUID = 1L;
	
	JLabel lInfos = new JLabel("<html><center>"+"Ce programme prend en charge les fichiers de type csv dont <i>l'ordre des colonnes</i> est le suivant :"+"</center></html>");
	// j'ai mis des balises html pour que le texte aille à la ligne automatiquement. C'est bien fait hein.
	
	String donnees[] = {"Nom*", "Prénom*", "Classe", "École", "Date de naissance"};
	
	JTable tableau = new JTable(new DefaultTableModel(null,donnees));
	
	JLabel lDetails = new JLabel("<html><center>Les champs marqués d'une * sont nécessaires</center></html>");
	JLabel lParcourir = new JLabel("<html><center>Cliquer sur parcourir pour chercher le fichier CSV contenant la liste des élèves.</center></html>");
	JLabel lOK = new JLabel("<html><center>Une fois la liste voulue choisie, cliquer sur Importer.</center></html>");
	JLabel lAnnuler = new JLabel("<html><center>Pour annuler l'importation, cliquer sur annuler.</center></html>");
	/*
	 * En fait je mets <center></center> à chaque fois mais c'est con parce qu'il
	 * y a une commande Java qui le fait...
	 */
	
	JButton bChoixFichier = new JButton("Parcourir");
	JFileChooser choix = new JFileChooser();
	
	JButton bOK = new JButton("Importer");
	JButton bAnnuler = new JButton("Annuler");
	
	DefaultTableModel modeleTable = new DefaultTableModel();
	
	FileFilter ffCSV = new FileNameExtensionFilter("Fichiers listes CSV", "csv");
	
	public ImportationEleves(){		
		/*
		 * Bouton Parcourir
		 */
		bChoixFichier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				choix.addChoosableFileFilter(ffCSV);
				choix.setAcceptAllFileFilterUsed(false);
				int retourChoix = choix.showOpenDialog(ImportationEleves.this);
				String line;
				String[] champs;
				modeleTable = (DefaultTableModel) ImportationEleves.this.tableau.getModel();
				modeleTable.setRowCount(0);
				int indice=0; // cet indice sert à savoir si on a des virgules ou des points virgules.
				if(retourChoix == JFileChooser.APPROVE_OPTION) {
					try {
						File f = new File(choix.getSelectedFile().getAbsolutePath());
						BufferedReader br = new BufferedReader(new FileReader(f));
						
						while ((line = br.readLine()) != null) {
							if (indice == 0) {
								if (FonctionsUtiles.nombreDeDans(';', line) > FonctionsUtiles.nombreDeDans(',', line)) { // oui hein, on ne sait jamais.
									indice=1; // case 1 : points virgules
								}
								else
								{
									indice=2; // case 2 : virgules
								}
							}
							if (indice==1) {
								champs = line.split(";");
							}
							else {
								champs = line.split(",");
							};
							/*
							 * J'ai voulu faire ça avec des case dans un switch mais eclipse prétextait que parfois, champs n'aurait pas été affecté. Salaud.
							 */
							modeleTable.addRow(champs);
						}
						
						br.close();
						
						lInfos.setText("");
					}
					catch (FileNotFoundException err) {
						err.printStackTrace();
					} catch (IOException err) {
						err.printStackTrace();
					}
				}
				else
				{
					// ?
				}				
			}
		});
		
		/*
		 * Bouton Importer
		 */
		bOK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (modeleTable.getRowCount()>0) {
					ConnectionSQL.insertionListeEleves(modeleTable);
					if (modeleTable.getRowCount()==1) {
						JOptionPane.showMessageDialog(ImportationEleves.this, "<html>C'est bon.<br>Cependant, pour n'importer qu'un seul élève, il y a le menu <i>Ajouter un élève</i>.</html>", "Importation réussie", JOptionPane.INFORMATION_MESSAGE);
					}
					else {
						JOptionPane.showMessageDialog(ImportationEleves.this, "Importation des "+modeleTable.getRowCount()+" élèves effectuée.", "Importation réussie", JOptionPane.INFORMATION_MESSAGE);
					}
					ImportationEleves.this.setVisible(false);
				}
				else JOptionPane.showMessageDialog(null, "Il n'y aucun élève dans la liste actuellement.", "Erreur d'importation", JOptionPane.PLAIN_MESSAGE);
			}
		});
		/*
		 * Bouton Annuler
		 */
		bAnnuler.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ImportationEleves.this.setVisible(false);
			}
		});
		
		/*
		 * Préparation du Layout
		 */
		lInfos.setHorizontalAlignment(JLabel.CENTER);
		lDetails.setHorizontalAlignment(JLabel.CENTER);
		lParcourir.setHorizontalAlignment(JLabel.CENTER);
		lOK.setHorizontalAlignment(JLabel.CENTER);
		lAnnuler.setHorizontalAlignment(JLabel.CENTER);
		
		/*
		 * Border Layout avec le tableau et tout
		 */
		JPanel panneauCentral = new JPanel();
		panneauCentral.setLayout(new BorderLayout());
		panneauCentral.add(lInfos, BorderLayout.NORTH);
		panneauCentral.add(new JScrollPane(tableau), BorderLayout.CENTER);
		panneauCentral.add(lDetails,BorderLayout.SOUTH);
		
		/*
		 * Panel avec les boutons
		 */
		JPanel panneauDroite = new JPanel();
		
		panneauDroite.setLayout(new GridBagLayout());
		GridBagConstraints cPD = new GridBagConstraints();
		cPD.fill=GridBagConstraints.HORIZONTAL;
		cPD.weightx=1;
		cPD.weighty=1;
		cPD.gridy=0;
		panneauDroite.add(lParcourir,cPD);
		cPD.gridy=1;
		panneauDroite.add(bChoixFichier,cPD);
		
		cPD.gridy=2;
		panneauDroite.add(lOK,cPD);
		
		cPD.gridy=3;
		panneauDroite.add(bOK,cPD);
		
		cPD.gridy=4;
		panneauDroite.add(lAnnuler,cPD);
		
		cPD.gridy=5;
		panneauDroite.add(bAnnuler,cPD);
		
		
		
		/*
		 * GridBagLayout autour du BorderLayout précédent
		 */
		this.setLayout(new GridBagLayout());
		
		GridBagConstraints c = new GridBagConstraints();
		c.fill=GridBagConstraints.BOTH;
		c.gridx = 0;
		c.weightx=3;
		c.weighty=1;
		c.insets=new Insets(5,5,5,5);
		// En fait l'insets ne fonctionne pas sur le panel contenant le BorderLayout, il semblerait.
		// Mais je le laisse pour la contrainte suivante. 
		
		this.getContentPane().add(panneauCentral, c);
		
		//c.fill=GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.weightx=1;
		
		this.getContentPane().add(panneauDroite, c);

		try{
			Image imageIcone = ImageIO.read(new File("resources/ajoutEleve.png"));
			this.setIconImage(imageIcone);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		this.setTitle("Importer une liste d'élèves");
		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}	
}
