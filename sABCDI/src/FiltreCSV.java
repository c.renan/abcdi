import java.io.File;

import javax.swing.filechooser.FileFilter;


public class FiltreCSV extends FileFilter {
	/**
	 * J'ai dû créer cette classe pour pouvoir faire un filtre
	 * dans le bouton parcourir de la classe ImportationEleves.
	 * Dans le tuto que j'ai trouvé à
	 * http://imss-www.upmf-grenoble.fr/prevert/Prog/Java/swing/JFileChooser.html
	 * il y avait plusieurs filtres et plusieurs choix.
	 * Moi m'en fous, veux que csv.
	 */
	/**
	 * Quand soudain j'ai regardé sur 
	 * https://openclassrooms.com/forum/sujet/jfilechooser-filter-les-fichiers-pouvant-etre-choisis-11771
	 * et j'ai vu que ça prenait une ligne de faire un filtre.
	 * Je me disais aussi...	
	 */
	/**
	 * Nan mais là je suis deg quand même.
	 * Franchement. J'y ai passé que vingt minutes mais mince quoi.
	 * Bref.
	 */
	@Override
	public boolean accept(File f) {
		if (f.isDirectory()) return true;
		String suffixe = null;
		String s=f.getName();
		int i = s.lastIndexOf('.');
		if ((i>0) && i< s.length()-1) {
			suffixe = s.substring(i+1).toLowerCase();
		}
		if (suffixe!=null) {
			return suffixe.equals("csv");
		}
		else return false;
	}

	@Override
	public String getDescription() {
		return "fichiers listes CSV";
	}
	
}
