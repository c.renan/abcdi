import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowSorter;
import javax.swing.RowSorter.SortKey;
import javax.swing.SortOrder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;


public class Retour extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	JTextField tScan = new JTextField();
	
	JLabel lScan = new JLabel("Scanner un livre : ");
	
	JPanel l1 = new JPanel();
	JPanel l2 = new JPanel();
	JPanel l3 = new JPanel();
	
	JPanel pTop = new JPanel();
	
	String[] titreNonRendus = {"Id","Élève", "Livre", "Date d'emprunt"};
	DefaultTableModel dtmNonRendus = new DefaultTableModel(null, titreNonRendus) {
		private static final long serialVersionUID = 1L;
		@SuppressWarnings({ "unchecked", "rawtypes" }) 
		@Override
        public Class getColumnClass(int column) {
            switch (column) {
                case 0:
                    return Integer.class;
                default:
                    return Object.class;
            }
        }
		@Override
		public boolean isCellEditable(int row, int column) {
			return false;
		}
	};
	JTable tNonRendus = new JTable(dtmNonRendus); 
	
	String[] titreNonRendusEleve = {"Id","Livre", "Date d'Emprunt"};
	DefaultTableModel dtmNonRendusEleve = new DefaultTableModel(null, titreNonRendusEleve) {
		private static final long serialVersionUID = 1L;
		@SuppressWarnings({ "unchecked", "rawtypes" }) 
		@Override
        public Class getColumnClass(int column) {
            switch (column) {
                case 0:
                    return Integer.class;
                default:
                    return Object.class;
            }
        }
		@Override
		public boolean isCellEditable(int row, int column) {
			return false;
		}
	};
	JTable tNonRendusEleve = new JTable(dtmNonRendusEleve); 
	
	JScrollPane jspNonRendus = new JScrollPane(tNonRendus);
	
	static JLabel lTitreTableau = new JLabel("Non-rendus");
	JButton bEraseEleve = new JButton("⌫");
	
	// pour le sort
	TableRowSorter<DefaultTableModel> trieur = new TableRowSorter<DefaultTableModel>(dtmNonRendus);
	List<RowSorter.SortKey> clesDeTri = new ArrayList<SortKey>(25);
	
	public Retour() {
		/*
		 * Tableau.
		 */
		tNonRendus.setRowSorter(trieur);
		clesDeTri.add(new RowSorter.SortKey(0, SortOrder.DESCENDING));
			
		trieur.setSortKeys(clesDeTri);

		tNonRendus.getColumnModel().getColumn(0).setMaxWidth(30);
		tNonRendus.getColumnModel().getColumn(3).setMaxWidth(120);

		tNonRendus.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent souris ) {
				JTable jt = (JTable) souris.getSource();
				Point p = souris.getPoint();
				int row = jt.rowAtPoint(p);
				int col = jt.columnAtPoint(p);
				if (souris.getClickCount()==2) {
					Map<String,Object> r = ConnectionSQL.selectionEmpruntParId(Integer.parseInt(jt.getValueAt(row, 0).toString()));
					if (col==1) { // ça c'est si l'élève est sélectionné.
						new DetailEleve(Integer.parseInt(r.get("ideleve").toString()));
					} else if (col==2) {
						new DetailLivre(Integer.parseInt(r.get("idlivre").toString()));
					}
				}
			}
		});
		
		majNonRendus(dtmNonRendus);
		
		tNonRendusEleve.getColumnModel().getColumn(0).setMaxWidth(30);
		tNonRendusEleve.getColumnModel().getColumn(2).setMaxWidth(120);

		tNonRendusEleve.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent souris ) {
				JTable jt = (JTable) souris.getSource();
				Point p = souris.getPoint();
				int row = jt.rowAtPoint(p);
				int col = jt.columnAtPoint(p);
				if (souris.getClickCount()==2) {
					Map<String,Object> r = ConnectionSQL.selectionEmpruntParId(Integer.parseInt(jt.getValueAt(row, 0).toString()));
					if (col==1) {
						new DetailLivre(Integer.parseInt(r.get("idlivre").toString()));
					}
				}
			}
		});
		
		
		jspNonRendus.setBorder(new JTextField().getBorder());
		jspNonRendus.setPreferredSize(new Dimension(640,480));
		
		/*
		 * Layout (un boxlayout vertical contenant moult flowlayouts)
		 */
		l1.setLayout(new FlowLayout());
		l1.add(lScan);
		tScan.setPreferredSize(new Dimension(200,30));
		tScan.addActionListener(alScan);
		l1.add(tScan);
		
		bEraseEleve.setVisible(false);
		bEraseEleve.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				bEraseEleve.setVisible(false);
				
				majNonRendus(dtmNonRendus);
				
				jspNonRendus.setViewportView(tNonRendus);
				tScan.requestFocus();
			}
		});
		l2.setLayout(new FlowLayout());
		l2.add(lTitreTableau);
		l2.add(bEraseEleve);
		
		/*
		 * Le componentlistener qui met le focus dans le tScan
		 */
		this.addComponentListener(new ComponentListener() {
			public void componentShown(ComponentEvent e) {
				Retour.this.requestFocusInWindow();
				tScan.requestFocus();
				// je remets le tableau à jour là aussi.
				
				majNonRendus(dtmNonRendus);
			}
			public void componentResized(ComponentEvent e) { }
			public void componentMoved(ComponentEvent e) { }
			public void componentHidden(ComponentEvent e) {
				bEraseEleve.doClick();
			}
			});


		pTop.setLayout(new BoxLayout(pTop,BoxLayout.PAGE_AXIS));
		pTop.add(l1);
		pTop.add(l2);
		
		this.setLayout(new BorderLayout());
		this.add(pTop,BorderLayout.NORTH);
		this.add(jspNonRendus,BorderLayout.CENTER);
		
	}

	ActionListener alScan = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			if (!tScan.getText().isEmpty()) {
				if (FonctionsUtiles.sixChiffrise(tScan.getText()).matches("\\d{6}")) {
					Map<String,Object> r = ConnectionSQL.selectionLivreParId(Integer.parseInt(tScan.getText()));
					if (r!=null) {
						int idLivre = Integer.parseInt(tScan.getText());
						String[] cesChamps= {"id", "idEleve", "dateemprunt", "nomEleve", "prenomEleve"};
						List<Map<String,Object>> cetEmprunt = ConnectionSQL.selectionParChamps(
								"SELECT em.rowid AS id, em.ideleve AS idEleve, em.dateemprunt AS dateemprunt, el.nom AS nomEleve, el.prenom AS prenomEleve"+
										" FROM emprunts AS em INNER JOIN eleves AS el WHERE em.ideleve = el.rowid AND em.idlivre="+idLivre +" AND em.revenu='non';", cesChamps);

						if (!cetEmprunt.isEmpty()) {
							//System.out.println(cetEmprunt.get(0).get("idEleve").toString()+" "+cetEmprunt.get(0).get("dateemprunt"));
							
							Map<String,Object> informations = new HashMap<String,Object>();
							informations.put("statut", "Disponible");
							ConnectionSQL.updatationLivre(idLivre, informations);
							
							ConnectionSQL.operation("UPDATE emprunts SET revenu='oui' WHERE rowid="+cetEmprunt.get(0).get("id").toString()+";");
							
							bEraseEleve.setVisible(true);
							lTitreTableau.setText("Non-rendus de "+cetEmprunt.get(0).get("prenomEleve")+" "+cetEmprunt.get(0).get("nomEleve"));
							
							majNonRendusEleve(dtmNonRendusEleve,cetEmprunt.get(0).get("idEleve").toString());
														
							jspNonRendus.setViewportView(tNonRendusEleve);
						} 
						else {
							JOptionPane.showMessageDialog(Retour.this, "Ce livre n'était pas emprunté...", "Erreur", JOptionPane.ERROR_MESSAGE);
						}
						
					} else {
						JOptionPane.showMessageDialog(Retour.this, "Le code-barres scanné ne correspond pas à un livre enregistré dans cette base de données.", "Erreur de code-barres", JOptionPane.ERROR_MESSAGE);
					}
				}
				else {
					JOptionPane.showMessageDialog(
							Retour.this,
							"Le code "+tScan.getText()+" ne correspond à aucun livre dans la base de données.",
							"Erreur de code-barres",
							JOptionPane.INFORMATION_MESSAGE);
				}
				tScan.setText("");
			}
		}
	};
	
	public static void majNonRendus(DefaultTableModel dtm) {
		String[] lesChamps = {"id", "nomEleve", "prenomEleve", "nomLivre", "dateemprunt"};
		
		List<Map<String,Object>> loEmprunts = ConnectionSQL.selectionParChamps("SELECT em.rowid AS id, el.nom AS nomEleve, el.prenom AS prenomEleve, li.nom AS nomLivre, " +
				"em.dateemprunt AS dateemprunt FROM emprunts AS em "+
				"INNER JOIN eleves AS el INNER JOIN livres AS li WHERE em.ideleve = el.rowid AND em.idlivre=li.rowid AND em.revenu='non';", lesChamps);
		dtm.setRowCount(0);
		
		for (int j=0;j<loEmprunts.size();j++) {
			String unEleve=loEmprunts.get(j).get("nomEleve").toString()+" "+loEmprunts.get(j).get("prenomEleve");
			dtm.addRow(new Object[]{loEmprunts.get(j).get("id"), unEleve, loEmprunts.get(j).get("nomLivre"),loEmprunts.get(j).get("dateemprunt")});
		}
		
		lTitreTableau.setText("Non-rendus ("+dtm.getRowCount()+")");
	};

	public static void majNonRendusEleve(DefaultTableModel dtm, String idEleve) {
		String[] lesChamps = {"id", "nomLivre", "dateemprunt"};

		List<Map<String,Object>> loEmprunts = ConnectionSQL.selectionParChamps("SELECT em.rowid AS id, li.nom AS nomLivre, em.dateemprunt AS dateemprunt "+
				"FROM emprunts AS em INNER JOIN eleves AS el INNER JOIN livres AS li "+
				"WHERE em.idEleve=el.rowid AND em.idLivre=li.rowid AND em.idEleve="+idEleve+" AND em.revenu='non';",lesChamps);
		dtm.setRowCount(0);

		for (int j=0;j<loEmprunts.size();j++) {
			dtm.addRow(new Object[]{loEmprunts.get(j).get("id"), loEmprunts.get(j).get("nomLivre"),loEmprunts.get(j).get("dateemprunt")});
		}
	};

}
