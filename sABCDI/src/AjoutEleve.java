import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;


public class AjoutEleve extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	RPlaceHolderTextField tfNom = new RPlaceHolderTextField("Nom*"); 
	RPlaceHolderTextField tfPrenom = new RPlaceHolderTextField("Prénom*"); 
	RPlaceHolderTextField tfClasse = new RPlaceHolderTextField("Classe");
	
	JComboBox<String> cbEcole = new JComboBox<String>();
	
	RPlaceHolderTextField tfDateNaiss = new RPlaceHolderTextField("Date de naissance");
	
	List<RPlaceHolderTextField> lesRPHTF = new ArrayList<RPlaceHolderTextField>();
	
	JButton bOkStop = new JButton("Enregistrer");
	JButton bOkGo = new JButton("Enr & Continuer");
	JButton bAnnuler = new JButton("Terminer");
	
	JLabel lReussite = new JLabel();
	
	JOptionPane pReussite = new JOptionPane("Élève ajouté(e).", JOptionPane.INFORMATION_MESSAGE);
	JDialog dReussite = new JDialog();
	
	public AjoutEleve() {
		/*
		 * Ce qui suit sert à quitter la fenêtre avec échap
		 */
		InputMap im=getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
		ActionMap am = getRootPane().getActionMap();
		
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "cancel");
		am.put("cancel", new AbstractAction() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
		/*
		 * Je fais une liste avec les rptf pour faire des actions groupées
		 */
		lesRPHTF.add(tfNom);
		lesRPHTF.add(tfPrenom);
		lesRPHTF.add(tfClasse);
		lesRPHTF.add(tfDateNaiss);
		
		for (RPlaceHolderTextField rphtf : lesRPHTF) {
			rphtf.setPreferredSize(new Dimension(200,30));
			rphtf.addKeyListener(new KeyListener() {
				public void keyTyped(KeyEvent e) { }
				public void keyReleased(KeyEvent e) { }
				
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode()==KeyEvent.VK_ENTER) {
						if (Boolean.valueOf(FonctionsUtiles.LireChamp("eleves", "continuerParDefaut"))) {
							bOkGo.doClick();
						}
						else {
							bOkStop.doClick();
						}
					}
						
				}
			});
		} // tu vois que c'est pratique les collections !
		
		cbEcole.setPreferredSize(new Dimension(200,30));
		// les écoles dans le jcombobox
		for (String uneEcole : FonctionsUtiles.listeDesEcoles()) {
			cbEcole.addItem(uneEcole);
		}
		cbEcole.setSelectedItem(FonctionsUtiles.LireChamp("ecoles", "defaut"));
		
		/*
		 * Et maintenant les ActionListener des boutons
		 */
		bOkStop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (tfNom.isVide() || tfPrenom.isVide()) {
					JOptionPane.showMessageDialog(null, "Entrer au moins un nom et un prénom.", "Pas assez d'informations", JOptionPane.INFORMATION_MESSAGE);
				}
				else {
					Map<String,Object> champs = new HashMap<String,Object>();
					
					champs.put("nom",tfNom.texte());
					champs.put("prenom", tfPrenom.texte());
					champs.put("classe",tfClasse.texte());
					champs.put("ecole",cbEcole.getSelectedItem().toString());
					champs.put("dateNaissance",tfDateNaiss.texte());
					champs.put("statut", "");
					ConnectionSQL.insertionEleve(champs);
					AjoutEleve.this.setVisible(false);
				}
			}
		});

		bOkGo.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				if (tfNom.isVide() || tfPrenom.isVide()) {
					JOptionPane.showMessageDialog(null, "Entrer au moins un nom et un prénom.", "Pas assez d'informations", JOptionPane.INFORMATION_MESSAGE);
				}
				else {
					Map<String,Object> champs = new HashMap<String,Object>();
					
					champs.put("nom",tfNom.texte());
					champs.put("prenom", tfPrenom.texte());
					champs.put("classe",tfClasse.texte());
					champs.put("ecole",cbEcole.getSelectedItem().toString());
					champs.put("dateNaissance",tfDateNaiss.texte());
					champs.put("statut", "");
					ConnectionSQL.insertionEleve(champs);
					 
					// Ce qui suit indique à l'utilisateur que ça a fonctionné...
					// Remarque vu que je ne lève pas d'exception, si jamais ça n'a pas fonctionné, ça lui indique quand même...
					dReussite = pReussite.createDialog(AjoutEleve.this, "Réussite");
					new Thread(new Runnable() // J'ai pris ça sur internet sans chercher.
			        {
			          public void run()
			          {
			            try
			            {
			              Thread.sleep(500);
			              dReussite.dispose();

			            }
			            catch ( Throwable th )
			            {
			              th.getStackTrace();
			            }
			          }
			        }).start();
					dReussite.setVisible(true);
					
					for (RPlaceHolderTextField rphtf : lesRPHTF) {
						rphtf.efface();
					}
					
					tfNom.requestFocus();
					
					
				}
			}
		});
		
		bAnnuler.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AjoutEleve.this.setVisible(false);
			}
		});
		
		/*
		 * Layout
		 */
		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		c.fill=GridBagConstraints.BOTH;
		c.insets = new Insets(5, 5, 5, 5);
		c.weighty=1;
		c.weightx=1;
		c.gridy= 0;
		c.gridx = 0;
		this.add(tfNom,c);
		c.gridx+=1;
		this.add(tfPrenom,c);
		
		c.gridy+=1;
		c.gridx=0;
		this.add(tfClasse,c);
		c.gridx+=1;
		this.add(cbEcole,c);
		
		c.gridy+=1;
		c.gridx = 0;
		this.add(tfDateNaiss,c);
		c.gridx+=1;
		this.add(bOkGo,c);
		
		c.gridy += 1;
		c.gridx = 0;
		this.add(bOkStop,c);
		c.gridx+=1;
		this.add(bAnnuler,c);

		try{
			Image imageIcone = ImageIO.read(new File("resources/ajoutEleve.png"));
			this.setIconImage(imageIcone);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		this.setTitle("Ajouter un élève");
		this.pack();
		this.setLocationRelativeTo(null);
		
		
		this.setVisible(true);
	}
}
