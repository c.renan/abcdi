import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowSorter;
import javax.swing.RowSorter.SortKey;
import javax.swing.SortOrder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;


public class HistoriqueEmpruntsLivre extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3414064866017343846L;

	JButton bFermer = new JButton("Fermer");
	JPanel pTableau = new JPanel();
	JPanel pBouton = new JPanel();
	
	String[] titre = {"Id", "Id Élève", "Élève", "Date d'emprunt", "Revenu ?"};
	DefaultTableModel dtmEmprunts = new DefaultTableModel(null,titre){
		private static final long serialVersionUID = 35383773L;
		@SuppressWarnings({ "unchecked", "rawtypes" })
		@Override
        public Class getColumnClass(int column) {
            switch (column) {
                case 0:
                    return Integer.class;
                default:
                    return Object.class;
            }
        }
		@Override
		public boolean isCellEditable(int row, int column) {
			return false;
		}
	};
	TableRowSorter<DefaultTableModel> trieur = new TableRowSorter<DefaultTableModel>(dtmEmprunts);
	List<RowSorter.SortKey> clesDeTri = new ArrayList<SortKey>(25);
	
	JTable tableEmprunts = new JTable(dtmEmprunts);
	JScrollPane jspEmprunts = new JScrollPane(tableEmprunts);
	
	public HistoriqueEmpruntsLivre(int idLivre) {
		Map<String,Object> infoLivre = ConnectionSQL.selectionLivreParId(idLivre);
		
		tableEmprunts.setRowSorter(trieur);
		clesDeTri.add(new RowSorter.SortKey(0, SortOrder.DESCENDING));
			
		trieur.setSortKeys(clesDeTri);
		
		// listener pour voir l'élève en question
		tableEmprunts.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent souris ) {
				JTable jt = (JTable) souris.getSource();
				Point p = souris.getPoint();
				int row= jt.rowAtPoint(p);
				if (souris.getClickCount()==2) {
					new DetailEleve(Integer.parseInt(jt.getValueAt(row, 1).toString()));
				}
			}
		});
		
		// init table
		tableEmprunts.getColumnModel().getColumn(0).setMaxWidth(30);
		tableEmprunts.getColumnModel().getColumn(1).setMaxWidth(60);
		tableEmprunts.getColumnModel().getColumn(3).setMaxWidth(120);
		tableEmprunts.getColumnModel().getColumn(4).setMaxWidth(60);
		List<Map<String,Object>> loEmprunts = ConnectionSQL.selectionEmprunts(idLivre, 0);
		// le 0 de selectionEmprunts indique que je fais ma recherche sur les livres et non pas sur les élèves.
		dtmEmprunts.setRowCount(0);
		
		for (int j=0;j<loEmprunts.size();j++) {
			String lEleve = ConnectionSQL.selectionEleveParId(Integer.parseInt(loEmprunts.get(j).get("ideleve").toString())).get("nom").toString()
					+" "
					+ConnectionSQL.selectionEleveParId(Integer.parseInt(loEmprunts.get(j).get("ideleve").toString())).get("prenom").toString();
			dtmEmprunts.addRow(new Object[]{Integer.parseInt(loEmprunts.get(j).get("id").toString()),loEmprunts.get(j).get("ideleve"),lEleve,loEmprunts.get(j).get("dateemprunt"),loEmprunts.get(j).get("revenu")});
		}

		jspEmprunts.setBorder(new JTextField().getBorder());
		//jspEmprunts.setPreferredSize(new Dimension(280,250));
		
		String nomDuLivre = infoLivre.get("nom").toString();
		if (nomDuLivre.length()>12) {
			nomDuLivre = nomDuLivre.substring(0,10)+"...";
		}

		bFermer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});

		getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
		
		pTableau.setLayout(new FlowLayout());
		pBouton.setLayout(new FlowLayout());
		
		pTableau.add(jspEmprunts);
		pBouton.add(bFermer);
		
		getContentPane().add(pTableau);
		getContentPane().add(pBouton);

		try{
			Image imageIcone = ImageIO.read(new File("resources/listeEmprunts.png"));
			this.setIconImage(imageIcone);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		this.pack();
		this.setTitle("Historique du livre "+nomDuLivre);
		this.setLocationRelativeTo(null);
		
		this.setVisible(true);
	}
	
}
