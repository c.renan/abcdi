import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;

public class Parametres extends JFrame {

	/**
	 * Ça se corse, ça se corse.
	 */
	private static final long serialVersionUID = 1L;

	JTabbedPane tpParametres = new JTabbedPane();

	JButton bFermer = new JButton("Fermer");
	
	public Parametres(JFrame fenetreParente) {
		/*
		 * Donc il paraît qu'il ne faut pas utiliser de Keylistener en général, mais des keybindings.
		 */
		InputMap im=getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
		ActionMap am = getRootPane().getActionMap();
		
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "cancel");
		am.put("cancel", new AbstractAction() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
		try{
			Image imageIcone = ImageIO.read(new File("resources/parametres.png"));
			this.setIconImage(imageIcone);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		this.setTitle("Paramètres ABCDI");
		this.setSize(720,350);
		this.setLocationRelativeTo(null);
		
		/*
		 * Les onglets
		 */
		
		/*
		 * Gestion du JTabbedPane
		 */
		tpParametres.addTab("Écoles", null, new ParametresEcoles(), "Les paramètres relatifs aux différentes écoles");
		tpParametres.addTab("Recherche", null, new ParametresRecherche(), "Les paramètres relatifs à la recherche dans le logiciel");
		tpParametres.addTab("Livres", null, new ParametresLivres(), "Les paramètres relatifs aux livres");
		tpParametres.addTab("Élèves", null, new ParametresEleves(), "Les paramètres relatifs aux élèves");
		tpParametres.addTab("Impression", null, new ParametresImpression(), "Les paramètres relatifs à l'impression des codes-barres");
		tpParametres.addTab("Paramètres avancés", null, new ParametresAvances(), "Onglet utile au développeur...");
		tpParametres.addTab("Mise à jour", null, new MiseAJour(), "Onglet pour mettre à jour le programme...");
		
		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		c.insets=new Insets(5,5,5,5);
		c.fill=GridBagConstraints.BOTH;
		c.gridx=0;
		c.gridy=0;
		c.weighty=1;
		c.weightx=1;
		
		this.add(tpParametres,c);
		
		bFermer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AuChocolat.parametres = EcrireParametres.Lire();
				Parametres.this.setVisible(false);
			}
		});
		
		c.fill=GridBagConstraints.NONE;
		c.gridy=1;
		c.weighty=0;
		c.weightx=0;
		this.add(bFermer,c);
		
		this.setVisible(true);
		
	}
	
	
}
