import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.BoxLayout;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.KeyStroke;


public class ChoixLivreDeMemeISBN extends JDialog {

	private static final long serialVersionUID = 669292873555986060L;
	/**
	 * 
	 */
	
	JPanel p = new JPanel();
	
	JPanel pl1 = new JPanel();

	JPanel plBoutons = new JPanel();
	
	JLabel lInfo = new JLabel("Plusieurs livres correspondant à cet ISBN ont été trouvés.");
	
	JButton bOk = new JButton("Ok");
	JButton bAnnuler = new JButton("Annuler");

	int resultat;
	String currentISBN;
	
	public ChoixLivreDeMemeISBN(List<Map<String,Object>> cetISBN) {
		currentISBN = cetISBN.get(0).get("isbn").toString().replaceAll("-", "");
		
		InputMap im=getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
		ActionMap am = getRootPane().getActionMap();

		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "cancel");
		am.put("cancel", new AbstractAction() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e) {
				resultat=-1;
				dispose();
			}
		});
		
		bAnnuler.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultat = -1;
				dispose();
			}
		});
		
		bOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultat = 1;
				dispose();
			}
		});
			
	}
	
	public int affiche() {

		pl1.setLayout(new FlowLayout());
		pl1.add(lInfo);
		
		plBoutons.setLayout(new FlowLayout());
		plBoutons.add(bOk);
		plBoutons.add(bAnnuler);
		
		p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));
		
		p.add(pl1);
		p.add(plBoutons);
		
		ChoixLivreDeMemeISBN.this.setTitle("ISBN "+currentISBN);
		ChoixLivreDeMemeISBN.this.setContentPane(p);
		ChoixLivreDeMemeISBN.this.pack();
		ChoixLivreDeMemeISBN.this.setLocationRelativeTo(null);
		ChoixLivreDeMemeISBN.this.setVisible(true);
		
		return resultat;
	}
}
