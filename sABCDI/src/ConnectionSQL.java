import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.swing.table.DefaultTableModel;

public class ConnectionSQL {
	
	public static String nomBDD = "ABCDb.db";
	
	public static List<Map<String,Object>> resultats = new ArrayList<Map<String,Object>>();
	
	public static String[] champsLivres = new String[]{"nom","auteur","dateParution","presentation","dimensions","editeur",
		"nbPages","collection","format","isbn","age", "motsCles","resume","statut","ecole","prix","dewey"}; // je n'ai pas mis état parce que je ne m'en sers pas pour l'instant.

	public static String[] champsEleves = new String[]{"nom","prenom","dateNaissance","ecole","classe","statut"};

	public static String[] champsEmprunts = new String[]{"idlivre", "ideleve", "date"};
	
	public static String[] champsIdLivres = new String[]{"id", "nom","auteur","dateParution","presentation","dimensions","editeur",
		"nbPages","collection","format","isbn","age","motsCles","resume","statut","ecole","prix","dewey"}; // je n'ai pas mis état parce que je ne m'en sers pas pour l'instant.

	public static String[] champsIdEleves = new String[]{"id", "nom","prenom","dateNaissance","ecole","classe","statut"};

	public static String[] champsIdEmprunts = new String[]{"id", "idlivre", "ideleve", "dateemprunt", "revenu"};
	
	/*
	 * Jusqu'ici, mes méthodes 'selection' renvoyaient des List<Object[]>
	 * Mais j'ai décidé, par souci de confort certainement, de passer aux
	 * Maps. Parce que c'est bien plus simple d'utiliser des Maps ici,
	 * je n'aurais pas ce problème de savoir quel indice renvoie à quelle valeur
	 * etc. Vraiment, c'est une bonne idée.
	 * 
	 * (et pis à la place de liste de champs, j'aurais pu utiliser keySet...)
	 */
	public static List<Map<String,Object>> selectionLivres(String filtres, String age) {
		resultats.clear();
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:"+nomBDD);
			c.setAutoCommit(false);
		
			stmt = c.createStatement();
			ResultSet rs;
			if (Boolean.valueOf(FonctionsUtiles.LireChamp("livres", "grouper"))) {
				rs = stmt.executeQuery( "SELECT rowid AS id,* FROM livres GROUP BY nom, auteur;" );
			} else {
				rs = stmt.executeQuery( "SELECT rowid AS id,* FROM livres;" );
			}
			
			while ( rs.next() ) {
				// je dois prendre tous les champs et les concaténer en une magnifique chaîne.
				String resultat = "";
				for (String champ : champsIdLivres) {
					if ((!champ.equals("age"))&&(!champ.equals("isbn"))) {
						try {
							if (champ=="id") {
								resultat += FonctionsUtiles.sixChiffrise(rs.getObject(champ).toString()) + " $^$ ";
							}
							resultat += FonctionsUtiles.vireAccents(rs.getObject(champ).toString())+" $^$ "; // c'est mon caractère spécial pour ne pas mélanger les champs
						} catch (NullPointerException e) {
							e.printStackTrace();
						}
					}
					if (champ.equals("isbn")) {
						try {
							resultat += rs.getObject(champ).toString().replaceAll("-", "");
						} catch (NullPointerException e) {
							e.printStackTrace();
						}
					}
				}
				
				boolean boule = true;
				
				String[] filtresPurs = FonctionsUtiles.vireAccents(filtres).split(" ");
				for (String fifi : filtresPurs) {
					if (!resultat.contains(fifi)) {
						boule=false;
					}
					if (!age.isEmpty()) {
						if (rs.getObject("age").toString().matches("\\[\\d+,\\d+\\]")) {
							if (!FonctionsUtiles.isAgeDansIntervalle(age, rs.getObject("age").toString())) {
								boule=false;
							}
						}
						if (!Boolean.valueOf(FonctionsUtiles.LireChamp("recherche", "sansAgeOk"))) {
							if (rs.getObject("age").toString().isEmpty()) {
								boule=false;
							}
						}
					}
				}
				
				if (boule){
					Map<String,Object> res = new HashMap<String,Object>();
					for (String champ : champsIdLivres) {
						res.put(champ,rs.getObject(champ));
					}
				
					resultats.add(res);
				}
			}
			
			rs.close();
			stmt.close();
			c.close();
			
			return resultats; 
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": "+e.getMessage() );
			System.exit(0);
		}
		return null;
	}
	
	public static List<Map<String, Object>> selectionEleves(String filtres) {
		resultats.clear();
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:"+nomBDD);
			c.setAutoCommit(false);
		
			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT rowid AS id,* FROM eleves;" );
			
			while ( rs.next() ) {
				// je prends tous les champs et les concatène en une magnifique chaîne.
				String resultat = "";
				for (String champ : champsIdEleves) {
					if (champ=="id") {
						resultat += FonctionsUtiles.sixChiffrise(rs.getObject(champ).toString(),true) + " $^$ ";
					} else {
						resultat += FonctionsUtiles.vireAccents(rs.getObject(champ).toString())+" $^$ "; // c'est mon caractère spécial pour ne pas mélanger les champs
					}
				}
				
				boolean boule = true;
				
				String[] filtresPurs = FonctionsUtiles.vireAccents(filtres).split(" ");
				for (String fifi : filtresPurs) {
					if (!resultat.contains(fifi)) {
						boule=false;
					}
				}
				
				if (boule){
					Map<String,Object> res = new HashMap<String,Object>();
					for (String champ : champsIdEleves) {
						res.put(champ,rs.getObject(champ));
					}
				
					resultats.add(res);
				}
			}
			
			rs.close();
			stmt.close();
			c.close();
			
			return resultats; 
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": "+e.getMessage() );
			System.exit(0);
		}
		return null;
	}
	
	public static Map<String,Object> selectionLivreParId(int id) {
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:"+nomBDD);
			c.setAutoCommit(false);
		
			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT rowid AS id,* FROM livres WHERE rowid="+id+";" );
			
			if (rs.next()) {
				Map<String, Object> res = new HashMap<String,Object>();
				for (String champ : champsIdLivres) {
					res.put(champ, rs.getObject(champ));
				}

				rs.close();
				stmt.close();
				c.close();
				
				return res;
			}
			else {
				rs.close();
				stmt.close();
				c.close();
				
				return null;
			}
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": "+e.getMessage() );
			System.exit(0);
		}
		return null;
	}

	public static Map<String,Object> selectionEleveParId(int id) {
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:"+nomBDD);
			c.setAutoCommit(false);
		
			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT rowid AS id,* FROM eleves WHERE rowid="+id+";" );
			
			if (rs.next()) {
				Map<String, Object> res = new HashMap<String,Object>();
				for (String champ : champsIdEleves) {
					res.put(champ,rs.getObject(champ));
				}
				
				rs.close();
				stmt.close();
				c.close();

				return res;
			}
			else {
				rs.close();
				stmt.close();
				c.close();
			}
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": "+e.getMessage() );
			System.exit(0);
		}
		// ici c'est si l'élève sélectionné a été supprimé
		Map<String,Object> eleveSupprime = new HashMap<String,Object>();
		eleveSupprime.put("id",id);
		for (String champ : champsEleves) {
			eleveSupprime.put(champ,"élève inexistant(e)");
		}
		return eleveSupprime;
	}

	public static void insertionLivre(Map<String,Object> tousLesChamps) {
		Connection c = null;
		Statement stmt = null;
		
		try 
		{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:"+nomBDD);
			c.setAutoCommit(false);
			//System.out.println("Opened database successfully.");
			
			stmt = c.createStatement();
	        String sql = "INSERT INTO livres (";
	        for (String champ : champsLivres) {
	        	sql += champ;
	        	if (!champsLivres[champsLivres.length-1].equals(champ)) {
	        		sql += ", ";
	        	}
	        }
	        sql += ") VALUES (";
	        for (String champ : champsLivres) {
	        	sql += "'"+tousLesChamps.get(champ).toString().replaceAll("'", "''")+"'";
	        	if (!champsLivres[champsLivres.length-1].equals(champ)) {
	        		sql += ", ";
	        	}
	        }
	        
	        sql +=");"; 
	        stmt.executeUpdate(sql);
	        stmt.close();
	        c.commit();
	        c.close();
		}
		catch ( Exception e ) {
	         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	         System.exit(0);
		}
		//System.out.println("Records created successfully");
	}
	
	private static String vApo(String src) {
		return "'"+src.replaceAll("'","''")+"'";
	}
	
	private static String vApos(Object src) {
		return vApo(src.toString());
	}
	
	public static void insertionEleve(Map<String,Object> tousLesChamps) {
		Connection c = null;
		Statement stmt = null;
		
		try 
		{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:"+nomBDD);
			c.setAutoCommit(false);
			
			stmt = c.createStatement();
	        String sql = "INSERT INTO eleves (";
	        for (String champ : champsEleves) {
	        	sql += champ;
	        	if (!champsEleves[champsEleves.length-1].equals(champ)) {
	        		sql +=", ";
	        	}
	        }
	        sql +=") VALUES (";
	        for (String champ : champsEleves) {
	        	sql += vApos(tousLesChamps.get(champ));
	        	if (!champsEleves[champsEleves.length-1].equals(champ)) {
	        		sql +=", ";
	        	}
	        }
	        sql+=");";

	        stmt.executeUpdate(sql);
	        stmt.close();
	        c.commit();
	        c.close();
		}
		catch ( Exception e ) {
	         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	         System.exit(0);
		}
	}
	
	public static void insertionListeEleves( DefaultTableModel tableauEleves ) {
		/**
		 * J'essaie.
		 */
		Connection c = null;
		Statement stmt = null;
		
		if (tableauEleves.getRowCount()>0) {	
			try 
			{
				Class.forName("org.sqlite.JDBC");
				c = DriverManager.getConnection("jdbc:sqlite:"+nomBDD);
				c.setAutoCommit(false);
				String lesEleves="";
				for (int i=0;i<tableauEleves.getRowCount();i++) {
					if (i>0) {
						lesEleves += ", ";
					}
					lesEleves += "("+vApos(tableauEleves.getValueAt(i, 0))+","+vApos(tableauEleves.getValueAt(i, 1))+","+
							vApos(tableauEleves.getValueAt(i, 2))+","+vApos(tableauEleves.getValueAt(i,3))+","+
							vApos(tableauEleves.getValueAt(i,4))+", '')";
					}
				
			
					stmt = c.createStatement();
						String sql = "INSERT INTO eleves (nom, prenom, classe, ecole, dateNaissance, statut) " +
								"VALUES "+lesEleves;
						stmt.executeUpdate(sql);
						stmt.close();
						c.commit();
	        c.close();
			}
			catch ( Exception e ) {
				System.err.println( e.getClass().getName() + ": " + e.getMessage() );
				System.exit(0);
			}
		}
	}
	
	public static void updatationLivre(int id, Map<String, Object> informations ) {
		String changements = "";
		int i=0;
		for (String champ : informations.keySet()) {
			if (i!=0) {
				changements += ", ";
			}
			changements += champ+"="+vApo(informations.get(champ).toString());
			i++;
		}
		
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:"+nomBDD);
			c.setAutoCommit(false);
		
			stmt = c.createStatement();
		
			String sql = "UPDATE livres SET "+changements+" WHERE rowid="+id;
			stmt.executeUpdate(sql);
			c.commit();
			
			stmt.close();
			c.close();			
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": "+e.getMessage() );
			System.exit(0);
		}
	}
	
	public static void updatationEleve(int id, Map<String, Object> informations ) {
		String changements = "";
		int i=0;
		for (String champ : informations.keySet()) {
			if (i!=0) {
				changements += ", ";
			}
			changements += champ+"="+vApo(informations.get(champ).toString());
			i++;
		}
		
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:"+nomBDD);
			c.setAutoCommit(false);
		
			stmt = c.createStatement();
		
			String sql = "UPDATE eleves SET "+changements+" WHERE rowid="+id;
			stmt.executeUpdate(sql);
			c.commit();
			
			stmt.close();
			c.close();			
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": "+e.getMessage() );
			System.exit(0);
		}
	}
	
	/*
	 * public static void deletation(String table) {
	 
		Connection c = null;
		Statement stmt = null;
		
		try 
		{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:"+nomBDD);
			c.setAutoCommit(false);

			stmt = c.createStatement();
	        String sql = "DELETE FROM "+table;
	        stmt.executeUpdate(sql);
	        stmt.close();
	        c.commit();
	        c.close();
		}
		catch ( Exception e ) {
	         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	         System.exit(0);
		}
	}
	*/
	public static List<Map<String,Object>> selectionEmprunts(int idLivre, int idEleve) {
		if (idLivre*idEleve!=0) {
			System.out.println("Non, il y a un problème, on veut au plus un seul indice non nul.");
			return null;
		}
		else if ((idLivre==idEleve)&&(idLivre==0)) {
			resultats.clear();
			Connection c = null;
			Statement stmt = null;
			
			try 
			{
				Class.forName("org.sqlite.JDBC");
				c = DriverManager.getConnection("jdbc:sqlite:"+nomBDD);
				c.setAutoCommit(false);

				stmt = c.createStatement();
				String selection = "SELECT rowid AS id,* FROM emprunts ORDER BY id DESC;";
		        ResultSet rs = stmt.executeQuery(selection);
		        
		        while (rs.next()) {
	        		Map<String, Object> res = new LinkedHashMap<String,Object>();
	        		for (String champ : champsIdEmprunts) {
	        			res.put(champ, rs.getObject(champ));
	        		}
	        		resultats.add(res);
		        }
		        
		        rs.close();
		        stmt.close();
		        c.commit();
		        c.close();
		        
		        return resultats;
			}
			catch ( Exception e ) {
		         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		         System.exit(0);
			}
			return null;
		}
		else {
			resultats.clear();
			Connection c = null;
			Statement stmt = null;
			
			try 
			{
				Class.forName("org.sqlite.JDBC");
				c = DriverManager.getConnection("jdbc:sqlite:"+nomBDD);
				c.setAutoCommit(false);

				stmt = c.createStatement();
				String selection = "SELECT rowid AS id,* FROM emprunts WHERE id";
				if (idLivre==0) {
					selection+="eleve="+idEleve+";";
				}
				else {
					selection+="livre="+idLivre+";";
				}
		        ResultSet rs = stmt.executeQuery(selection);
		        
		        while (rs.next()) {
		        	//if (idLivre==0) {
	        		Map<String, Object> res = new HashMap<String,Object>();
	        		for (String champ : champsIdEmprunts) {
	        			res.put(champ, rs.getObject(champ));
	        		}
	        		resultats.add(res);
		        }
		        
		        rs.close();
		        stmt.close();
		        c.commit();
		        c.close();
		        
		        return resultats;
			}
			catch ( Exception e ) {
		         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		         System.exit(0);
			}
			return null;
		}
	}
	
	public static Map<String,Object> selectionEmpruntParId(int id) {
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:"+nomBDD);
			c.setAutoCommit(false);
		
			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT idlivre,ideleve FROM emprunts WHERE rowid="+id+";" );
			
			if (rs.next()) {
				Map<String, Object> res = new HashMap<String,Object>();
				
				res.put("idlivre",rs.getObject("idlivre"));
				res.put("ideleve",rs.getObject("ideleve"));
				
				rs.close();
				stmt.close();
				c.close();

				return res;
			}
			else {
				rs.close();
				stmt.close();
				c.close();
			}
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": "+e.getMessage() );
			System.exit(0);
		}
		return null;
	}
	
	public static void insertionCodeBarre( String nature, int id ) {
		/*
		 * TABLE codesBarres(nature TEXT, idObjetCB INT)
		 */
		Connection c = null;
		Statement stmt = null;
		
		try 
		{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:"+nomBDD);
			c.setAutoCommit(false);
			stmt = c.createStatement();
	        String sql = "INSERT INTO codesBarres (nature, idObjetCB) " +
	        		"VALUES ('"+nature+"', "+id+");";
	        stmt.executeUpdate(sql);
	        stmt.close();
	        c.commit();
	        c.close();
		}
		catch ( Exception e ) {
	         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	         System.exit(0);
		}
	}
	
	public static List<Map<String,Object>> selection(String requete, String categorie) {
		// là, requete bon, c'est la requête. Mais categorie, c'est en gros comment on répond.
		// visiblement ça ne marche qu'avec du SELECT rowid AS id,* FROM...
		resultats.clear();
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:"+nomBDD);
			c.setAutoCommit(false);

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery(requete);

			while ( rs.next() ) {
				Map<String,Object> res = new HashMap<String,Object>();
				if (categorie.equals("livres")) {
					for (String champ : champsIdLivres) {
						res.put(champ,rs.getObject(champ));
					}
				} else if (categorie.equals("eleves")) {
					for (String champ : champsIdEleves) {
						res.put(champ,rs.getObject(champ));
					}	
				} else if (categorie.equals("emprunts")) {
					for (String champ : champsIdEmprunts) {
						res.put(champ,	rs.getObject(champ));
					}
				}
				resultats.add(res);
			}

			rs.close();
			stmt.close();
			c.close();

			return resultats; 
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": "+e.getMessage() );
			System.exit(0);
		}
		return null;
	}
	
	public static List<Map<String,Object>> selectionParChamps(String requete, String[] champs) {
		// là, requete bon, c'est la requête. Mais categorie, c'est en gros comment on répond.
		// visiblement ça ne marche qu'avec du SELECT rowid AS id,* FROM...
		resultats.clear();
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:"+nomBDD);
			c.setAutoCommit(false);

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery(requete);

			while ( rs.next() ) {
				Map<String,Object> res = new HashMap<String,Object>();
				for (String champ : champs) {
					res.put(champ,rs.getObject(champ));
				}
				
				resultats.add(res);
			}

			rs.close();
			stmt.close();
			c.close();

			return resultats; 
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": "+e.getMessage() );
			System.exit(0);
		}
		return null;
	}
	
	public static void operation( String requete ) {
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:"+nomBDD);
			c.setAutoCommit(false);
		
			stmt = c.createStatement();
		
			stmt.executeUpdate(requete);
			c.commit();
			
			stmt.close();
			c.close();			
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": "+e.getMessage() );
			System.exit(0);
		}
	}	
}
