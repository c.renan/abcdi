import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BoxLayout;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.DefaultEditorKit;


public class DetailLivre extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;	
	JLabel lNom = new JLabel("Nom");
	JLabel lAuteur = new JLabel("Auteur");
	JLabel lDateP = new JLabel("Date de parution");
	JLabel lPresentation = new JLabel("Présentation");
	JLabel lDimensions = new JLabel("Dimensions");
	JLabel lEditeur = new JLabel("Éditeur");
	JLabel lNbPages = new JLabel("Nombre de pages");
	JLabel lCollection = new JLabel("Collection");
	JLabel lFormat = new JLabel("Format");
	JLabel lResume = new JLabel("Résumé");
	JLabel lStatut = new JLabel("Statut");
	//JLabel lEtat = new JLabel("État");
	JLabel lEcole = new JLabel("École");
	JLabel lAge = new JLabel("Âge");
	JLabel lISBN = new JLabel("ISBN");
	JLabel lMotsCles = new JLabel("Mots-Clés");
	JLabel lPrix = new JLabel("Prix");
	JLabel lDewey = new JLabel("Cote");
	
	JTextField tNom = new JTextField();
	JTextField tAuteur = new JTextField();
	JTextField tDateP = new JTextField();
	JTextField tPresentation = new JTextField();
	JTextField tDimensions = new JTextField();
	JTextField tEditeur = new JTextField();
	JTextField tNbPages = new JTextField();
	JTextField tCollection = new JTextField();
	JTextField tFormat = new JTextField();
	JTextField tAge = new JTextField();
	JTextField tISBN = new JTextField();
	JTextField tMotsCles = new JTextField();
	JTextField tPrix = new JTextField();
	JTextField tDewey = new JTextField();
	
	JComboBox<String> cbEcole = new JComboBox<String>();
	
	JTextArea tResume = new JTextArea();

	// JComboBox pour le statut du livre
	String[] comboString = {"Disponible", "Emprunté", "Disparu", "Archivé"};
	JComboBox<String> cbStatut = new JComboBox<String>(comboString);
	
	// Un JScrollPane pour la textarea resume
	JScrollPane spResume = new JScrollPane(tResume, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
			
	Map<String, JTextField> lesJTF = new LinkedHashMap<String, JTextField>();
	Map<String, JLabel> lesLabels = new LinkedHashMap<String, JLabel>();
	
	JButton bHistorique = new JButton("Historique des emprunts");
	
	JButton bFermer = new JButton("Fermer");
	JButton bEnregistrer = new JButton("Enregistrer les modifications");
	
	JPopupMenu jpmResume = new JPopupMenu();

	public DetailLivre(final int idLivre) {
		/*
		 * Donc il paraît qu'il ne faut pas utiliser de Keylistener en général, mais des keybindings.
		 */
		InputMap im=getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
		ActionMap am = getRootPane().getActionMap();
		
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "cancel");
		am.put("cancel", new AbstractAction() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
		final Map<String,Object> infoLivre = ConnectionSQL.selectionLivreParId(idLivre);
		
		/*
		 * Avant je faisais des listes de labels et de tf. Maintenant je fais des maps.
		 */
		lesLabels.put("nom",lNom);
		lesLabels.put("auteur",lAuteur);
		lesLabels.put("dateParution",lDateP);
		lesLabels.put("presentation",lPresentation);
		lesLabels.put("dimensions",lDimensions);
		lesLabels.put("editeur",lEditeur);
		lesLabels.put("nbPages",lNbPages);
		lesLabels.put("collection",lCollection);
		lesLabels.put("format",lFormat);
		lesLabels.put("age",lAge);
		lesLabels.put("isbn",lISBN);
		lesLabels.put("motsCles",lMotsCles);
		lesLabels.put("prix",lPrix);
		lesLabels.put("dewey",lDewey);
		// je ne mets ni résumé ni état parce qu'ils ne sont pas associés à un jtextfield
		// je ne mets pas non plus lStatut parce qu'il n'est... oui pour la même raison en fait.
		
		lResume.setHorizontalAlignment(JLabel.RIGHT);
		lStatut.setHorizontalAlignment(JLabel.RIGHT);
		lEcole.setHorizontalAlignment(JLabel.RIGHT);
		
		lesJTF.put("nom",tNom);
		lesJTF.put("auteur",tAuteur);
		lesJTF.put("dateParution",tDateP);
		lesJTF.put("presentation",tPresentation);
		lesJTF.put("dimensions",tDimensions);
		lesJTF.put("editeur",tEditeur);
		lesJTF.put("nbPages",tNbPages);
		lesJTF.put("collection",tCollection);
		lesJTF.put("format",tFormat);
		lesJTF.put("age", tAge);
		lesJTF.put("isbn", tISBN);
		lesJTF.put("motsCles", tMotsCles);
		lesJTF.put("prix",tPrix);
		lesJTF.put("dewey",tDewey);
		//lesJTF.put("ecole",tEcole);
		
		for (String champ : lesLabels.keySet()) {
			lesJTF.get(champ).setText(infoLivre.get(champ).toString());
			lesJTF.get(champ).setPreferredSize(new Dimension(200,30));
			lesLabels.get(champ).setHorizontalAlignment(JLabel.RIGHT);
		}
		
		// mettre la couleur du Dewey dans le JTF
		if (tDewey.getText().length()>=3) {
			if (FonctionsUtiles.colorDewey(tDewey.getText())!=null) {
				tDewey.setBackground(FonctionsUtiles.colorDewey(tDewey.getText()));
				tDewey.setForeground(FonctionsUtiles.couleurQuiSeVoit(tDewey.getBackground()));	
			}
		}
		// un mouseListener pour une nouvelle fenêtre qui indique les couleurs dewey
		tDewey.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent souris ) {
				if (souris.getClickCount()==2) {
					new deweyDetails();
				}
			}
		});
		tDewey.getDocument().addDocumentListener(new DocumentListener() {
			public void removeUpdate(DocumentEvent e) {
				colorize();
			}
			public void insertUpdate(DocumentEvent e) {
				colorize();
			}
			
			public void changedUpdate(DocumentEvent e) {
				colorize();
			}
			
			public void colorize() {
				if ((tDewey.getText().length()>=3)&&(tDewey.getText().matches(".*\\d\\d\\d.*"))) {
					tDewey.setBackground(FonctionsUtiles.colorDewey(tDewey.getText().replaceAll("[^0-9]","").substring(0, 3))); // substring n'est probablement pas nécessaire mais bon.
					// hein
					// on ne sait jamais que voulez-vous
					tDewey.setForeground(FonctionsUtiles.couleurQuiSeVoit(tDewey.getBackground()));
				}
				else {
					tDewey.setBackground(tNom.getBackground());
					tDewey.setForeground(tNom.getForeground());
				}
			}
		});

		cbStatut.setPreferredSize(new Dimension(200,30));
		cbStatut.setSelectedItem(infoLivre.get("statut").toString());
		
		cbEcole.setPreferredSize(new Dimension(200,30));
		for (String uneEcole : FonctionsUtiles.listeDesEcoles()) {
			cbEcole.addItem(uneEcole);
		}
		
		cbEcole.setSelectedItem(infoLivre.get("ecole").toString());
		
    	tResume.setLineWrap(true);
    	tResume.setWrapStyleWord(true);
    	tResume.setText(infoLivre.get("resume").toString());
	
    	// le menu copier coller pour le tResume
    	Action couper = new DefaultEditorKit.CutAction();
    	couper.putValue(Action.NAME, "Couper");
    	couper.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_X, KeyEvent.CTRL_DOWN_MASK));
    	jpmResume.add(couper);
    	
    	Action copier = new DefaultEditorKit.CopyAction();
    	copier.putValue(Action.NAME, "Copier");
    	copier.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.CTRL_DOWN_MASK));
    	jpmResume.add(copier);
    	
    	Action coller = new DefaultEditorKit.PasteAction();
    	coller.putValue(Action.NAME, "Coller");
    	coller.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_V, KeyEvent.CTRL_DOWN_MASK));
    	jpmResume.add(coller);
    	
    	tResume.setComponentPopupMenu(jpmResume);
    	
    	
    	
		spResume.setBorder(new JTextField().getBorder());
		spResume.setPreferredSize(new Dimension(300,150)); // à l'époque où le setPreferredSize était sur le tResume, on ne pouvait pas scroller.
		
		tResume.setCaretPosition(0);
		
		// le bouton historique
		
		bHistorique.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent e) {
				new HistoriqueEmpruntsLivre(Integer.parseInt(infoLivre.get("id").toString()));
			}
		});
		
		/*
		 * GridBagLayout
		 */
		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		//Je me prépare des insets
		Insets iGauche = new Insets(0,10,0,3);
		Insets iDroite = new Insets(0,3,0,10);

		c.fill=GridBagConstraints.HORIZONTAL;
		// initialisation avant la boucle
		c.gridx = 0;
		c.gridy = 0;
		// Boucle pour créer le layout en deux deux
		for (String champ : lesLabels.keySet() ) {
			c.insets=iGauche;
			this.add(lesLabels.get(champ),c);
			c.gridx+=1;
			c.insets=iDroite;
			this.add(lesJTF.get(champ),c);
			c.gridx=(c.gridx+1)%4;
			if (c.gridx==0) {
				c.gridy+=1;
			}
		}
		// le premier changement de gridx n'a pas lieu car il est à la fin de la boucle précédente
		// j'ajoute l'école
		c.insets=iGauche;
		this.add(lEcole,c);
		c.gridx=(c.gridx+1)%4;
		if (c.gridx==0) c.gridy+=1;
		c.insets=iDroite;
		this.add(cbEcole,c);
		
		// j'ajoute le statut
		c.gridx=(c.gridx+1)%4;
		if (c.gridx==0) c.gridy+=1;
		c.insets=iGauche;
		this.add(lStatut,c);
		c.gridx+=1;
		c.insets=iDroite;
		this.add(cbStatut,c);

		// j'ajoute l'historique des emprunts
		c.gridx=(c.gridx+1)%4;
		if (c.gridx==0) c.gridy+=1;
		c.gridwidth=4; // changer cette valeur en cas d'ajout de champ
		c.insets=iDroite;
		this.add(bHistorique,c);
		
		c.gridwidth=1;

		c.gridx=0;
		c.gridy+=1;
		c.insets=iGauche;
		this.add(lResume,c);
		c.insets=iDroite;
		c.gridx+=1;
		c.gridwidth=3;
		c.gridheight=2;
		this.add(spResume,c);
		
		// pour les boutons je fais un nouveau panel que je mettrai sur toute la longueur
		
		JPanel panneauBas = new JPanel();
		bFermer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DetailLivre.this.setVisible(false);
			}
		});
		panneauBas.add(bFermer);

		
		bEnregistrer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (tNom.getText().isEmpty() || tAuteur.getText().isEmpty() ) {
					JOptionPane.showMessageDialog(DetailLivre.this, "<html>Le nom du livre et le nom de l'auteur doivent être renseignés <i>a minima</i>.</html>", "Erreur", JOptionPane.ERROR_MESSAGE);
					if (tNom.getText().isEmpty()) tNom.requestFocus();
					else tAuteur.requestFocus();
				}
				else {
					Map<String, Object> informations = new HashMap<String, Object>();
			
					// je devrais utiliser mon lesJTF puis rajouter les cb
					
					informations.put("nom",tNom.getText());
					informations.put("auteur",tAuteur.getText());
					informations.put("dateParution",tDateP.getText());
					informations.put("presentation", tPresentation.getText());
					informations.put("collection", tCollection.getText());
					informations.put("dimensions",tDimensions.getText());
					informations.put("editeur", tEditeur.getText());
					informations.put("nbPages", tNbPages.getText());
					informations.put("format", tFormat.getText());
					informations.put("statut",cbStatut.getSelectedItem());
					informations.put("ecole", cbEcole.getSelectedItem());
					informations.put("resume",tResume.getText());
					informations.put("motsCles",tMotsCles.getText());
					informations.put("dewey", tDewey.getText());
					informations.put("prix", tPrix.getText());
					informations.put("age",tAge.getText());
					informations.put("isbn", tISBN.getText());

					ConnectionSQL.updatationLivre(idLivre, informations);

					JOptionPane.showMessageDialog(DetailLivre.this, "La fiche "+tNom.getText()+" a bien été enregistrée.", "Fiche enregistrée", JOptionPane.INFORMATION_MESSAGE);
					DetailLivre.this.setVisible(false);
				}
			}
		});
		panneauBas.add(bEnregistrer);
		
		c.gridx=0;
		c.gridy+=2;
		c.gridheight=1;
		c.gridwidth=4;
		this.add(panneauBas,c);
		
		this.pack();
		try{
			Image imageIcone = ImageIO.read(new File("resources/detailLivre.png"));
			this.setIconImage(imageIcone);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		this.setResizable(false);
		
		this.setTitle(infoLivre.get("id").toString()+" -- "+infoLivre.get("nom").toString());
		this.setLocationRelativeTo(null);
		
		this.setVisible(true);
	}
	
	class deweyDetails extends JDialog {

		/**
		 * 
		 */
		private static final long serialVersionUID = -8079227387426132548L;
				
		JPanel pTexte = new JPanel();
		JPanel pTout = new JPanel();
		JPanel pBouton = new JPanel();
		
		JButton bFermer = new JButton("Fermer");
		
		JLabel l0 = new JLabel("000 - Informatique, informations, ouvrages généraux");
		JLabel l1 = new JLabel("100 - Philosophie, parapsychologie, psychologie");
		JLabel l2 = new JLabel("200 - Religion");
		JLabel l3 = new JLabel("300 - Sciences sociales");
		JLabel l4 = new JLabel("400 - Langage");
		JLabel l5 = new JLabel("500 - Sciences pures");
		JLabel l6 = new JLabel("600 - Technologie");
		JLabel l7 = new JLabel("700 - Beaux-arts, arts décoratifs, sports");
		JLabel l8 = new JLabel("800 - Littérature");
		JLabel l9 = new JLabel("900 - Géographie, histoire");
		
		Map<String,JLabel> lesJL = new LinkedHashMap<String,JLabel>();
		
		deweyDetails() {
			lesJL.put("000",l0);
			lesJL.put("100",l1);
			lesJL.put("200",l2);
			lesJL.put("300",l3);
			lesJL.put("400",l4);
			lesJL.put("500",l5);
			lesJL.put("600",l6);
			lesJL.put("700",l7);
			lesJL.put("800",l8);
			lesJL.put("900",l9);
			
			try {
				Image imageIcone = ImageIO.read(new File("resources/detailLivre.png"));
				this.setIconImage(imageIcone);
			} catch (Exception e) {
				e.printStackTrace();
			}
		
			bFermer.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					deweyDetails.this.dispose();
				}
			});
			
			pTout.setLayout(new BoxLayout(pTout,BoxLayout.PAGE_AXIS));
			pTexte.setLayout(new GridBagLayout());
			pBouton.setLayout(new FlowLayout());
			
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.fill=GridBagConstraints.BOTH;
			gbc.gridy=0;
			
			for (String d : lesJL.keySet()) {
				lesJL.get(d).setOpaque(true);
				lesJL.get(d).setBackground(FonctionsUtiles.colorDewey(d));
				lesJL.get(d).setForeground(FonctionsUtiles.couleurQuiSeVoit(FonctionsUtiles.colorDewey(d)));
				lesJL.get(d).setBorder(new EmptyBorder(5,5,5,5));
				
				pTexte.add(lesJL.get(d),gbc);
				gbc.gridy+=1;
			}
			
			pBouton.add(bFermer);
			
			pTout.add(pTexte);
			pTout.add(pBouton);
			
			this.add(pTout);
		
			this.setTitle("Dewey simplifiée");
			
			this.pack();
			
			this.setLocationRelativeTo(DetailLivre.this.tDewey);
			this.setVisible(true);
		}
	
		
	}
	
}
