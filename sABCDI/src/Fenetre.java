import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Desktop;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.JWindow;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.commons.lang.SystemUtils;

public class Fenetre extends JFrame {
	/**
	 * Cette classe Fenetre définit la fenêtre. Je n'ai jamais écrit de doc
	 * avant pardon.
	 */
	private static final long serialVersionUID = 1L;

	// lecture des paramètres.

	FileFilter ffDbak = new FileNameExtensionFilter(
			"Fichiers de sauvegarde de bases de données", "dbak");
	JFileChooser choixRestauration = new JFileChooser("./dbak");

	// déclaration des menus
	private JMenuBar barMenu = new JMenuBar();

	private JMenu mBDD = new JMenu("Base de données");
	private JMenuItem mBDD_Save = new JMenuItem("Sauvegarder");
	private JMenuItem mBDD_Restore = new JMenuItem("Restaurer");
	private JMenuItem mBDD_Purge = new JMenuItem("Effacer toute la base de données");
	private JMenuItem mBDD_Folder = new JMenuItem("Ouvrir le dossier contenant les sauvegardes");

	// TODO s'occuper de ce menu
	/*
	 * private JMenu mLivres = new JMenu("Livres");
	 * private JMenuItem mLivres_Import = new JMenuItem("Importer une liste de livres...");
	 * private JMenuItem mLivres_Export = new JMenuItem("Exporter une liste de livres...");
	 */
	private JMenu mEleves = new JMenu("Élèves");
	private JMenuItem mEleves_Import = new JMenuItem("Importer liste");
	private JMenuItem mEleves_Add = new JMenuItem("Ajouter un élève");
//	private JMenuItem mEleves_Purge = new JMenuItem("Effacer tous les élèves");

	private JMenu mParam = new JMenu("Paramètres");
	private JCheckBoxMenuItem mParam_Splash = new JCheckBoxMenuItem(
			"Splashscreen actif");

	private JMenuItem mParam_Autres = new JMenuItem("Réglages");

	private JMenu mAide = new JMenu("Aide");
	
	private JMenuItem mAide_Aide = new JMenuItem("Aide...");
	private JMenuItem mAide_APropos = new JMenuItem("À propos...");
	/*
	 * Faudra peut-être faire un menu d'aide...
	 */

	// Comme j'ai vu dans le tuto, ils font une fonction initmenu alors je vais
	// faire pareil.
	private void initMenu() {
		// ici je mets l'icône du menu paramètres > réglages
		try{
			Image img = ImageIO.read(new File("resources/parametres.png"));
			Image resizedImage = img.getScaledInstance(20, 20, Image.SCALE_SMOOTH);
			mParam_Autres.setIcon(new ImageIcon(resizedImage));
		} catch (Exception e) {
			e.printStackTrace();
		}		
		
		/* le menu
		 * et ses divers actionlistener
		 */
		mBDD_Save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// je mets la date en forme
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd-HHmmss");
				Date maintenow = Calendar.getInstance().getTime();
				String laDate = df.format(maintenow);
				// ensuite j'écris le fichier
				FonctionsUtiles.copieFichier(ConnectionSQL.nomBDD, "dbak/"+laDate+".dbak");
				// j'indique que la sauvegarde a été faite
				JOptionPane.showMessageDialog(Fenetre.this, "<html>La base de données a été sauvegardée sous le nom "+laDate+".dbak.</html>", "Sauvegarde effectuée",JOptionPane.INFORMATION_MESSAGE);
			}
		});
		mBDD_Save.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_DOWN_MASK));
		mBDD.add(mBDD_Save); 		// OK facile je l'ai déjà faite pour le menu de suppression des élèves
		mBDD_Restore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				choixRestauration.addChoosableFileFilter(ffDbak);
				choixRestauration.setAcceptAllFileFilterUsed(false);
				int retourChoixR = choixRestauration.showOpenDialog(Fenetre.this);
				if (retourChoixR == JFileChooser.APPROVE_OPTION) {
					int cornelien = JOptionPane.showConfirmDialog(Fenetre.this, "Remplacer la base de données actuelle par le fichier "+choixRestauration.getSelectedFile().getName()+" ?",
							"Confirmer remplacement", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
					// j'ai appelé mon int cornelien parce qu'il correspond à un choix. Cornélien. M'emmerdez pas, je vous dis qu'il est tard.
					if (cornelien==0) {
						FonctionsUtiles.copieFichier(choixRestauration.getSelectedFile().getAbsolutePath(), ConnectionSQL.nomBDD);
					}
				}
				else {
					/* rien ne se passe dans ce cas-là. En fait. Du coup,
					 * pourquoi mettre un else ?
					 * PARCE QU'IL EST 2H04 ET QUE MINCE JE FAIS CE QUE JE
					 * VEUX OU PAS ?
					 */
					
				}
			}
		});
		mBDD_Restore.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, KeyEvent.CTRL_DOWN_MASK));
		mBDD.add(mBDD_Restore);		// OK !! Quelle efficacité.
		
		mBDD_Purge.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Object[] options = {"Oui, sans scrupules", "Oui, mais on sauvegarde d'abord", "En fait, non"};
				int retourOuiCestSur = JOptionPane.showOptionDialog(Fenetre.this, "Effacer toute la base de données actuelle ?", "Effacement complet", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, 1);
				switch (retourOuiCestSur) {
				case 0:
					FonctionsUtiles.copieFichier("empty.db", ConnectionSQL.nomBDD);
					JOptionPane.showMessageDialog(Fenetre.this, "<html>La base de données a bien été supprimée sans scrupules.</html>","Effacement effectué", JOptionPane.INFORMATION_MESSAGE);
					break;
				case 1:
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd-HHmmss");
					Date maintenow = Calendar.getInstance().getTime();
					String laDate = df.format(maintenow);
					FonctionsUtiles.copieFichier(ConnectionSQL.nomBDD, "dbak/"+laDate+".dbak");
					// j'indique que la sauvegarde a été faite //NOT
					//	JOptionPane.showMessageDialog(Fenetre.this, "<html>La base de données a été sauvegardée sous le nom "+laDate+".dbak.</html>", "Sauvegarde effectuée",JOptionPane.INFORMATION_MESSAGE);
					FonctionsUtiles.copieFichier("empty.db", ConnectionSQL.nomBDD);
					JOptionPane.showMessageDialog(Fenetre.this, "<html>La base de données a bien été supprimée.<br>Pour la retrouver, restaurer le fichier "+laDate+".dbak."+"</html>","Effacement effectué", JOptionPane.INFORMATION_MESSAGE);
					break;
				case 2:
					// ici on dit annuler
					break;
				default:
					// ici on ne va jamais
					break;	
				}
			}
		});
		mBDD.add(mBDD_Purge);
		
		mBDD_Folder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					if (SystemUtils.IS_OS_WINDOWS) {
						//Runtime.getRuntime().exec("explorer.exe \""+System.getProperty("user.dir")+"/dbak/\"");
						Desktop.getDesktop().open(new File(System.getProperty("user.dir")+"/dbak/"));
					} else {
						URI gagarine = new URI("file://"+System.getProperty("user.dir")+"/dbak/");
						Desktop bureau = Desktop.getDesktop();
						bureau.browse(gagarine);
					}
				} catch (IOException io) {
					io.printStackTrace();
				} catch (URISyntaxException youri) {
					youri.printStackTrace();
				}
				
			}
		});
		mBDD_Folder.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, KeyEvent.CTRL_DOWN_MASK));
		mBDD.add(mBDD_Folder);
		
		mBDD.setMnemonic('B');
		
		mEleves_Import.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				new ImportationEleves();
			}
		});
		mEleves_Import.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, KeyEvent.CTRL_DOWN_MASK));
		mEleves_Add.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				new AjoutEleve();
			}
		});
		mEleves_Add.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, KeyEvent.CTRL_DOWN_MASK));
		
		mEleves.setMnemonic('l');
		
		mEleves.add(mEleves_Import);	
		mEleves.add(mEleves_Add);		
		//	mEleves.add(mEleves_Purge);		
		
		File splashFile = new File("splash");
		if (splashFile.exists()) {
			mParam_Splash.setSelected(true);
		}
		mParam_Splash.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				File splashFile = new File("splash");
				if (mParam_Splash.isSelected()) {
					try {
						splashFile.createNewFile();
					} catch (IOException io) {
						io.printStackTrace();
					}
				}
				else {
					splashFile.delete();
					
				}
			}
		});
		mParam.add(mParam_Splash);
		
		mParam_Autres.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				new Parametres(Fenetre.this);
			}
		});
		mParam_Autres.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, KeyEvent.CTRL_DOWN_MASK));
		mParam.add(mParam_Autres);
		
		mParam.setMnemonic('P');
		
		mAide_Aide.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
				Desktop.getDesktop().open(new File("help/ABCDI_aide.pdf"));
				} catch (IOException io) {
					io.printStackTrace();
				}
			}
		});
		mAide_Aide.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_H, KeyEvent.CTRL_DOWN_MASK));
		mAide.add(mAide_Aide);
		mAide_APropos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new APropos();
			}
		});
		mAide.add(mAide_APropos);
		mAide.setMnemonic('A');
		
		barMenu.add(mBDD);
		barMenu.add(mEleves);
		barMenu.add(mParam);
		barMenu.add(mAide);
		
		
		this.setJMenuBar(barMenu);
	}

	CardLayout cl = new CardLayout();
	JPanel content = new JPanel(); // je copie même les noms je suis sans âme

	private int hauteurFenetre = 700;

	// String[][] listeActivites = {{"1","RECHERCHE"}, {"2","PRET"},
	// {"3","RETOUR"}, {"4","AJOUT_DE_LIVRE"}, {"5","GENERATION_CODES_BARRES"}};
	String[] listeActivites = { "carteR", "carteP", "carteB", "carteA", "carteGCB", "carteT" };
	int indice = 0;

	public Fenetre(JWindow splash) {
		try {
			Image imageIcone = ImageIO.read(new File("resources/icone.png"));
			this.setIconImage(imageIcone);
		} catch (Exception e) {
			e.printStackTrace();
		}

		this.addWindowListener(new WindowListener() {
			public void windowOpened(WindowEvent e) { }
			
			public void windowIconified(WindowEvent e) {}
			
			public void windowDeiconified(WindowEvent e) {}
			
			public void windowDeactivated(WindowEvent e) {}
			
			public void windowClosing(WindowEvent e) {
				FonctionsUtiles.copieFichier(ConnectionSQL.nomBDD, "dbak/current.dbak");	
			}
			
			public void windowClosed(WindowEvent e) {}
			
			public void windowActivated(WindowEvent e) {}
		});
		
		this.setTitle("ABCDI");
		this.setSize(800, hauteurFenetre);
		this.setLocationRelativeTo(null); // milieu d'écran
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // On doit quitter
																// le programme
																// en cliquant
																// sur la croix.

		this.initMenu();

		// on crée cinq conteneurs qui vont porter les différentes activités
		// ces panels s'appellent des cartes. C'est le CardLayout.
		// (oui je m'explique parce que je suis une tanche)

		Recherche carteR = new Recherche();
		Pret carteP = new Pret();
		Retour carteB = new Retour();
		PanneauAjout carteA = new PanneauAjout();
		GCB carteGCB = new GCB();
		Terminal carteT = new Terminal();

		JPanel panBoutons = new JPanel();

		// là je définis mes boutons :
		int hauteurBouton = (int) Math.min(hauteurFenetre / 7, 100);

		JToggleButton bRecherche = new JToggleButton();
		try {
			Image img = ImageIO.read(new File("resources/recherche.png"));
			Image resizedImage = img.getScaledInstance(hauteurBouton,
					hauteurBouton, Image.SCALE_SMOOTH);
			bRecherche.setIcon(new ImageIcon(resizedImage));
		} catch (Exception e) {
			e.printStackTrace();
		}
		bRecherche.setToolTipText("Accéder à la fonction de recherche");
		bRecherche.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				cl.show(content, listeActivites[0]);
			}
		});

		JToggleButton bPret = new JToggleButton();
		try {
			Image img = ImageIO.read(new File("resources/preter.png"));
			Image resizedImage = img.getScaledInstance(hauteurBouton,
					hauteurBouton, Image.SCALE_SMOOTH);
			bPret.setIcon(new ImageIcon(resizedImage));
		} catch (Exception e) {
			e.printStackTrace();
		}
		bPret.setToolTipText("Accéder au prêt des livres");
		bPret.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				cl.show(content, listeActivites[1]);
			}
		});

		JToggleButton bRetour = new JToggleButton();
		try {
			Image img = ImageIO.read(new File("resources/retour.png"));
			Image resizedImage = img.getScaledInstance(hauteurBouton,
					hauteurBouton, Image.SCALE_SMOOTH);
			bRetour.setIcon(new ImageIcon(resizedImage));
		} catch (Exception e) {
			e.printStackTrace();
		}
		bRetour.setToolTipText("Accéder au retour des livres");
		bRetour.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				cl.show(content, listeActivites[2]);
			}
		});

		JToggleButton bAjout = new JToggleButton();
		try {
			Image img = ImageIO.read(new File("resources/ajouter.png"));
			Image resizedImage = img.getScaledInstance(hauteurBouton,
					hauteurBouton, Image.SCALE_SMOOTH);
			bAjout.setIcon(new ImageIcon(resizedImage));
		} catch (Exception e) {
			e.printStackTrace();
		}
		bAjout.setToolTipText("Accéder à la fonction d'ajout de livres");
		bAjout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				cl.show(content, listeActivites[3]);
			}
		});

		JToggleButton bGCB = new JToggleButton();
		try {
			Image img = ImageIO.read(new File("resources/gcb.png"));
			Image resizedImage = img.getScaledInstance(hauteurBouton,
					hauteurBouton, Image.SCALE_SMOOTH);
			bGCB.setIcon(new ImageIcon(resizedImage));
		} catch (Exception e) {
			e.printStackTrace();
		}
		bGCB.setToolTipText("Accéder à la fonction de génération de codes-barres");
		bGCB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				cl.show(content, listeActivites[4]);
			}
		});
		
		JToggleButton bTerminal = new JToggleButton();
		try {
			Image img = ImageIO.read(new File("resources/terminal.png"));
			Image resizedImage = img.getScaledInstance(hauteurBouton,
					hauteurBouton, Image.SCALE_SMOOTH);
			bTerminal.setIcon(new ImageIcon(resizedImage));
		} catch (Exception e) {
			e.printStackTrace();
		}
		bTerminal.setToolTipText("Accéder au journal des démarches clientes");
		bTerminal.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				cl.show(content, listeActivites[5]);
			}
		});
		
		// je réunis tous mes boutons en un bouton group
		ButtonGroup bgGrosMenu = new ButtonGroup();
		bgGrosMenu.add(bRecherche);
		bgGrosMenu.add(bPret);
		bgGrosMenu.add(bRetour);
		bgGrosMenu.add(bAjout);
		bgGrosMenu.add(bGCB);
		bgGrosMenu.add(bTerminal);

		// panBoutons.setLayout(new BoxLayout(panBoutons,BoxLayout.PAGE_AXIS));
		// // ça marche pas pour le resizing
		GridLayout gl = new GridLayout(6, 1);
		gl.setVgap(1);
		panBoutons.setLayout(gl);

		panBoutons.add(bRecherche);
		panBoutons.add(bPret);
		panBoutons.add(bRetour);
		panBoutons.add(bAjout);
		panBoutons.add(bGCB);
		panBoutons.add(bTerminal);

		content.setLayout(cl);
		// on ajoute les cartes à la pile avec un nom pour les retrouver :
		content.add(carteR, listeActivites[0]);
		content.add(carteP, listeActivites[1]);
		content.add(carteB, listeActivites[2]);
		content.add(carteA, listeActivites[3]);
		content.add(carteGCB, listeActivites[4]);
		content.add(carteT, listeActivites[5]);

		this.getContentPane().add(panBoutons, BorderLayout.WEST);
		this.getContentPane().add(content, BorderLayout.CENTER);

		splash.dispose();
		this.setVisible(true);
	}

}
