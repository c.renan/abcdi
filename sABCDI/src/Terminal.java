import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

public class Terminal extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2243594098047546574L;

	static String[] colonnesTerminal = {"date","type","requête"};
	
	public static DefaultTableModel dtmLog = new DefaultTableModel(null, colonnesTerminal) {

		/**
		 * 
		 */
		private static final long serialVersionUID = -7417263159769782701L;
		@SuppressWarnings({"unchecked","rawtypes"})
		@Override
		public Class getColumnClass(int column) {
			return String.class;
		}
		@Override
		public boolean isCellEditable(int row, int column) {
			return false;
		}
	};
	
	public static JTable tLog = new JTable(dtmLog) {
		/**
		 * 
		 */
		private static final long serialVersionUID = 2974274384223546905L;

		@Override
		public Component prepareRenderer(TableCellRenderer renderer, int rowIndex, int columnIndex) {
            JComponent component = (JComponent) super.prepareRenderer(renderer, rowIndex, columnIndex);  

            component.setBackground(new Color(45, 52, 54));
            
            if (getValueAt(rowIndex, 1).toString().equalsIgnoreCase("emprunt")) {
            	component.setForeground(new Color(116, 185, 255));
            }
            else if (getValueAt(rowIndex, 1).toString().equalsIgnoreCase("retour")){
                component.setForeground(new Color(253, 121, 168));
            }
            else if (getValueAt(rowIndex, 1).toString().equalsIgnoreCase("scan")){
            	component.setForeground(new Color(255, 234, 167));
            }
            else if (getValueAt(rowIndex, 1).toString().equalsIgnoreCase("recherche")){
            	component.setForeground(Color.WHITE);
            }
            else {
            	component.setForeground(new Color(85, 239, 196));
            }
            return component;
        }
	};
	
	
	JScrollPane spLog = new JScrollPane(tLog);
	
	public Terminal() {
		tLog.setGridColor(Color.BLACK);
		
		tLog.getColumnModel().getColumn(0).setMaxWidth(150);
		tLog.getColumnModel().getColumn(0).setPreferredWidth(130);
		tLog.getColumnModel().getColumn(1).setMaxWidth(100);
		tLog.getColumnModel().getColumn(1).setPreferredWidth(90);
		
		/*
		 * Mouse listener...
		 */
		tLog.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent souris ) {
				JTable jt = (JTable) souris.getSource();
				Point p = souris.getPoint();
				int row = jt.rowAtPoint(p);
				int col = jt.columnAtPoint(p);
				
				// demander si c'est bouton gauche ou bouton droit
				/*
				 * Si c'est bouton gauche, on complète la ligne par le nom
				 * Si c'est bouton droit, on affiche les détails.
				 */
				
					if ((col>1)&&(jt.getValueAt(row, col-1).toString().equals("Connexion"))) {
						try {
							int idEleve = Integer.parseInt(jt.getValueAt(row, col).toString().substring(1,6));
							if (SwingUtilities.isLeftMouseButton(souris)) {
								Map<String,Object> r = ConnectionSQL.selectionEleveParId(idEleve);
								jt.setValueAt(FonctionsUtiles.sixChiffrise(r.get("id").toString(), true)+" "+r.get("prenom")+" "+r.get("nom"), row, col);
							} else if (SwingUtilities.isRightMouseButton(souris)) {
								new DetailEleve(idEleve);
							}
						} catch (Exception e) {
							JOptionPane.showMessageDialog(Terminal.this, "Erreur : "+e.getMessage());
						}
					}
					else if ((col>1)&&(jt.getValueAt(row, col-1).toString().equals("Scan") || jt.getValueAt(row, col-1).toString().equals("Retour"))) {
						try {
							int idLivre = Integer.parseInt(jt.getValueAt(row, col).toString().substring(0,6));
							if (SwingUtilities.isLeftMouseButton(souris)) {
								Map<String,Object> r = ConnectionSQL.selectionLivreParId(idLivre);

								jt.setValueAt(FonctionsUtiles.sixChiffrise(r.get("id").toString())+" "+r.get("nom"), row, col);
							} else if (SwingUtilities.isRightMouseButton(souris)) {
								new DetailLivre(idLivre);
							}
						} catch (Exception e) {
							JOptionPane.showMessageDialog(Terminal.this, "Erreur : "+e.getMessage());
						}
					}
					else if ((col>1)&&(jt.getValueAt(row, col-1).toString().equals("Emprunt"))) {
						try {
							int idEleve = Integer.parseInt(jt.getValueAt(row, col).toString().substring(1,6));
							int idLivre = Integer.parseInt(jt.getValueAt(row, col).toString().substring(7,13));
							if (SwingUtilities.isLeftMouseButton(souris)) {
								Map<String,Object> r = ConnectionSQL.selectionEleveParId(idEleve);
								Map<String,Object> s = ConnectionSQL.selectionLivreParId(idLivre);

								jt.setValueAt(FonctionsUtiles.sixChiffrise(r.get("id").toString(),true)+"/"+FonctionsUtiles.sixChiffrise(s.get("id").toString())+" "+r.get("nom")+" "+r.get("prenom")+"/"+s.get("nom"), row, col);
							} else if (SwingUtilities.isRightMouseButton(souris)) {
								new DetailLivre(idLivre);
								new DetailEleve(idEleve);
							}
						} catch (Exception e) {
							JOptionPane.showMessageDialog(Terminal.this, "Erreur : "+e.getMessage());
						}
						
					}

					
					/*
					if (col==1) { // ça c'est si l'élève est sélectionné.
						new DetailEleve(Integer.parseInt(r.get("ideleve").toString()));
					} else if (col==2) {
						new DetailLivre(Integer.parseInt(r.get("idlivre").toString()));
					}
					*/
				
			}
		});
		
		
		try {
			BufferedReader br = new BufferedReader(new FileReader("clients.log"));
			String ligne;
			while ((ligne=br.readLine())!=null) {
				//appendTPLog(ligne+"\n");
				insertTPLog(0, ligne+"\n");
			}
			br.close();
		}
		catch (FileNotFoundException fnf) {
			fnf.printStackTrace();
		}
		catch (IOException ioe) {
			ioe.printStackTrace();
		}
		
		this.setLayout(new BorderLayout());
		this.add(spLog, BorderLayout.CENTER);
	}
	
	static public String[] prepareTPLog(String requete) {
		if (!requete.split(" ")[2].substring(0,2).equals("V=")) {
			String type="";
			String actualRequest = requete.substring(22);
			actualRequest= actualRequest.substring(0,actualRequest.length()-1); // le dernier caractère est un CR ou CR LF enfin un truc chiant. \n en fait. 
			if (requete.split(" ")[2].substring(0,2).equals("R=")) {
				type="Recherche";
			}
			else if (requete.split(" ")[2].substring(0,2).equals("S=")) {
				type="Scan";
			}		
			else if (requete.split(" ")[2].substring(0,2).equals("E=")) {
				type="Emprunt";
				actualRequest = requete.substring(22).split(",L=")[0]+"/"+requete.substring(22).split(",L=")[1];
			}		
			else if (requete.split(" ")[2].substring(0,2).equals("Z=")) {
				type="Retour";
			}
			else if (requete.split(" ")[2].substring(0,2).equals("C=")) {
				type="Connexion";
			}
			String date = requete.substring(0,19);

			String[] laListe = {date,type,actualRequest};

			return laListe;
		}
		else {
			return null;
		}
	}
	
	static public void appendTPLog(String requete) {
		String[] laListe = prepareTPLog(requete);
		if (laListe != null) {
			dtmLog.addRow(laListe);
		}
		// TODO des infobulles et/ou un JOptionPane en cas de double clic ?
		
	}
	
	static public void insertTPLog(int row, String requete) {
		String[] laListe = prepareTPLog(requete);
		if (laListe != null ) {
			dtmLog.insertRow(row, laListe);
		}
		// TODO des infobulles et/ou un JOptionPane en cas de double clic ?
		
	}
	
}
