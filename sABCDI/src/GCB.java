import java.awt.Desktop;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.ProgressMonitor;
import javax.swing.SwingWorker;

import org.apache.commons.lang.SystemUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.util.Matrix;

import net.sourceforge.barbecue.Barcode;
import net.sourceforge.barbecue.BarcodeException;
import net.sourceforge.barbecue.BarcodeFactory;
import net.sourceforge.barbecue.BarcodeImageHandler;
import net.sourceforge.barbecue.output.OutputException;

public class GCB extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Bouton Générer :
	JButton boutonGo = new JButton("Générer codes-barres");

	JButton boutonDossier = new JButton();
	JProgressBar jpbCreation = new JProgressBar();

	Recherche pan = new Recherche();

	Map<String, Object> aCoder = new HashMap<String, Object>();

	List<Map<String, String>> postafiok = new ArrayList<Map<String,String>>();
	
	public GCB() {
		/*
		 * Gestion des boutons
		 */
		boutonGo.addActionListener(this);

		try {
			Image img = ImageIO.read(new File("resources/dossierCB.png"));
			Image resizedImage = img.getScaledInstance(30, 30,
					Image.SCALE_SMOOTH);
			boutonDossier.setIcon(new ImageIcon(resizedImage));
		} catch (Exception e) {
			e.printStackTrace();
		}
		boutonDossier
				.setToolTipText("Accéder au dossier contenant les codes-barres déjà édités");
		boutonDossier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				try {
					if (SystemUtils.IS_OS_WINDOWS) {
						Desktop.getDesktop().open(
								new File(System.getProperty("user.dir")
										+ "/codesBarres/"));
					} else {
						URI gagarine = new URI("file://"
								+ System.getProperty("user.dir")
								+ "/codesBarres/");
						Desktop bureau = Desktop.getDesktop();
						bureau.browse(gagarine);
					}
				} catch (IOException io) {
					io.printStackTrace();
				} catch (URISyntaxException youri) {
					youri.printStackTrace();
				}
			}
		});

		this.setLayout(new GridBagLayout());
		GridBagConstraints cTR = new GridBagConstraints();
		cTR.gridx = 0;
		cTR.gridwidth = 2;
		cTR.gridy = 0;
		cTR.fill = GridBagConstraints.BOTH;
		cTR.weightx = 1;
		cTR.weighty = 1;
		this.add(pan, cTR);

		GridBagConstraints cBG = new GridBagConstraints();
		cBG.fill = GridBagConstraints.NONE;
		cBG.gridx = 0;
		cBG.gridy = 1;
		cBG.weightx = 1;
		this.add(boutonGo, cBG);

		cBG.weightx = 0;
		cBG.gridx += 1;
		this.add(boutonDossier, cBG);
	}

	public void actionPerformed(ActionEvent e) {

		SwingWorker<Void,Void> worker = new SwingWorker<Void,Void>() {
			@Override
			protected Void doInBackground() throws Exception {
				doTheJob();
				return null;
			}
		};

		worker.execute();
	}




	void doTheJob() {
		boolean boule;
		
		postafiok.clear();
		
		float progres = 0f;
		ProgressMonitor pm = new ProgressMonitor(GCB.this, "Édition des codes-barres", "", 0, 300);
		pm.setMillisToDecideToPopup(0);
		
		if (pan.rbLivres.isSelected()) {
			if (pan.tableauLivres.getSelectedRowCount() > 0) {
				if (Boolean.valueOf(FonctionsUtiles.LireChamp("livres",
						"grouper"))) {
					boule = false;
					JOptionPane.showMessageDialog(GCB.this,
							"<html>Les livres sont regroupés.<br>Il faut les dégrouper (dans les paramètres) pour pouvoir éditer les code-barres.</html>",
							"Livres regroupés",
							JOptionPane.INFORMATION_MESSAGE);
				} else {
					boule = true;
					for (int i = 0; i < pan.tableauLivres.getSelectedRowCount(); i++) {
						int idLivre = Integer.parseInt(pan.tableauLivres.getValueAt(pan.tableauLivres.getSelectedRows()[i],0).toString());
						pm.setNote("Traitement du livre n°"+idLivre);

						String numero = FonctionsUtiles.codeBarrise(
								pan.tableauLivres.getValueAt(pan.tableauLivres.getSelectedRows()[i],0).toString(), "livre");

						aCoder = ConnectionSQL.selectionLivreParId(idLivre);

						String nom = aCoder.get("nom").toString() + " - "
								+ aCoder.get("auteur").toString();

						if (nom.length()>27) {
							nom = nom.substring(0, 25)+"...";
						}

						String ecole = "École "
								+ aCoder.get("ecole").toString();

						Map<String, String> elements = new HashMap<String, String>();
						elements.put("nom", nom);
						elements.put("numero", numero);
						elements.put("ecole", ecole);

						postafiok.add(elements);

						/*
						 * Ici je rajoute qu'il faut mettre le livre dans la
						 * liste des éléments dont le code-barres a déjà été
						 * édité
						 */
						ConnectionSQL.insertionCodeBarre("livres", idLivre);
						/*
						 * Je ne me sers pas du tout de cette liste, mais on ne
						 * sait jamais...
						 */
						
						progres += 200/pan.tableauLivres.getSelectedRowCount();
						pm.setProgress(Math.round(progres));
						
					}
				}
			} else {
				JOptionPane.showMessageDialog(GCB.this,
						"Au moins un livre doit être sélectionné.",
						"Sélection vide", JOptionPane.INFORMATION_MESSAGE);
				boule = false;
			}
		} else {
			if (pan.tableauEleves.getSelectedRowCount() > 0) {
				boule = true;
				for (int i = 0; i < pan.tableauEleves.getSelectedRowCount(); i++) {
					int idEleve = Integer.parseInt(pan.tableauEleves
							.getValueAt(pan.tableauEleves.getSelectedRows()[i],
									0).toString());
					pm.setNote("Traitement de l'élèvé n°"+idEleve);

					String numero = FonctionsUtiles.codeBarrise(
							pan.tableauEleves.getValueAt(
									pan.tableauEleves.getSelectedRows()[i], 0)
									.toString(), "eleve");

					aCoder = ConnectionSQL.selectionEleveParId(idEleve);

					String nom = aCoder.get("nom").toString() + " "
							+ aCoder.get("prenom").toString();

					if (nom.length()>27) {
						nom = nom.substring(0, 25)+"...";
					}

					String ecole = "École " + aCoder.get("ecole").toString();

					Map<String, String> elements = new HashMap<String, String>();
					elements.put("nom", nom);
					elements.put("numero", numero);
					elements.put("ecole", ecole);

					postafiok.add(elements);

					/*
					 * Ici je rajoute qu'il faut mettre le livre dans la liste
					 * des éléments dont le code-barres a déjà été édité
					 */
					ConnectionSQL.insertionCodeBarre("eleves", idEleve);
					
					progres += 200/pan.tableauEleves.getSelectedRowCount();
					pm.setProgress(Math.round(progres));
					
				}
			} else {
				boule = false;
				JOptionPane.showMessageDialog(GCB.this,
						"Au moins un élève doit être sélectionné.",
						"Sélection vide", JOptionPane.INFORMATION_MESSAGE);
			}
		}

		/*
		 * Édition
		 */
		if (boule) {
			Barcode barcode = null;
			for (Map<String, String> element : postafiok) {
				pm.setNote("Édition de "+element.get("nom"));
				try {
					barcode = BarcodeFactory.createCode128(element.get("numero"));
					barcode.setBarHeight(60);
					barcode.setDrawingText(false);

					File f = new File("codesBarres/elements/" + element.get("numero") + ".png");
					BarcodeImageHandler.savePNG(barcode, f);
				} catch (BarcodeException bce) {
					bce.printStackTrace();
				} catch (OutputException oe) {
					oe.printStackTrace();
				}
				progres+=50/postafiok.size();
				pm.setProgress(Math.round(progres));
			}
			// nom du fichier
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd-HHmmss");
			Date maintenow = Calendar.getInstance().getTime();
			String laDate = df.format(maintenow);

			PDDocument document = new PDDocument();
			int nombrePages = 1+postafiok.size()/40;
			PDPage[] pages = new PDPage[nombrePages];

			for (int i=0;i<nombrePages;i++) {
				pages[i] = new PDPage(PDRectangle.A4);
			}

			try {
				PDPageContentStream flux = new PDPageContentStream(document, pages[0]);
				flux.setFont(PDType1Font.HELVETICA, 10);
				/*
				 * Dans /home/crd/Documents, il y a un fichier .ods avec les longueurs, pour m'y retrouver
				 */
				// 595×841.5f c'est 21×29.7 en pt.
				final float echelle = Float.parseFloat(FonctionsUtiles.LireChamp("impression","scale"));
				
				final float largeurEtiquette = echelle * Float.parseFloat(FonctionsUtiles.LireChamp("impression", "largeurEtiquette"));
				final float hauteurEtiquette = echelle * Float.parseFloat(FonctionsUtiles.LireChamp("impression", "hauteurEtiquette"));

				final float largeurCB = echelle * Float.parseFloat(FonctionsUtiles.LireChamp("impression", "largeurCB"));
				final float hauteurCB = echelle * Float.parseFloat(FonctionsUtiles.LireChamp("impression", "hauteurCB"));

				final float oX = Float.parseFloat(FonctionsUtiles.LireChamp("impression", "coinDepartX"));
				final float oY = Float.parseFloat(FonctionsUtiles.LireChamp("impression", "coinDepartY"));
				
				final float coinDepartX = oX*echelle+(1-echelle)*595/2;  // ce calcul un peu fou, c'est Thalès. Merci Thalès.
				final float coinDepartY = 841.5f-(oY*echelle+(1-echelle)*841.5f/2);
				
				final int nbCol = Integer.parseInt(FonctionsUtiles.LireChamp("impression", "nbCol"));
				final int nbLig = Integer.parseInt(FonctionsUtiles.LireChamp("impression", "nbLig"));
				
				final float paddingX = echelle * Float.parseFloat(FonctionsUtiles.LireChamp("impression", "paddingX"));
				final float paddingY = echelle * Float.parseFloat(FonctionsUtiles.LireChamp("impression", "paddingY"));
				
				final float innerHorizontalSpace = echelle * Float.parseFloat(FonctionsUtiles.LireChamp("impression", "innerHorizontalSpace"));
				final float innerVerticalSpace = echelle * Float.parseFloat(FonctionsUtiles.LireChamp("impression", "innerVerticalSpace"));
				/* */

				int nbCB = nbCol*nbLig;
				
				int j=0;
				for (Map<String, String> element : postafiok) {
					pm.setNote("Traitement de "+element.get("nom"));
					if ((j%nbCB==0)&&(j!=0)) {
						int pageNo = j/nbCB;

						flux.close();

						flux = new PDPageContentStream(document, pages[pageNo]);
						flux.setFont(PDType1Font.HELVETICA, 10);
					}
					int ligne=(j%nbCB)/nbCol;
					int colonne = j%nbCol;

					float coinX = coinDepartX + colonne*largeurEtiquette +(colonne-1)*innerHorizontalSpace;
					float coinY = coinDepartY - ligne*hauteurEtiquette - (ligne-1)*innerVerticalSpace;


					// je dessine un rectangle pour mon premier essai.
					
					/*
					flux.moveTo(coinX, coinY);


					flux.lineTo(coinX+largeurEtiquette, coinY); 
					flux.lineTo(coinX+largeurEtiquette, coinY-hauteurEtiquette);
					flux.lineTo(coinX,coinY-hauteurEtiquette);
					flux.lineTo(coinX,coinY);

					flux.stroke();
					*/

					float centeredXPosition = coinX + largeurEtiquette/2;
					float ecoleWidth = PDType1Font.HELVETICA.getStringWidth(element.get("ecole"));
					float nomWidth = PDType1Font.HELVETICA.getStringWidth(element.get("nom"));

					PDImageXObject pdImage = PDImageXObject.createFromFile("codesBarres/elements/"+element.get("numero")+".png", document);
					flux.drawImage(pdImage, coinX+paddingX, coinY-paddingY-hauteurCB, largeurCB, hauteurCB);
					// coinY - 19.125f - 34.0f car j'enlève l'offset intérieur à l'étiquette (padding) et la hauteur du code-barre


					flux.beginText();
					flux.newLineAtOffset(centeredXPosition-ecoleWidth/200, coinY-paddingY+paddingX);
					flux.showText(element.get("ecole"));
					flux.endText();
					flux.beginText();
					flux.newLineAtOffset(centeredXPosition-nomWidth/200, coinY-hauteurEtiquette+paddingX);
					flux.showText(element.get("nom"));
					flux.endText();

					flux.beginText();
					flux.setTextMatrix(Matrix.getRotateInstance(Math.PI/2, coinX+largeurCB+2*paddingX+1.0f, coinY-paddingY-hauteurCB+1.0f));
					flux.showText(element.get("numero"));
					flux.endText();

					j++;
					progres+=50/postafiok.size();
					pm.setProgress(Math.round(progres));
				}

				// TODO ajouter dans les paramètres la possibilité de changer le format (/!\ et donc le nombre d'étiquettes par feuille)
				// TODO ajouter la possibilité que les étiquettes ne soient pas collées sur la feuille (offset)

				flux.close();

				pm.close();

			} catch (IOException e1) {
				e1.printStackTrace();
			}
			for (int i=0;i<nombrePages;i++) {
				document.addPage(pages[i]);
			}
			try {
				document.save("codesBarres/"+laDate+".pdf");
				document.close();

				Desktop.getDesktop().open(new File("codesBarres/"+laDate+".pdf"));
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}	
		}
	}
}

