import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.text.DefaultEditorKit;


public class ParametresImpression extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	JPanel colonneGraphique = new JPanel() {
		/**
		 * 
		 */
		private static final long serialVersionUID = 2L; // faudrait que je me renseigne sur l'intérêt de ce truc. Je suis sûr que c'est bien quand on sait à quoi ça sert.

		public void paintComponent(Graphics g){

			float coefficient = 0.28f;
			
			int largeurEtiquette = Math.round(coefficient*Float.parseFloat(FonctionsUtiles.LireChamp("impression", "largeurEtiquette")));
			int hauteurEtiquette = Math.round(coefficient*Float.parseFloat(FonctionsUtiles.LireChamp("impression", "hauteurEtiquette")));

			int coinDepartX = Math.round(coefficient*Float.parseFloat(FonctionsUtiles.LireChamp("impression", "coinDepartX")));
			int coinDepartY = Math.round(coefficient*Float.parseFloat(FonctionsUtiles.LireChamp("impression", "coinDepartY")));
			
			int nbCol = Integer.parseInt(FonctionsUtiles.LireChamp("impression","nbCol"));
			int nbLig = Integer.parseInt(FonctionsUtiles.LireChamp("impression", "nbLig"));
			
			int innerHorizontalSpace = Math.round(coefficient*Float.parseFloat(FonctionsUtiles.LireChamp("impression","innerHorizontalSpace")));
			int innerVerticalSpace = Math.round(coefficient*Float.parseFloat(FonctionsUtiles.LireChamp("impression","innerVerticalSpace")));
			
			
			// 595×841.5f c'est 21×29.7 en pt.
			g.drawRect(0, 0, Math.round(coefficient*595), Math.round(coefficient*841.5f));
			for (int y=0;y<nbLig;y++) {
				for (int x=0;x<nbCol;x++) {
					g.drawRect(coinDepartX + x*(largeurEtiquette+innerHorizontalSpace), coinDepartY+y*(hauteurEtiquette+innerVerticalSpace), largeurEtiquette, hauteurEtiquette);
				}
			}
		}
	};
	JPanel colonneTexte = new JPanel();
	JPanel pTexteEtBouton = new JPanel();
	
	JPanel p1 = new JPanel();
	JPanel p2 = new JPanel();
	
	JPanel l0 = new JPanel();
	JPanel l1 = new JPanel();
	JPanel l3 = new JPanel();
	JPanel l5 = new JPanel();
	JPanel l6 = new JPanel();
	JPanel l7 = new JPanel();
	JPanel l9 = new JPanel();
	JPanel l11 = new JPanel();
	JPanel l13 = new JPanel();
	JPanel l14 = new JPanel();
	JPanel l15 = new JPanel();
	
	JTextField tfLargeurEtiquette = new JTextField();
	JTextField tfHauteurEtiquette = new JTextField();
	JTextField tfLargeurCB = new JTextField();
	JTextField tfHauteurCB = new JTextField();
	JTextField tfInnerHorizontalSpace = new JTextField();
	JTextField tfInnerVerticalSpace = new JTextField();
	JTextField tfCoinDepartX = new JTextField();
	JTextField tfCoinDepartY = new JTextField();
	JTextField tfCoinArriveeX = new JTextField();
	JTextField tfCoinArriveeY = new JTextField();
	JTextField tfNbCol = new JTextField();
	JTextField tfNbLig = new JTextField();
	JTextField tfPaddingX = new JTextField();
	JTextField tfPaddingY = new JTextField();
	JTextField tfScale = new JTextField();
	
	String[] unites = {"pt","cm"};
	JComboBox<String> cbUnite = new JComboBox<String>(unites);
	
	JButton bAdjust = new JButton("Ajuster à l'étiquette");
	
	JButton bValider = new JButton("Valider");
	
	Map<String,JTextField> jtfs = new LinkedHashMap<String,JTextField>();
	Map<String,JPanel> panels = new LinkedHashMap<String,JPanel>();
	
	String[] champsImpression = new String[]{"largeurEtiquette","hauteurEtiquette","largeurCB","hauteurCB","innerHorizontalSpace","innerVerticalSpace","coinDepartX","coinArriveeX","coinDepartY","coinArriveeY","nbCol","nbLig","paddingX","paddingY","scale"};
	
	String[] champsX = new String[]{"largeurEtiquette","innerHorizontalSpace","coinDepartX","coinArriveeX"};
	String[] champsY = new String[]{"hauteurEtiquette","innerVerticalSpace","coinDepartY","coinArriveeY"};
	
	JPopupMenu pmX = new JPopupMenu();
	JPopupMenu pmY = new JPopupMenu();
	JMenuItem miCalcX = new JMenuItem("Calculer automatiquement");
	JMenuItem miCalcY = new JMenuItem("Calculer automatiquement");
	
	JScrollPane spTexte = new JScrollPane(colonneTexte);
	
	String currentUnit = "pt";
	
	public ParametresImpression() {
	
		// listage des JTextFields
		jtfs.put("largeurEtiquette",tfLargeurEtiquette);
		jtfs.put("hauteurEtiquette",tfHauteurEtiquette);
		jtfs.put("largeurCB",tfLargeurCB);
		jtfs.put("hauteurCB",tfHauteurCB);
		jtfs.put("innerHorizontalSpace",tfInnerHorizontalSpace);
		jtfs.put("innerVerticalSpace",tfInnerVerticalSpace);
		jtfs.put("coinDepartX",tfCoinDepartX);
		jtfs.put("coinArriveeX",tfCoinArriveeX);
		jtfs.put("coinDepartY",tfCoinDepartY);
		jtfs.put("coinArriveeY",tfCoinArriveeY);
		jtfs.put("nbCol",tfNbCol);
		jtfs.put("nbLig",tfNbLig);
		jtfs.put("paddingX",tfPaddingX);
		jtfs.put("paddingY",tfPaddingY);
		jtfs.put("scale",tfScale);

		
		for (String nom : champsImpression) {
			jtfs.get(nom).setPreferredSize(new Dimension(50,20));
			jtfs.get(nom).setText(FonctionsUtiles.LireChamp("impression", nom));
		}
		
		
		//miCalcX.addActionListener(alCalcX);
		
		// parametrage des popupmenus
		pmX.add(miCalcX);
		Action couper = new DefaultEditorKit.CutAction();
    	couper.putValue(Action.NAME, "Couper");
    	couper.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_X, KeyEvent.CTRL_DOWN_MASK));
    	pmX.add(couper);
    	
    	Action copier = new DefaultEditorKit.CopyAction();
    	copier.putValue(Action.NAME, "Copier");
    	copier.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.CTRL_DOWN_MASK));
    	pmX.add(copier);
    	
    	Action coller = new DefaultEditorKit.PasteAction();
    	coller.putValue(Action.NAME, "Coller");
    	coller.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_V, KeyEvent.CTRL_DOWN_MASK));
    	pmX.add(coller);
    	
		
		pmY.add(miCalcY);
		pmY.add(couper);
		pmY.add(copier);
		pmY.add(coller);
		
		// le mouselistener pour les popupmenus
		for (String nom : champsX) {	
			jtfs.get(nom).addMouseListener(mlClicX);
			
		}
		
		for (String nom : champsY) {	
			jtfs.get(nom).addMouseListener(mlClicY);
			
		}
		
		// divers ActionListener
		
		cbUnite.addActionListener(alChangeUnite);
		
		bAdjust.addActionListener(alAdjust);
		
		colonneGraphique.setBorder(new EmptyBorder(10,10,10,10));
		
		
		this.setLayout(new BoxLayout(this,BoxLayout.LINE_AXIS));
		
		colonneTexte.setLayout(new BoxLayout(colonneTexte, BoxLayout.PAGE_AXIS));
		
		l0.setLayout(new FlowLayout());
		l0.add(new JLabel("Unité :"));
		l0.add(cbUnite);
		
		// les étiquettes -> layout sur la page
		
		p1.setLayout(new BoxLayout(p1, BoxLayout.PAGE_AXIS));
		p1.setBorder(BorderFactory.createTitledBorder("Agencement page"));
		
		l1.setLayout(new FlowLayout());
		l1.add(new JLabel("Étiquette :"));
		l1.add(tfLargeurEtiquette);
		l1.add(new JLabel("×"));
		l1.add(tfHauteurEtiquette);
					
		
		l5.setLayout(new FlowLayout());
		l5.add(new JLabel("Espace horizontal entre les codes-barres :")); // TODO à rajouter dans le parametres.ini
		l5.add(tfInnerHorizontalSpace);
		
		l6.setLayout(new FlowLayout());
		l6.add(new JLabel("Espace vertical entre les codes-barres :")); // TODO à rajouter dans parametres.ini
		l6.add(tfInnerVerticalSpace);
		
		l7.setLayout(new FlowLayout());
		l7.add(new JLabel("Marge gauche :"));
		l7.add(tfCoinDepartX);
		l7.add(new JLabel("Marge droite :")); // celle-ci doit être surveillée par un listener car ça change les longueurs en cas de changement.
		l7.add(tfCoinArriveeX);
		
		l9.setLayout(new FlowLayout());
		l9.add(new JLabel("Marge haut :"));
		l9.add(tfCoinDepartY);
		l9.add(new JLabel("Marge bas :"));
		l9.add(tfCoinArriveeY);
	
		l11.setLayout(new FlowLayout());
		l11.add(new JLabel("Nombre d'étiquettes :"));
		l11.add(tfNbCol);
		l11.add(new JLabel("×"));
		l11.add(tfNbLig);
		
		// layout dans l'étiquette
		
		p2.setLayout(new BoxLayout(p2, BoxLayout.PAGE_AXIS));
		p2.setBorder(BorderFactory.createTitledBorder("Agencement codes-barres"));
				
		l3.setLayout(new FlowLayout());
		l3.add(new JLabel("Code-barre :"));
		l3.add(tfLargeurCB);
		l3.add(new JLabel("×"));
		l3.add(tfHauteurCB);
		
		l13.setLayout(new FlowLayout());
		l13.add(new JLabel("Marges horizontales dans l'étiquette :"));
		l13.add(tfPaddingX);
		
		l14.setLayout(new FlowLayout());
		l14.add(new JLabel("Marges verticales dans l'étiquette :"));
		l14.add(tfPaddingY);
		
		bAdjust.setAlignmentX(JPanel.CENTER_ALIGNMENT);
		
		// échelle
		
		l15.setLayout(new FlowLayout());
		l15.add(new JLabel("Échelle :"));
		l15.add(tfScale);
		
		
		colonneTexte.add(l0);
		
		p1.add(l1);
		p1.add(l5);
		p1.add(l6);
		p1.add(l7);
		p1.add(l9);
		p1.add(l11);
		
		colonneTexte.add(p1);
		
		p2.add(l3);
		p2.add(l13);
		p2.add(l14);
		p2.add(bAdjust);
		
		colonneTexte.add(p2);
		
		colonneTexte.add(l15);
		
		pTexteEtBouton.setLayout(new BoxLayout(pTexteEtBouton, BoxLayout.PAGE_AXIS));
		
		pTexteEtBouton.add(spTexte);
		bValider.addActionListener(alValidation);
		bValider.setAlignmentX(JScrollPane.CENTER_ALIGNMENT);
		pTexteEtBouton.add(bValider);

		
		this.add(colonneGraphique);
		
		this.add(pTexteEtBouton);
		
	}
	
	private ActionListener alValidation = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			boolean erreur = false;
			// TODO il faut vérifier que les longueurs additionnées donnent bien la longueur et la hauteur de la feuille
			
			// horizontalement :
			int nbCol = Integer.parseInt(jtfs.get("nbCol").getText());
			float largeurEtiquette = Float.parseFloat(jtfs.get("largeurEtiquette").getText());
			float coinDepartX = Float.parseFloat(jtfs.get("coinDepartX").getText());
			float coinArriveeX = Float.parseFloat(jtfs.get("coinArriveeX").getText());
			float innerHorizontalSpace = Float.parseFloat(jtfs.get("innerHorizontalSpace").getText());

			float total = coinDepartX + nbCol*largeurEtiquette+ (nbCol-1)*innerHorizontalSpace + coinArriveeX;
			
			if (Math.abs(total-595) > 1.5) {
				JOptionPane.showMessageDialog(ParametresImpression.this,
						"La somme des longueurs horizontales est "+String.valueOf(total)+" alors qu'elle devrait être 595.",
						"Erreur de longueurs horizontales", JOptionPane.WARNING_MESSAGE);
				for (String nom : champsX) {
					jtfs.get(nom).setBorder(BorderFactory.createLineBorder(Color.RED));
				}
				jtfs.get("nbCol").setBorder(BorderFactory.createLineBorder(Color.RED));
				
				erreur = true;
			}
			
			int nbLig = Integer.parseInt(jtfs.get("nbLig").getText());
			float hauteurEtiquette = Float.parseFloat(jtfs.get("hauteurEtiquette").getText());
			float coinDepartY = Float.parseFloat(jtfs.get("coinDepartY").getText());
			float coinArriveeY = Float.parseFloat(jtfs.get("coinArriveeY").getText());
			float innerVerticalSpace = Float.parseFloat(jtfs.get("innerVerticalSpace").getText());

			total = coinDepartY + nbLig*hauteurEtiquette+ (nbLig-1)*innerVerticalSpace + coinArriveeY;
			
			if (Math.abs(total-841.5) > 1.5) {
				JOptionPane.showMessageDialog(ParametresImpression.this,
						"La somme des longueurs verticales est "+String.valueOf(total)+" alors qu'elle devrait être 841.5.",
						"Erreur de longueurs verticales", JOptionPane.WARNING_MESSAGE);
				for (String nom : champsY) {
					jtfs.get(nom).setBorder(BorderFactory.createLineBorder(Color.RED));
				}
				jtfs.get("nbLig").setBorder(BorderFactory.createLineBorder(Color.RED));
				
				erreur = true;
			}
			
			
			
			if (!erreur) {
				int nombreModifs = 0;

				for (String nom : champsImpression) {
					if (!jtfs.get(nom).getText().equals(FonctionsUtiles.LireChamp("impression", nom))) {
						nombreModifs += 1;
						EcrireParametres.Changer("impression", nom, jtfs.get(nom).getText());
					}
				}


				if (nombreModifs == 1) {
					JOptionPane.showMessageDialog(ParametresImpression.this, "La modification a été apportée.", "Modification", JOptionPane.INFORMATION_MESSAGE);
				}
				if (nombreModifs > 1) {
					JOptionPane.showMessageDialog(ParametresImpression.this, nombreModifs+" modifications ont été apportées.", "Modifications", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		}
		
	};
	
	
	private ActionListener alChangeUnite = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			if (!cbUnite.getSelectedItem().equals(currentUnit)) {
				currentUnit = cbUnite.getSelectedItem().toString();
				
				if (cbUnite.getSelectedItem().equals("pt")) {
					// ici on passe des cm aux pt (ie ×28.333 (ie ×85/3))
					// j'arrondis au centième.
					for (String nom : champsImpression) {
						if (!nom.equals("scale") && !nom.equals("nbCol") && !nom.equals("nbLig")) {
							if (!jtfs.get(nom).getText().isEmpty()) {
								float f = Float.parseFloat(jtfs.get(nom).getText());
								f = (float)Math.round(100*f*85/3)/100;
								jtfs.get(nom).setText(String.valueOf(f));
							}
						}
					}
					bValider.setEnabled(true);
					bValider.setToolTipText(null);
				} else {
					// ici on passe des pt aux cm (du coup je divise par 85/3)
					for (String nom : champsImpression) {
						if (!nom.equals("scale") && !nom.equals("nbCol") && !nom.equals("nbLig")) {
							if (!jtfs.get(nom).getText().isEmpty()) {
								float f = Float.parseFloat(jtfs.get(nom).getText());
								f = (float)Math.round(100*f*3/85)/100;
								jtfs.get(nom).setText(String.valueOf(f));
							}
						}
					}
					bValider.setEnabled(false);
					bValider.setToolTipText("Le bouton Valider est désactivé tant que l'unité est cm.");
				}
			}
			
		}
	};

	ActionListener alCalcXLargeurEtiquette = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			
		}
	};
	
	
	private class AlCalcX implements ActionListener {
		JTextField tf;
		
		public AlCalcX(JTextField paramTF) {
			this.tf = paramTF;
		}
		
		public void actionPerformed(ActionEvent e) {
			float totalX;
			if (cbUnite.getSelectedItem().toString().equals("pt")) {
				totalX = 595;
			} else {
				// le même en cm
				totalX = 21;
			}
			
			float largeur = Float.parseFloat(jtfs.get("largeurEtiquette").getText());
			float innerX = Float.parseFloat(jtfs.get("innerHorizontalSpace").getText());
			float depX = Float.parseFloat(jtfs.get("coinDepartX").getText());
			float arrX = Float.parseFloat(jtfs.get("coinArriveeX").getText());
			int nbCol = Integer.parseInt(jtfs.get("nbCol").getText());
			
			String ceJTF="";
			
			for (String nom : champsX ) {
				if (jtfs.get(nom) == tf) {
					ceJTF = nom;
				}
			}
			
			float longueur = 0;
			
			/* On différencie les cas : chaque case sa formule */
			if (ceJTF.equals("largeurEtiquette")) {	
				longueur = ((float)Math.round(100*(totalX-(nbCol-1)*innerX-depX-arrX)/nbCol))/100;
			}
			if (ceJTF.equals("innerHorizontalSpace")) {
				longueur = ((float)Math.round(100*(totalX - nbCol*largeur-depX-arrX)/(nbCol-1)))/100;
			}
			if (ceJTF.equals("coinDepartX")) {
				longueur = ((float)Math.round(100*(totalX - nbCol*(largeur+innerX)+innerX-arrX)))/100;
			}
			if (ceJTF.equals("coinArriveeX")) {
				longueur = ((float)Math.round(100*(totalX - nbCol*(largeur+innerX)+innerX-depX)))/100;
			}
			
			tf.setText(String.valueOf(longueur));
		}
	};
	
	private class AlCalcY implements ActionListener {
		JTextField tf;
		
		public AlCalcY(JTextField paramTF) {
			this.tf = paramTF;
		}
		
		public void actionPerformed(ActionEvent e) {
			float totalX;
			if (cbUnite.getSelectedItem().toString().equals("pt")) {
				totalX = 841.5f;
			} else {
				// le même en cm
				totalX = 29.7f;
			}
			
			float hauteur = Float.parseFloat(jtfs.get("hauteurEtiquette").getText());
			float innerY = Float.parseFloat(jtfs.get("innerVerticalSpace").getText());
			float depY = Float.parseFloat(jtfs.get("coinDepartY").getText());
			float arrY = Float.parseFloat(jtfs.get("coinArriveeY").getText());
			int nbLig = Integer.parseInt(jtfs.get("nbLig").getText());
			
			String ceJTF="";
			
			for (String nom : champsY ) {
				if (jtfs.get(nom) == tf) {
					ceJTF = nom;
				}
			}
			
			float longueur = 0;
			
			/* On différencie les cas : chaque case sa formule */
			if (ceJTF.equals("hauteurEtiquette")) {	
				longueur = ((float)Math.round(100*(totalX-(nbLig-1)*innerY-depY-arrY)/nbLig))/100;
			}
			if (ceJTF.equals("innerVerticalSpace")) {
				longueur = ((float)Math.round(100*(totalX - nbLig*hauteur-depY-arrY)/(nbLig-1)))/100;
			}
			if (ceJTF.equals("coinDepartY")) {
				longueur = ((float)Math.round(100*(totalX - nbLig*(hauteur+innerY)+innerY-arrY)))/100;
			}
			if (ceJTF.equals("coinArriveeY")) {
				longueur = ((float)Math.round(100*(totalX - nbLig*(hauteur+innerY)+innerY-depY)))/100;
			}
			
			tf.setText(String.valueOf(longueur));
		}
	};
	
	MouseAdapter mlClicX = new MouseAdapter() {
		public void mousePressed(MouseEvent souris ) {
			if (SwingUtilities.isRightMouseButton(souris)) {
				JTextField jtf = (JTextField) souris.getSource();
				
				// Je suppose que ce n'est pas la meilleure méthode.
				for (ActionListener al : miCalcX.getActionListeners() ) {
					miCalcX.removeActionListener(al);
				}
				miCalcX.addActionListener(new AlCalcX(jtf));
				// mais bon. Hein. Je ne suis pas non plus payé hein.
				
				Point p = souris.getPoint();
				pmX.show(jtf,p.x,p.y);
			}
		}
	};

	MouseAdapter mlClicY = new MouseAdapter() {
		public void mousePressed(MouseEvent souris ) {
			if (SwingUtilities.isRightMouseButton(souris)) {
				JTextField jtf = (JTextField)souris.getSource();
				
				// Je suppose que ce n'est pas la meilleure méthode.
				for (ActionListener al : miCalcY.getActionListeners() ) {
					miCalcY.removeActionListener(al);
				}
				miCalcY.addActionListener(new AlCalcY(jtf));
				// mais bon. Hein. Je ne suis pas non plus payé hein.
				
				Point p = souris.getPoint();
				pmY.show(jtf,p.x,p.y);
			}
		}
	};

	ActionListener alAdjust = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			// X
			float largeurEtiquette = Float.parseFloat(jtfs.get("largeurEtiquette").getText());
			float largeurCB = Float.parseFloat(jtfs.get("largeurCB").getText());
			float paddingX = Float.parseFloat(jtfs.get("paddingX").getText());
			
			float multiplicateurX = 0.91f*largeurEtiquette/(largeurCB+2*paddingX);

			jtfs.get("largeurCB").setText(String.valueOf( ( (float) Math.round(	100*multiplicateurX*largeurCB ) )/100 ) );
			jtfs.get("paddingX").setText(String.valueOf( ( (float) Math.round(	100*multiplicateurX*paddingX ) )/100 ) );

			// Y
			float hauteurEtiquette = Float.parseFloat(jtfs.get("hauteurEtiquette").getText());
			float hauteurCB = Float.parseFloat(jtfs.get("hauteurCB").getText());
			float paddingY = Float.parseFloat(jtfs.get("paddingY").getText());
			
			float multiplicateurY = hauteurEtiquette/(hauteurCB+2*paddingY);

			jtfs.get("hauteurCB").setText(String.valueOf( ( (float) Math.round(	100*multiplicateurY*hauteurCB ) )/100 ) );
			jtfs.get("paddingY").setText(String.valueOf( ( (float) Math.round(	100*multiplicateurY*paddingY ) )/100 ) );

		}
	};
}
