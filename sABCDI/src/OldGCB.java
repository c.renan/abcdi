import java.awt.Desktop;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ProgressMonitor;
import javax.swing.SwingWorker;

import org.apache.commons.lang.SystemUtils;


public class OldGCB extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Bouton Générer :
	JButton boutonGo = new JButton("Générer codes-barres");
	
	JButton boutonDossier = new JButton();
	
	Recherche pan = new Recherche();

	Map<String, Object> aCoder = new HashMap<String, Object>();
	
	public OldGCB() {
		/*
		 * Gestion des boutons
		 */
		boutonGo.addActionListener(this);
		
		try {
			Image img = ImageIO.read(new File("resources/dossierCB.png"));
			Image resizedImage = img.getScaledInstance(30,30, Image.SCALE_SMOOTH);
			boutonDossier.setIcon(new ImageIcon(resizedImage));
		} catch (Exception e) {
			e.printStackTrace();
		}
		boutonDossier.setToolTipText("Accéder au dossier contenant les codes-barres déjà édités");
		boutonDossier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				try {
					if (SystemUtils.IS_OS_WINDOWS) {
						Desktop.getDesktop().open(new File(System.getProperty("user.dir")+"/codesBarres/"));
					} else {
						URI gagarine = new URI("file://"+System.getProperty("user.dir")+"/codesBarres/");
						Desktop bureau = Desktop.getDesktop();
						bureau.browse(gagarine);
					}
				} catch (IOException io) {
					io.printStackTrace();
				} catch (URISyntaxException youri) {
					youri.printStackTrace();
				}
			}
		});
		
		this.setLayout(new GridBagLayout());
		GridBagConstraints cTR = new GridBagConstraints();
		cTR.gridx=0;
		cTR.gridwidth=2;
		cTR.gridy=0;
		cTR.fill=GridBagConstraints.BOTH;
		cTR.weightx=1;
		cTR.weighty=1;
		this.add(pan,cTR);
				
		GridBagConstraints cBG = new GridBagConstraints();
		cBG.fill=GridBagConstraints.NONE;
		cBG.gridx=0;
		cBG.gridy=1;
		cBG.weightx=1;
		this.add(boutonGo,cBG);
		
		cBG.weightx=0;
		cBG.gridx+=1;
		this.add(boutonDossier,cBG);
	}

	public void actionPerformed(ActionEvent e) {
		String postafiok = "";
		/*
		 * Cette string postafiok est ce que j'envoie au site internet.
		 */
		// je me demande si cette variable a un nom qui respecte les conventions d'appellation java...
		String nature = "";
		boolean boule;
		if (pan.rbLivres.isSelected()) {
			if (pan.tableauLivres.getSelectedRowCount()>0) {
				if (Boolean.valueOf(FonctionsUtiles.LireChamp("livres", "grouper"))) {
					JOptionPane.showMessageDialog(OldGCB.this, "<html>Les livres sont regroupés.<br>Il faut les dégrouper (dans les paramètres) pour pouvoir éditer les code-barres.</html>",  "Livres regroupés", JOptionPane.INFORMATION_MESSAGE);
					boule=false;
				}
				else {
					boule=true;
					nature = "livres";
					for (int i=0;i<pan.tableauLivres.getSelectedRowCount();i++) {
						int idLivre = Integer.parseInt(pan.tableauLivres.getValueAt(pan.tableauLivres.getSelectedRows()[i], 0).toString());
						aCoder = ConnectionSQL.selectionLivreParId(idLivre);
	
						/*
						 * Ici je rajoute qu'il faut mettre le livre dans la liste des éléments dont le code-barres a déjà été édité
						 */
						ConnectionSQL.insertionCodeBarre("livres", idLivre);
						/*
						 * Je ne me sers pas du tout de cette liste, mais on ne sait jamais...
						 */
						
	
						if (i>0) {
							postafiok += "%20;;%20";
						}
						postafiok += FonctionsUtiles.vireGules(aCoder.get("id").toString())+","+
								FonctionsUtiles.vireGules(aCoder.get("nom").toString()) + ","+
								FonctionsUtiles.vireGules(aCoder.get("ecole").toString());
					}
				}
			}
			else {
				boule=false;
				JOptionPane.showMessageDialog(OldGCB.this, "Au moins un livre doit être sélectionné.", "Sélection vide", JOptionPane.INFORMATION_MESSAGE);
			}
		}
		else {
			if (pan.tableauEleves.getSelectedRowCount()>0) {
				boule = true;
				nature = "eleves";
				for (int i=0;i<pan.tableauEleves.getSelectedRowCount();i++) {
					int idEleve = Integer.parseInt(pan.tableauEleves.getValueAt(pan.tableauEleves.getSelectedRows()[i], 0).toString());
					aCoder = ConnectionSQL.selectionEleveParId(idEleve);

					/*
					 * Ici je rajoute qu'il faut mettre le livre dans la liste des éléments dont le code-barres a déjà été édité
					 */
					ConnectionSQL.insertionCodeBarre("eleves", idEleve);

					if (i>0) {
						postafiok += "%20;;%20";
					}
					postafiok += FonctionsUtiles.vireGules(aCoder.get("id").toString())+","+
							FonctionsUtiles.vireGules(aCoder.get("nom").toString()) + ","+
							FonctionsUtiles.vireGules(aCoder.get("prenom").toString())+ ","+
							FonctionsUtiles.vireGules(aCoder.get("ecole").toString());
				}
			}
			else {
				boule= false;
				JOptionPane.showMessageDialog(OldGCB.this, "Au moins un élève doit être sélectionné.", "Sélection vide", JOptionPane.INFORMATION_MESSAGE);
			}
		}
		if (boule) {
			final String n = nature;
			final String p = postafiok;
			SwingWorker<Void, Void> worker = new SwingWorker<Void,Void>() {
				 @Override
				 protected Void doInBackground() throws Exception {
					 doTheJob(n,p);
					 return null;
				 }

			 };

			 worker.execute();
		}
		
	}

	void doTheJob(String nature, String postafiok) {
		try {
			ProgressMonitor pm = new ProgressMonitor(OldGCB.this, "Téléchargement des codes-barres","",0,3);
			pm.setMillisToDecideToPopup(0);
			
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd-HHmmss");
			Date maintenow = Calendar.getInstance().getTime();
			String laDate = df.format(maintenow);

			if (!FonctionsUtiles.LireChamp("site", "proxy").isEmpty()) {
				System.setProperty("https.proxyHost", FonctionsUtiles.quelProxy());
				System.setProperty("https.proxyPort", FonctionsUtiles.quelPort());
			}

			pm.setProgress(1);
			
			URL myURL = new URL(FonctionsUtiles.LireChamp("site", "url")+"codeBarre.php?n="+nature+"&s="+postafiok+"&nom="+laDate);

			BufferedReader in = new BufferedReader(new InputStreamReader(myURL.openStream()));

			in.close();
			// J'ouvre et je ferme directement simplement pour activer la connexion.
			
			pm.setProgress(2);
			
			URL myURLFile = new URL(FonctionsUtiles.LireChamp("site", "url")+"codesBarres/"+laDate+".pdf");

			ReadableByteChannel rbc = Channels.newChannel(myURLFile.openStream());

			FileOutputStream fos = new FileOutputStream(new File("codesBarres/"+laDate+".pdf"));
			fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);

			fos.close();

			if (!FonctionsUtiles.LireChamp("site", "proxy").isEmpty()) {
				System.clearProperty("https.proxyHost");
			}
			
			pm.setProgress(3);
			
			Desktop.getDesktop().open(new File("codesBarres/"+laDate+".pdf"));
		} catch (FileNotFoundException fnf) {
			JOptionPane.showMessageDialog(OldGCB.this, "<html>Le fichier n'a pas été créé. Vérifier l'URL dans les paramètres avancés.<br>" +
					"Si le problème persiste, contacter le développeur : c.renan.dev@gmail.com<br>"+
					"<small>File Not Found Exception</small>"+
					"</html>", "Erreur d'URL", JOptionPane.ERROR_MESSAGE);
		} catch (UnknownHostException uh) {
			JOptionPane.showMessageDialog(OldGCB.this, "<html>Le site "+FonctionsUtiles.LireChamp("site", "url")+" n'a pas été trouvé. Vérifier l'URL dans les paramètres avancés.<br>" +
					"Si le problème persiste, contacter le développeur : c.renan.dev@gmail.com<br>"+
					"<small>Unknown Host Exception</small>"+
					"</html>", "Erreur d'URL", JOptionPane.ERROR_MESSAGE);
		}
		catch (MalformedURLException malformed) {
			JOptionPane.showMessageDialog(OldGCB.this, "<html>Le site "+FonctionsUtiles.LireChamp("site", "url")+" n'a pas été trouvé. Vérifier l'URL dans les paramètres avancés.<br>" +
					"Si le problème persiste, contacter le développeur : c.renan.dev@gmail.com<br>"+
					"<small>Malformed URL Exception</small>"+
					"</html>", "Erreur d'URL", JOptionPane.ERROR_MESSAGE);
		} catch (IOException io) {
			JOptionPane.showMessageDialog(OldGCB.this, "<html>Erreur d'entrée/sortie. Vérifier la connection internet.<br>" +
					"Si le problème persiste, contacter le développeur : c.renan.dev@gmail.com<br>"+
					"<small>IO Exception</small>"+
					"</html>", "Erreur d'IO", JOptionPane.ERROR_MESSAGE);
			io.printStackTrace();
		}
	 }

	
	
	public class lanceLeDL extends JDialog {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1993696170173453193L;

		JPanel pLabel = new JPanel();
		JLabel lText = new JLabel("<html>Transmission des informations au site en cours...<br>Cette opération peut prendre quelques secondes...</html>");
		
		public lanceLeDL(String nature, String postafiok) {

			/*
			 * Il faut que je fasse ma boîte de dialogue...
			 */
			
			/*pLabel.setLayout(new FlowLayout());
			pLabel.add(lText);
			pLabel.add(new JButton("Fermer"));
			*/
			//getContentPane().setLayout(new FlowLayout());
			getContentPane().add(lText);
			
			this.setTitle("Téléchargement des codes barres.");
			//this.setContentPane(pLabel);
			this.pack();
			this.setLocationRelativeTo(OldGCB.this);
			this.setVisible(true);
			
			try {
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd-HHmmss");
				Date maintenow = Calendar.getInstance().getTime();
				String laDate = df.format(maintenow);

				if (!FonctionsUtiles.LireChamp("site", "proxy").isEmpty()) {
					System.setProperty("https.proxyHost", FonctionsUtiles.quelProxy());
					System.setProperty("https.proxyPort", FonctionsUtiles.quelPort());
				}

				URL myURL = new URL(FonctionsUtiles.LireChamp("site", "url")+"codeBarre.php?n="+nature+"&s="+postafiok+"&nom="+laDate);

				BufferedReader in = new BufferedReader(new InputStreamReader(myURL.openStream()));

				in.close();
				// J'ouvre et je ferme directement simplement pour activer la connexion.

				lText.setText("<html>Téléchargement du fichier de codes-barres...<br>Cette opération peut prendre quelques secondes...</html>");
				this.repaint();
				
				URL myURLFile = new URL(FonctionsUtiles.LireChamp("site", "url")+"codesBarres/"+laDate+".pdf");

				ReadableByteChannel rbc = Channels.newChannel(myURLFile.openStream());

				FileOutputStream fos = new FileOutputStream(new File("codesBarres/"+laDate+".pdf"));
				fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);

				fos.close();

				if (!FonctionsUtiles.LireChamp("site", "proxy").isEmpty()) {
					System.clearProperty("https.proxyHost");
				}

				Desktop.getDesktop().open(new File("codesBarres/"+laDate+".pdf"));
			} catch (FileNotFoundException fnf) {
				JOptionPane.showMessageDialog(OldGCB.this, "<html>Le fichier n'a pas été créé. Vérifier l'URL dans les paramètres avancés.<br>" +
						"Si le problème persiste, contacter le développeur : c.renan.dev@gmail.com<br>"+
						"<small>File Not Found Exception</small>"+
						"</html>", "Erreur d'URL", JOptionPane.ERROR_MESSAGE);
			} catch (UnknownHostException uh) {
				JOptionPane.showMessageDialog(OldGCB.this, "<html>Le site "+FonctionsUtiles.LireChamp("site", "url")+" n'a pas été trouvé. Vérifier l'URL dans les paramètres avancés.<br>" +
						"Si le problème persiste, contacter le développeur : c.renan.dev@gmail.com<br>"+
						"<small>Unknown Host Exception</small>"+
						"</html>", "Erreur d'URL", JOptionPane.ERROR_MESSAGE);
			}
			catch (MalformedURLException malformed) {
				JOptionPane.showMessageDialog(OldGCB.this, "<html>Le site "+FonctionsUtiles.LireChamp("site", "url")+" n'a pas été trouvé. Vérifier l'URL dans les paramètres avancés.<br>" +
						"Si le problème persiste, contacter le développeur : c.renan.dev@gmail.com<br>"+
						"<small>Malformed URL Exception</small>"+
						"</html>", "Erreur d'URL", JOptionPane.ERROR_MESSAGE);
			} catch (IOException io) {
				JOptionPane.showMessageDialog(OldGCB.this, "<html>Erreur d'entrée/sortie. Vérifier la connection internet.<br>" +
						"Si le problème persiste, contacter le développeur : c.renan.dev@gmail.com<br>"+
						"<small>IO Exception</small>"+
						"</html>", "Erreur d'IO", JOptionPane.ERROR_MESSAGE);
				io.printStackTrace();
			}
			
			/*
			 * Et que j'en dispose une fois que c'est fait.
			 */
			
			this.dispose();
		}
	}
}
