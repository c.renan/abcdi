# ABCDI

Gestionnaire de bibliothèque en JAVA.

## Contenu

Ce package contient deux dossiers indissociables dans leur utilisation : `sABCDI` et `sABCDIc` : le serveur et le client de l'application ABCDI.

Indissociable non, car on peut se servir de `sABCDI` de manière autonome.

## Structure des projets

Comme c'était la première fois que je faisais du Java, j'ai mis toutes mes classes dans le même dossier sans me demander si c'était bien. La plupart des classes ont des noms explicites comme `Fenetre.java` même si ce ne sont pas des noms très académiques.

Pourtant les classes à exécuter pour lancer le projet sont

- la classe `AuChocolat` pour `sABCDI`,
- la classe `ALaFraise` pour `sABCDIc`.

C'est parce que je suis un fameux luron, vous verriez ça.

## TODO

Il va falloir rajouter la doc (en gitignorant le pdf) ; la manière de compiler ce projet en un .exe

## Export en `.exe`
Pour compiler ce projet en `.exe`, à partir d'**Eclipse**,

- On commence par faire **File > Export**
- Sous la forme d'un **Runnable Jar File**
- Il faut exporter soit la classe `ALaFraise`, soit la classe `AuChocolat` dans un fichier appelé respectivement `sABCDIc` ou `sABCDI`.
- Ensuite utilise les scripts présents dans le dossier `compilation`.
  > Le problème, c'est que je n'ai pas conservé la structure de mes dossiers donc il faudra certainement changer deux ou trois choses.\
  > D'autre part, dans les fichiers `config_pourVERSION.xml` j'ai indiqué des chemins n'importe quoi, car je pense que les chemins doivent être absolus. Je propose d'utiliser un `sed` pour venir à bout de ce problème... Sauf que je viens d'essayer et `sed` n'aime pas trop beaucoup les slashes.\
  > À voir donc.\
  > Enfin en attendant, vous pouvez mettre vos chemins absolus.